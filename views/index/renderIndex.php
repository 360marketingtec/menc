
    <div class="container">
      <div class="row">
        <div id="content" class="col-sm-12">
          <div class="banner-1">
		<? foreach( $this->offersSlider as $slide ){ ?>
            <div class="banner-box"> <a class="clearfix" href="<?=$slide[v][link];?>">
			<img src="<?=photo::feachJson($slide[home_slider][photo]);?>" alt="banner-1"
class="img-responsive"/>
              <div class="s-desc">
                <div class="vert-align">
                  <h2><?=$slide[home_slider][title];?></h2>
                  <h3><?=$slide[home_slider][desc];?></h3>
                </div>
              </div>
              </a> </div>
		<? } ?>
          </div>
          <div class="clear"></div>

          <div class="box featured">
            <div class="box-heading">
              <h3><?=translate::localWords("top_products");?></h3>
            </div>
            <div class="box-content">
              <div class="row">
                <?$i=1;
                if($i<9){
                foreach ($this->products as $product ){ ?>

                <div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div class="product-thumb transition">
                    <div class="quick_info">
                      <div id="quickview_<?=$i;?>" class="quickview-style">
                        <div>
                          <div class="left col-sm-4">
                            <div class="quickview_image image">
					<a href="/<?=$product[products][url];?>/4/1/<?=$product[products][id];?>">
					<img alt="<?=$product[products][title];?>" title="<?=$product[products][title];?>" class="img-responsive" src="/image/cache/catalog/Lithium-Ion-Drill-Driver-270x270.png"/></a>
							</div>
                          </div>
                          <div class="right col-sm-8">
                            <h2><?=$product[products][title];?></h2>
                            <div class="inf">
                              <p class="quickview_manufacture manufacture manufacture">Brand: <a href="http://localhost/menkk/index.php?route=product/manufacturer/info&amp;manufacturer_id=5">Malita</a> </p>
                              <p class="product_model model">Model: Product 8</p>
                            </div>
                            <div class="cart-button">
                            <button class="btn btn-add" data-toggle="tooltip"
                              title="<?=translate::localWords("Add_to_Cart");?>" type="button"
                              onclick="cart.add('35');">
                              <i class="fa fa-shopping-cart"></i>
                            </button>
                            <button class="btn btn-icon" type="button" data-toggle="tooltip"
                            title="<?=translate::localWords("Add_to_Wish_List");?>"
                            onclick="wishlist.add('35');">
                            <i class="fa fa-heart"></i>
                            </button>
                            <button class="btn btn-icon" type="button" data-toggle="tooltip"
                            title="<?=translate::localWords("Compare_this_Product");?>"
                            onclick="compare.add('35');">
                            <i class="fa fa-exchange"></i></button>
                            </div>
                            <div class="clear"></div>
                            <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
class="fa fa-star-o fa-stack-2x"></i></span> </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="quickview_description description">
                              <p><b><?=translate::localWords("The_products_of_our_store");?></b>
                                <?=$product[products][short_desc];?></p>
                              <p>If you want to know more information you can address our superb online <b>24/7</b> support system. We have a great number of different promos and you can get a pretty good discount. Keep saving your money with our store!</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="image">
<a class="lazy" style="padding-bottom: 100%" href="/<?=$product[products][url];?>/4/1/<?=$product[products][id];?>">
  <img alt="<?=$product[products][title];?>" title="<?=$product[products][title];?>" class="img-responsive"
data-src="<?=photo::feachJson($product[products][photo],'name',0);?>"
src="<?=photo::feachJson($product[products][photo],'name',0);?>"/>
</a>
</div>
                    

                  </div>
                </div>

                <?$i++;}
              }?>

              </div>
            </div>
          </div>
          <div></div>
          <div class="clear"></div>
          <div id="carousel0" class="owl-carousel partners">
			<? foreach( $this->partnersLogos as $logo ){?>
            <div class="item text-center"> <a href="<?=$logo[partners_logos][link];?>"><img src="<?=photo::feachJson($logo[partners_logos][logo]);?>" alt="<?=$logo[partners_logos][name];?>" class="img-responsive" /></a> </div>
			<? } ?>
          </div>
          <script type="text/javascript"><!--
$('#carousel0').owlCarousel({
items: 6,
autoPlay: 3000,
navigation: false,
navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
pagination: false,
itemsTablet: 3,
itemsMobile: 2
});
--></script>
        </div>
      </div>
    </div>
