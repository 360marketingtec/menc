<!DOCTYPE html>
<html lang="en">
     <head>
		<meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=$this->metaTitle;?></title>
		<meta name="description" CONTENT="<?=$this->metaDesc;?>">
		<meta name="keywords" CONTENT="<?=$this->metaKeywords;?>">

		<meta name="description" content="Tools online store"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link href="/image/favicon.png" rel="icon"/>
		<script src="/includes/js/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
		<link href="/includes/js/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
		<script src="/includes/js/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<link href="/includes/js/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
		<link href="/includes/js/jquery/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="/includes/css/magnificent.css" rel="stylesheet">
		<link href="/includes/js/jquery.bxslider/jquery.bxslider.css" rel="stylesheet">
		<link href="/includes/css/photoswipe.css" rel="stylesheet">
		<link href="/includes/js/fancybox/jquery.fancybox.css" rel="stylesheet">
		<link href="/stylesheet/material-design.css" rel="stylesheet">
		<link href="/includes/js/jquery/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet"
		media="screen"/>
		<link href="/includes/js/jquery/owl-carousel/owl.transitions.css" type="text/css" rel="stylesheet"
		media="screen"/>
		<script src="/includes/js/common.js" type="text/javascript"></script>
		<script src="/includes/js/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
		<!--custom script-->
		<script src="/includes/js/device.min.js" type="text/javascript"></script>
		<!--[if lt IE 9]>
		<div style='clear:both;height:59px;padding:0 15px 0 15px;position:relative;z-index:10000;text-align:center;'>
		<a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img
		src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0"
		height="42" width="820"
		alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
		</a>
		</div><![endif]-->
		<link href="/includes/css/stylesheet.css" rel="stylesheet">
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
		<link href="http://fonts.googleapis.com/css?family=Ubuntu:300&subset=latin,cyrillic">

		<? if( loader::$langId == 1 ){ ?>
			<link rel="stylesheet" href="/includes/css/style_ar.css">
		<? } ?>


		<?=$this->headerPlus;?>
		<?=common::getStaticCoods('header');?>

     </head>

<body class="common-home">

<!-- swipe menu -->
<div class="swipe">
  <div class="swipe-menu">
    <ul>
		<li class="current"><a href="/"><?=translate::localWords("home");?></a></li>
		<?=menu::navMenu() ;?>
    </ul>
  </div>
</div>
<div id="page">
  <div class="toprow-1"> <a class="swipe-control" href="#"><i class="fa fa-align-justify"></i></a> </div>
  <header class="header">
    <div class="top_panel">
      <div class="container">
        <nav id="top-links" class="nav">
          <ul class="list-inline">
          	<li class="current"><a href="/"><?=translate::localWords("home");?></a></li>
			<?=menu::navMenu() ;?>
          </ul>
        </nav>


        <div class="box-language">
          <form action="http://localhost/menkk/index.php?route=common/language/language" method="post" enctype="multipart/form-data" id="language">
            <div class="btn-group"> <span class="dropdown-toggle" data-toggle="dropdown"> <img class="hidden" src="/image/flags/gb.png" alt="English" title="English"> en <span class="hidden-xs hidden-sm hidden-md hidden">Language</span> </span>
              <ul class="dropdown-menu pull-right">
                <li><a href="en"><img class="hidden" src="/image/flags/gb.png" alt="English" title="English" /> English</a></li>
                <li><a href="de"><img class="hidden" src="/image/flags/de.png" alt="Deutsch" title="Deutsch" /> Arabic</a></li>

              </ul>
            </div>
            <input type="hidden" name="code" value="" />
            <input type="hidden" name="redirect" value="http://localhost/menkk/index.php?route=common/home" />
          </form>
        </div>
      </div>
    </div>
    <div class="menu-wrap">
      <div class="container">
        <div id="logo" class="logo"> <a href="/"><img src="/image/catalog/logo.png" title="Tools online store"
alt="Tools online store" class="img-responsive"/></a></div>

		<nav id="menu" class="navbar">
		  <div class="collapse navbar-collapse navbar-ex1-collapse">
		    <ul class="nav__primary navbar-nav">
				<? foreach( menu::topProductsCatogries() as $company ){?>
			  		<li><a href="/<?=$company[companies][title];?>/4/0/<?=$company[companies][id];?>"><?=$company[companies][title];?></a></li>
				<? } ?>
            </ul>
          </div>
        </nav>
      </div>
    </div>
    <div class="container">
      <div id="menu-gadget" class="menu-gadget">
        <div id="menu-icon" class="menu-icon"><?=translate::localWords("categories");?></div>
        <ul class="menu">
			<? foreach( menu::topProductsCatogries() as $company ){?>
		  		<li><a href="/<?=$company[companies][title];?>/4/0/<?=$company[companies][id];?>"><?=$company[companies][title];?></a></li>
			<? } ?>
        </ul>
      </div>
    </div>
    <div class="container">
      <hr/>
    </div>
  </header>
  <div class="header_modules"></div>

	<div id="container">
		  <? view::renderContentView(); ?>
	</div>
<!--=======footer=================================-->




  <div class="content_bottom"> </div>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-3 col-lg-3" data-equal-group="1">
          <div class="footer"> <a href="http://localhost/menkk/index.php?route=common/home"><img src="/image/catalog/logo.png" title="Tools online store"
alt="Tools online store" class="img-responsive"/></a>
            <div class="box_html footer_caption">
              <p><?=menu::fexedFooter();?></p>
            </div>
            <!-- begin olark code -->
            <script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="/javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="/javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com//jsclient/loader0/js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('7830-582-10-3714');/*]]>*/</script>
            <noscript>
            <a href="https://www.olark.com/site/7830-582-10-3714/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a>
            </noscript>
            <!-- end olark code -->
          </div>
        </div>
        <div class="col-sm-12 col-md-9 col-lg-9">
          <div class="footer-wrap" data-equal-group="1">
            <div class="footer_box">
              <h5>Information</h5>
              <ul class="list-unstyled">
                <li> <a href="http://localhost/menkk/index.php?route=information/information&amp;information_id=4">About Us</a> </li>
                <li> <a href="http://localhost/menkk/index.php?route=information/information&amp;information_id=6">Delivery Information</a> </li>
                <li> <a href="http://localhost/menkk/index.php?route=information/information&amp;information_id=3">Privacy Policy</a> </li>
                <li> <a href="http://localhost/menkk/index.php?route=information/information&amp;information_id=5">Terms &amp; Conditions</a> </li>
              </ul>
            </div>
            <div class="footer_box">
              <h5>Customer Service</h5>
              <ul class="list-unstyled">
                <li> <a href="http://localhost/menkk/index.php?route=information/contact">Contact Us</a> </li>
                <li> <a href="http://localhost/menkk/index.php?route=account/return/add">Returns</a> </li>
                <li> <a href="http://localhost/menkk/index.php?route=information/sitemap">Site Map</a> </li>
              </ul>
            </div>
            <div class="footer_box">
              <h5>Extras</h5>
              <ul class="list-unstyled">
                <li> <a href="http://localhost/menkk/index.php?route=product/manufacturer">Brands</a> </li>
                <li> <a href="http://localhost/menkk/index.php?route=account/voucher">Gift Vouchers</a> </li>
                <li> <a href="http://localhost/menkk/index.php?route=affiliate/account">Affiliates</a> </li>
                <li> <a href="http://localhost/menkk/index.php?route=product/special">Specials</a> </li>
              </ul>
            </div>
            <div class="footer_box">
              <h5>My Account</h5>
              <ul class="list-unstyled">
                <li> <a href="http://localhost/menkk/index.php?route=account/account">My Account</a> </li>
                <li> <a href="http://localhost/menkk/index.php?route=account/order">Order History</a> </li>
                <li> <a href="http://localhost/menkk/index.php?route=account/wishlist">Wish List</a> </li>
                <li> <a href="http://localhost/menkk/index.php?route=account/newsletter">Newsletter</a> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <hr/>
      <ul class="social-list list-unstyled">
        <li> <a href='//www.facebook.com'> <i class="fa fa-facebook"></i> <span>Facebook</span> </a> </li>
        <li> <a href='//www.twitter.com'> <i class="fa fa-twitter"></i> <span>Twitter</span> </a> </li>
        <li> <a href='//www.google-plus.com'> <i class="fa fa-google-plus"></i> <span>Google Plus</span> </a> </li>
      </ul>
      <div class="copyright"> Powered By <a href="http://www.360marketingtec.com">360Marketing</a><br />
         &copy; 2016
        <!-- [[%FOOTER_LINK]] -->
      </div>
    </div>
  </footer>
  <script src="/includes/js/livesearch.js" type="text/javascript"></script>
  <script src="/includes/js/script.js" type="text/javascript"></script>
</div>
<?=common::getStaticCoods('footer');?>
</body>
</html>
