<div class="container">
<ul class="breadcrumb">
<li><a href="http://localhost/menkk/index.php?route=common/home"><i class="fa fa-home"></i></a></li>
<li><a href="http://localhost/menkk/index.php?route=product/product&amp;product_id=28">
  <?=$this->product[products][title];?></a></li>
</ul>

<div class="row">


<div id="content" class="col-sm-12 product_page">
  <div class="row product-content-columns">

<!-- Content left -->
<div class="col-sm-5 col-lg-7 product_page-left">
<!-- product image -->
<div id="default_gallery" class="product-gallery">
<div class="image-thumb">
<ul id="image-additional" class="image-additional">

    <li>
    <a href="#" data-image="<?=photo::feachJson($this->product[products][photo],'name',0);?>">
    <img src="<?=photo::feachJson($this->product[products][photo],'name',0);?>" alt=""/>
    </a>
    </li>
    <li>
    <a href="#" data-image="/image/cache/catalog/Screwdriver-Impact-Driver-Combo-2-800x800.png">
    <img src="<?=photo::feachJson($this->product[products][photo],'name',1);?>" alt=""/></a>
    </li>
    <li>
    <a href="#" data-image="/image/cache/catalog/Screwdriver-Impact-Driver-Combo-800x800.png">
    <img src="<?=photo::feachJson($this->product[products][photo],'name',2);?>" alt=""/></a>
    </li>

</ul>
</div>

<div id="product-image" class="product-image">

<div class="magnificent-wrap">
<div class="magnificent" data-magnificent="product-image">
<div class="polaroid">
<div class="inner">
<img src="<?=photo::feachJson($this->product[products][photo],'name',0);?>"/>
</div>
</div>
</div>
</div>

<div class="magnificent-viewport-wrap">
<div data-magnificent-viewport="product-image">
<div class="inner">
<img src="<?=photo::feachJson($this->product[products][photo],'name',1);?>"/>
</div>
</div>
</div>

<script class="source">
$(function () {
$('#product-image [data-magnificent]').magnificent();
});
</script>
</div>
</div>



<script type="/text/javascript">
jQuery(document).ready(function(){
var myPhotoSwipe = $("#gallery a").photoSwipe({ enableMouseWheel: false , enableKeyboard: false, captionAndToolbarAutoHideDelay:0 });
});
</script>

<div id="full_gallery">
<ul id="gallery">
<li><a href="/image/cache/catalog/Screwdriver-Impact-Driver-800x800.png" data-something="something" data-another-thing="anotherthing">
  <img src="<?=photo::feachJson($this->product[products][photo],'name',0);?>" title="<?=$this->product[products][title];?>" alt="<?=$this->product[products][title];?>" />
</a></li>
  <li><a href="/image/cache/catalog/Screwdriver-Impact-Driver-Combo-2-800x800.png" data-something="something1" data-another-thing="anotherthing1">
    <img src="<?=photo::feachJson($this->product[products][photo],'name',1);?>" alt="<?=$this->product[products][title];?>" />
  </a></li>
<li><a href="/image/cache/catalog/Screwdriver-Impact-Driver-Combo-800x800.png" data-something="something1" data-another-thing="anotherthing1">
  <img src="<?=photo::feachJson($this->product[products][photo],'name',2);?>" alt="<?=$this->product[products][title];?>" />
</a></li>
</ul>
</div>

</div>

<!-- Content right -->
<div class="col-sm-7 col-lg-5 product_page-right">
<div class="general_info product-info">

<h1 class="product-title"><?=$this->product[products][title];?></h1>

<!-- Prodyuct rating status -->
<div class="rating-section product-rating-status">
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
&nbsp;
&nbsp;
<a onclick="document.getElementById('tab-review').scrollIntoView();">3 reviews</a> /
<a onclick="document.getElementById('tab-review').scrollIntoView();"><?=translate::localWords("Write_a_review");?></a>
</div>
</div>

<div class="price-section">
<span class="price-new"><?=$this->product[products][price_new];?></span>
<span class="price-old"><?=$this->product[products][price_old];?></span>
<div class="reward-block">
<span	class="reward">Price in reward points: 200</span>
</div>
</div>

<ul class="list-unstyled product-section">
<li><?=translate::localWords("Brand");?>: <a href="/<?=$this->company[products][url];?>/4/0/<?=$this->company[products][id];?>"><?=$this->company[companies][title];?></a></li>
<li>Product Code: <span>Product 1</span></li>
<li>Reward Points: <span>400</span></li>
<li><?=translate::localWords("Availability");?>:
<?if($this->product[products][available]=='1'){?>
  <span><?=translate::localWords("In_Stock");?></span>
<?}else{?>
  <span><?=translate::localWords("out_of_Stock");?></span>
  <?}?>
</li>
</ul>
</div>

<div id="product">

<!-- Product options -->
<div class="product-options form-horizontal">
<h3><?=translate::localWords("Available_Options");?></h3>
<div class="form-group">
    <label class="control-label col-sm-4"><?=translate::localWords("Color");?></label>

    <div id="input-option234" class="col-sm-8">
      <?print_r($this->colors);
      foreach($this->colors as $color){?>
        <div class="radio">
          <label>
              <input type="radio" name="color" value="<?=$color[colors][id];?>" /> <?=$color[colors][name];?>
          </label>
        </div>
        <?}?>
    </div>
</div>
<div class="form-group required">
  <label class="control-label col-sm-4" for="input-option233"><?=translate::localWords("Size");?></label>
<div class="col-sm-8">
<select name="option[233]" id="input-option233" class="form-control">
    <option value=""><?=translate::localWords("Please_Select");?> </option>
    <option value="1"><?=translate::localWords("Small");?>
    </option>
    <option value="2"><?=translate::localWords("Medium");?>
    </option>
    <option value="3"><?=translate::localWords("Large");?>
    </option>
</select>
</div>
</div>
</div>

<!-- product reccurings -->
<div class="product-reccurings">
</div>

<!-- Add to cart form -->
<div class="form-group form-horizontal">
<div class="form-group">
<label class="control-label col-sm-4" for="input-quantity">Qty</label>
<div class="col-sm-2">
<input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control" />
</div>
</div>

<input type="hidden" name="product_id" value="28" />
<button type="button" id="button-cart" data-loading-text="Loading..." class="product-btn-add">
<i class="material-design-shopping231"></i>
Add to Cart                        </button>
</div>

<ul class="product-buttons">
<li><button class="product-btn" onclick="wishlist.add('28');"><i class="material-design-favorite22"></i></button></li>
<li><button type="button" class="product-btn" onclick="compare.add('28');"><i class="material-design-shuffle24"></i></button></li>
</ul>

<div class="product-share">
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" ></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
<script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script>
<!-- AddThis Button END -->
</div>

</div>

</div>
</div>

<!-- Product description -->
<div id="tab-description" class="product-desc product-section">
<h3 class="product-section_title"><?=translate::localWords("Description");?></h3>
<iframe src="https://www.youtube.com/embed/mvvl-GpCFxc?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" height="315" width="560"></iframe>
<p><b>The products of our store</b> <?=$this->product[products][data];?>
</div>

<!-- Product specifications -->
<div id="tab-specification" class="product-spec product-section">
<h3 class="product-section_title">Specification</h3>
<table class="table table-bordered">
<tbody>
<tr>
<th colspan="2"><strong>Characteristics</strong></th>
</tr>
</tbody>
<tbody>
<tr>
<td>Attributes 1</td>
<td>500</td>
</tr>
<tr>
<td>Attributes 3</td>
<td>800</td>
</tr>
<tr>
<td>Attributes 7</td>
<td>700</td>
</tr>
<tr>
<td>Attributes 9</td>
<td>900</td>
</tr>
</tbody>
</table>
</div>

<!-- Product reviews -->

<div id="tab-review" class="product-reviews product-section">
<h3 class="product-section_title">Reviews (3)</h3>
<form class="form-horizontal">

<!-- Reviews list -->
<div id="review"></div>

<!-- Review form -->
<div class="review-form-title">
<h3 class="product-section_title" id="reviews_form_title">Write a review</h3>
</div>
<div class="product-review-form" id="reviews_form">
<div class="form-group required">
<label class="control-label col-sm-3" for="input-name">Your Name</label>
<div class="col-sm-9">
<input type="text" name="name" value="" id="input-name" class="form-control" />
</div>
</div>
<div class="form-group required">
<label class="control-label col-sm-3" for="input-review">Your Review</label>
<div class="col-sm-9">
<textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
<div class="help-block"><span class="text-danger">Note:</span> HTML is not translated!</div>
</div>
</div>
<div class="form-group required">
<label class="control-label col-sm-3">Rating</label>
<div class="col-sm-9">
&nbsp;&nbsp;&nbsp; Bad&nbsp;
<input type="radio" name="rating" value="1" />
&nbsp;
<input type="radio" name="rating" value="2" />
&nbsp;
<input type="radio" name="rating" value="3" />
&nbsp;
<input type="radio" name="rating" value="4" />
&nbsp;
<input type="radio" name="rating" value="5" />
&nbsp;Good							</div>
</div>
<div class="form-group required">
<label class="control-label col-sm-3" for="input-captcha">Enter the code in the box below</label>
<div class="col-sm-9" >
<input type="text" name="captcha" value="" id="input-captcha" class="form-control" />
</div>
</div>
<div class="form-group">
<div class="col-sm-9 col-sm-offset-3">
<img src="index.php?route=tool/captcha" alt="" id="captcha" />
<div class="pull-right">
<button type="button" id="button-review" data-loading-text="Loading..." class="btn btn-primary">Continue</button>
</div>
</div>

</div>

</div>
</form>
</div>

<!-- Related products -->

<div class="related-products product-section">
<h3 class="product-section_title"><?=translate::localWords("Related_Products");?></h3>
<div class="related-slider">

  <?foreach ($this->relatedProducts as $relatedProduct) {?>
<div>
<div class="product-thumb transition">
<div class="image">
      <a class="lazy"
      style="padding-bottom: 100%"
      href="http://localhost/menkk/index.php?route=product/product&amp;product_id=28">
      <img alt="<?=$relatedProduct[products][title];?>"
      title="<?=$relatedProduct[products][title];?>"
      class="img-responsive"
      src="/image/cache/catalog/Screwdriver-Impact-Driver-270x270.png"
      src="#"/>
</a>
</div>
<div class="caption">
      <div class="name" data-equal-group="2">
      <a href="http://localhost/menkk/index.php?route=product/product&amp;product_id=28">Screwdriver Impact Driver Combo</a>
      </div>
      <div class="description">
      <?$relatedProduct[products][short_desc];?></div>
      <div class="price">
      <span class="price-new"><?=$relatedProduct[products][price_new];?></span> <span
      class="price-old"><?=$relatedProduct[products][price_old];?></span>
      </div>
</div>
<div class="cart-button">

  <button class="product-btn-add" type="button"
  onclick="cart.add('28');">
  <i class="material-design-shopping231"></i>
  <span><?=translate::localWords("Add_to_Cart");?></span>
  </button>

<div>
<button class="product-btn" type="button" data-toggle="tooltip"
title="<?=translate::localWords("Add_to_Wish_List");?>"
onclick="wishlist.add('28');">
<i class="material-design-favorite22"></i>
</button>
<button class="product-btn" type="button" data-toggle="tooltip"
title="<?=translate::localWords("Compare_this_Product");?>"
onclick="compare.add('28');"><i
class="material-design-shuffle24"></i>
</button>
</div>
<div class="rating">
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
class="fa fa-star-o fa-stack-2x"></i></span>
<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
class="fa fa-star-o fa-stack-2x"></i></span>
</div>
</div>
</div>
</div>
<?}?>
<!-- Product comments -->
<!-- <div class="product-comments product-section">
<h3 class="product-section_title">Comments</h3>
<div id="disqus_thread"></div>
<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'thtest123'; // required: replace example with your forum shortname

/* * * DON'T EDIT BELOW THIS LINE * * */
(function() {
var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
</div> -->

</div>
</div>
</div>

<script>
document.getElementById('input-quantity').onkeypress = function(e) {

e = e || event;

if (e.ctrlKey || e.altKey || e.metaKey) return;

var chr = getChar(e);

if (chr == null) return;

if (chr < '0' || chr > '9') {
return false;
}

}

function getChar(event) {
if (event.which == null) {
if (event.keyCode < 32) return null;
return String.fromCharCode(event.keyCode) // IE
}

if (event.which!=0 && event.charCode!=0) {
if (event.which < 32) return null;
return String.fromCharCode(event.which)
}

return null;
}
jQuery('#reviews_form_title').addClass('close-tab');
jQuery('#reviews_form_title').on("click", function(){
if (jQuery(this).hasClass('close-tab')) { jQuery(this).removeClass('close').parents('#tab-review').find('#reviews_form').slideToggle(); }
else {
jQuery(this).addClass('close-tab').parents('#tab-review').find('#reviews_form').slideToggle();
}
})
</script>

<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
$.ajax({
url: 'index.php?route=product/product/getRecurringDescription',
type: 'post',
data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
dataType: 'json',
beforeSend: function() {
$('#recurring-description').html('');
},
success: function(json) {
$('.alert, .text-danger').remove();

if (json['success']) {
$('#recurring-description').html(json['success']);
}
}
});
});
//-->
</script>

<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
$.ajax({
url: 'index.php?route=checkout/cart/add',
type: 'post',
data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
dataType: 'json',
beforeSend: function() {
$('#button-cart').button('loading');
},
complete: function() {
$('#button-cart').button('reset');
},
success: function(json) {
$('.alert, .text-danger').remove();
$('.form-group').removeClass('has-error');

if (json['error']) {
if (json['error']['option']) {
for (i in json['error']['option']) {
var element = $('#input-option' + i.replace('_', '-'));

if (element.parent().hasClass('input-group')) {
element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
} else {
element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
}
}
}

if (json['error']['recurring']) {
$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
}

// Highlight any found errors
$('.text-danger').parent().addClass('has-error');
}

if (json['success']) {
$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');



<!--$('html, body').animate({ scrollTop: 0 }, 'slow');-->

$('#cart').load('index.php?route=common/cart/info #cart');
setTimeout(function() {$('.alert').fadeOut(1000)},3000)
}
}
});
});
//-->
</script>

<script type="text/javascript"><!--
$('.date').datetimepicker({
pickTime: false
});

$('.datetime').datetimepicker({
pickDate: true,
pickTime: true
});

$('.time').datetimepicker({
pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
var node = this;

$('#form-upload').remove();

$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

$('#form-upload input[name=\'file\']').trigger('click');

$('#form-upload input[name=\'file\']').on('change', function() {
$.ajax({
url: 'index.php?route=tool/upload',
type: 'post',
dataType: 'json',
data: new FormData($(this).parent()[0]),
cache: false,
contentType: false,
processData: false,
beforeSend: function() {
$(node).button('loading');
},
complete: function() {
$(node).button('reset');
},
success: function(json) {
$('.text-danger').remove();

if (json['error']) {
$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
}

if (json['success']) {
alert(json['success']);

$(node).parent().find('input').attr('value', json['code']);
}
},
error: function(xhr, ajaxOptions, thrownError) {
alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
}
});
});
});
//-->
</script>

<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
e.preventDefault();

$('#review').fadeOut('slow');

$('#review').load(this.href);

$('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=28');

$('#button-review').on('click', function() {
$.ajax({
url: 'index.php?route=product/product/write&product_id=28',
type: 'post',
dataType: 'json',
data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
beforeSend: function() {
$('#button-review').button('loading');
},
complete: function() {
$('#button-review').button('reset');
$('#captcha').attr('src', 'index.php?route=tool/captcha#'+new Date().getTime());
$('input[name=\'captcha\']').val('');
},
success: function(json) {
$('.alert-success, .alert-danger').remove();

if (json['error']) {
$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
}

if (json['success']) {
$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

$('input[name=\'name\']').val('');
$('textarea[name=\'text\']').val('');
$('input[name=\'rating\']:checked').prop('checked', false);
$('input[name=\'captcha\']').val('');
}
}
});
});
//-->
</script>

<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'thtest123'; // required: replace example with your forum shortname

/* * * DON'T EDIT BELOW THIS LINE * * */
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = '//' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
