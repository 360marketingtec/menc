

<div class="container">
<ul class="breadcrumb">
<li><a href="#/"><i class="fa fa-home"></i></a></li>
<li><a href="#/"><?=$this->company[companies][title];?></a></li>
</ul>
<div class="row">
<div id="content" class="col-sm-9">
  <h2><?=$this->company[companies][title];?></h2>
<div class="row">
<div class="col-sm-10">
  <p><b>The products of our store</b>
  <?=$this->company[companies][data];?>
</p>
</div>
</div>
<br><br>
<div class="product-filter clearfix">
<div class="row">
<div class="col-md-2">
<label class="control-label" for="input-sort"><?=translate::localWords("Sort_By");?>:</label>
</div>
<div class="col-md-3">
<select id="input-sort" class="form-control col-sm-3" onchange="location = this.value;">
      <option value="http://localhost/menkk/index.php?route=product/company&amp;path=20&amp;sort=p.sort_order&amp;order=ASC" selected="selected">Default</option>
      <option value="#/">Name (A - Z)</option>
      <option value="#/">Name (Z - A)</option>
      <option value="#/">Price (Low &gt; High)</option>
      <option value="#/">Price (High &gt; Low)</option>
      <option value="#/">Rating (Highest)</option>
      <option value="#/">Rating (Lowest)</option>
      <option value="#/">Model (A - Z)</option>
      <option value="#/">Model (Z - A)</option>
</select>
</div>

<div class="col-md-2">
<label class="control-label" for="input-limit"><?=translate::localWords("Show");?>:</label>
</div>
<div class="col-md-2">
  <select id="input-limit" class="form-control" onchange="location = this.value;">
      <option value="#/" selected="selected">6</option>
      <option value="#/">25</option>
      <option value="#/">50</option>
      <option value="#/">75</option>
      <option value="#/">100</option>
</select>
</div>

<div class="col-md-3 text-right">
<div class="button-view">
<button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="List">
  <i class="fa fa-th-list"></i></button>
<button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="Grid">
  <i class="fa fa-th"></i></button>
</div>
</div>

</div>
</div>

<div class="nav-cat clearfix">
<div class="pull-left">
  <ul class="pagination">
    <li class="active"><span>1</span></li>
    <li><a href="#/">2</a></li>
    <li><a href="#/">&gt;</a></li>
    <li><a href="http://localhost/menkk/index.php?route=product/company&amp;path=20&amp;page=2">&gt;|</a></li>
  </ul>
</div>

<div class="pull-left nam-page">Showing 1 to 6 of 12 (2 Pages)</div>
<div class="pull-right">
<a href="#/" id="compare-total" class="btn">Product Compare (0)</a>
</div>
</div>

<div class="row">
<?foreach( $this->products as $product ){?>
<div class="product-layout product-list col-xs-12">
<div class="product-thumb">
<div class="image">
<a class="lazy" style="padding-bottom: 100%" href="/<?=$product[products][url];?>/4/1/<?=$product[products][id];?>">
<img alt="<?=$product[products][title];?>" title="<?=$product[products][title];?>" class="img-responsive" data-src="/image/cache/catalog/18-Gauge-Brad-Nailer-270x270.png"
src="<?=photo::feachJson($product[products][photo],'name',0);?>"/>
</a>
</div>
<div class="caption">
    <div class="name" data-equal-group="2">
    <a href="/<?=$product[products][url];?>/4/1/<?=$product[products][id];?>"><?=$product[products][title];?></a>
    </div>
    <div class="description"><?=$product[products][data];?></div>

    <div class="price">
      <span class="price-new">$60.00</span>
      <span class="price-old">$100.00</span>
    </div>
</div>
<div class="cart-button">
    <button class="product-btn-add" type="button"
    onclick="cart.add('47');">
    <i class="material-design-shopping231"></i>
    <span><?=translate::localWords("Add_to_Cart");?></span>

    </button>
    <div>
    <button class="product-btn" type="button" data-toggle="tooltip"
    title="Add to Wish List"
    onclick="wishlist.add('47');">
    <i class="material-design-favorite22"></i>
  </button>
    <button class="product-btn" type="button" data-toggle="tooltip"
    title="Compare this Product"
    onclick="compare.add('47');"><i
    class="material-design-shuffle24"></i></button>
    </div>
    <div class="rating">
    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
    class="fa fa-star-o fa-stack-2x"></i></span>
    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
    class="fa fa-star-o fa-stack-2x"></i></span>
    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
    class="fa fa-star-o fa-stack-2x"></i></span>
    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
    class="fa fa-star-o fa-stack-2x"></i></span>
    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i
    class="fa fa-star-o fa-stack-2x"></i></span>
    </div>
</div>

</div>
</div>
<?}?>

<div class="row">
  <div class="col-sm-6 text-left">
    <ul class="pagination">
      <li class="active"><span>1</span></li>
      <li><a href="#/">2</a></li>
      <li><a href="#/">&gt;</a></li>
      <li><a href="#/">&gt;|</a></li>
    </ul>
  </div>
  <div class="col-sm-6 text-right">Showing 1 to 6 of 12 (2 Pages)</div>
</div>

</div>
</div>
</div>
