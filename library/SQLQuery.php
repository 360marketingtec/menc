<?php


/**
 * @name : SQLQuery 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : mange database quires  

 * 
 */

class SQLQuery {
	protected static $_dbHandle;
	protected static $_result;
	public static $_table ;
	public static $_extraConditions ;
	protected static $_limit ;
	protected static $_orderBy ;
	protected static $_pagination ;
	public static $_paginationPagesNum ;
	protected static $_paginationItemsInPage;
	public static $_values;
	public static $_neededFields ;
	public static $_showSQL ;
	public static $_leftJoin ;


/**
 * @name : construct 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : auto start method 

 * 
 */

	public function __construct(){
		SQLQuery::connect(loader::$config[hostName], loader::$config[userName], loader::$config[passWord],loader::$config[database]);
	}


/**
 * @name : connect 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : connect to mysql server 

 * 
 */

	public function connect($address, $account, $pwd, $name) {
		SQLQuery::$_dbHandle = @mysql_connect($address, $account, $pwd) or die("Can't connect to MYSQL server");
		if (SQLQuery::$_dbHandle != 0) {
			if (mysql_select_db($name, SQLQuery::$_dbHandle)) {
				 mysql_query("set names utf8"); 
				return 1;
			}else {
				return 0;
			}
		}else {
			return 0;
		}
	}


/**
 * @name : disconnect 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : disconnect from mysql server

 * 
 */

	public function disconnect() {
		@mysql_close(SQLQuery::$_dbHandle) ;
	}

/**
 * @name : fields 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : define the needed fields for the query string eg:" select id,name,email from table"

 * @uses : model::fields("id,name,email"); 
 */    
	public function fields( $neededFields ){
		SQLQuery::$_neededFields = $neededFields ;
		unset($neededFields);
	}

/**
 * @name : selectAll 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : select all rows in table 
 * @		: it also can be merged with the "where - order - limit - etc " conditions  
 * @returns : array [$i][table name][field name]
 * @uses : 
		model::like("search key word");
		foreach(model::selectAll() as $row ){
			echo $row[table name][field] ;
		}

 * 
 */

	public function selectAll() {
		if( ! SQLQuery::$_neededFields ){
			$query = 'SELECT * FROM `'.SQLQuery::$_table[get_called_model($this)].'`';
		}else{
			$query = 'SELECT '. SQLQuery::$_neededFields .' FROM `'.SQLQuery::$_table[get_called_model($this)].'`';
		}
		
		return SQLQuery::query($query);
	}
    
/**
 * @name : select 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : select one row in table 
 * @		: it also can be merged with the "where - order - limit - etc " conditions  
 * @returns : array [table name][field name]
 * @uses : 
		model::where("id",1);
		$row = model::select() ;
		echo $row[table name][field] ;

 * 
 */

	public function select() {
		if( ! SQLQuery::$_neededFields ){
			$query = 'SELECT * FROM `'.SQLQuery::$_table[get_called_model()].'`';
		}else{
			$query = 'SELECT '. SQLQuery::$_neededFields .' FROM `'.SQLQuery::$_table[get_called_model($this)].'`';
		}
		return SQLQuery::query($query, 1);
	}

/**
 * @name : delete 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : delete from table 
 * @		: it also can be merged with the "where - order - limit - etc " conditions  
 * @uses : 
		model::delete($id) ;
 * 
 */

	public function delete($field,$value) {
		$query = 'DELETE FROM '.SQLQuery::$_table[get_called_model($this)] ;
		if( $field && $value ){
			$query .= " WHERE ".$field." = '".mysql_real_escape_string($value)."'";
		}
		unset($id);
		$this->_result = SQLQuery::query($query);
	}

/**
 * @name : where 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : generate a sql query string using WHERE   
 * @		: it also can be merged with the " where - order - select - update - delete- etc " functions   
 * @uses : 
		model::where("id",1);
 * 
 */

	public function where($field, $value,$operation='=') {

		if( SQLQuery::$_extraConditions ){
			SQLQuery::$_extraConditions .= ' AND '.SQLQuery::$_table[get_called_model($this)].".".$field.' '.$operation.' \''.mysql_real_escape_string($value).'\' ';
		}else{
			SQLQuery::$_extraConditions .= ' '.SQLQuery::$_table[get_called_model($this)].".".$field." ".$operation." '".mysql_real_escape_string($value)."'";
		}
		if( strtolower($operation) == 'is' ){
			SQLQuery::$_extraConditions = str_replace( "'".mysql_real_escape_string($value)."'",mysql_real_escape_string($value), SQLQuery::$_extraConditions );
		}
		if( strtolower($operation) == 'in' ){
			SQLQuery::$_extraConditions = SQLQuery::$_table[get_called_model($this)].".".$field . ' in (' . mysql_real_escape_string($value) . ") " ;
		}

		unset($field, $value);
	}

/**
 * @name : like 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : generate a sql query string using LIKE   
 * @		: it also can be merged with the " where - order - select - update - delete- etc " functions   
 * @uses : 
		model::like("search key word");
 * 
 */

	public function like($field, $value) {
		if( SQLQuery::$_extraConditions ){
			SQLQuery::$_extraConditions .= ' AND '.SQLQuery::$_table[get_called_model($this)].".".$field.' LIKE \'%'.mysql_real_escape_string($value).'%\' ';
		}else{
			SQLQuery::$_extraConditions .= ' '.SQLQuery::$_table[get_called_model($this)].".".$field.' LIKE \'%'.mysql_real_escape_string($value).'%\' ';
		}
		unset($field, $value);
	}

/**
 * @name : setLimit 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : generate a sql query string using LIMIT   
 * @		: it also can be merged with the " where - order - select - update - delete- etc " functions   
 * @uses : 
		model::limit(4);
 * 
 */

	public function setLimit($limit) {
		SQLQuery::$_limit = " LIMIT " . mysql_real_escape_string( $limit ) ;
	}

/**
 * @name : setPagination 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : generate a pagination
 * @		: it also can be merged with the " where - order - select - update - delete- etc " functions   
 * @uses : 
		model::setPagination( int items in page , int page number );
		model::select();
		$this->paginationPagesNumPage = model::$_paginationPagesNum  ;

		for ( $i =0 ; $i < $this->paginationPagesNumPage ; $i++ ){
			echo "<a href='/$i'>$i</a> | ";
		}
 * 
 */

	public function setPagination($itemsInPage,$limitStartFrom){
		if( ! $limitStartFrom ) $limitStartFrom = 0 ;
		SQLQuery::$_paginationItemsInPage = $itemsInPage ;
		SQLQuery::$_pagination = " LIMIT " . mysql_real_escape_string( intval($limitStartFrom) * intval($itemsInPage) ) ;
		SQLQuery::$_pagination .= " , " . mysql_real_escape_string( $itemsInPage ) ;
		unset($itemsInPage,$limitStartFrom);
	}

/**
 * @name : orderBy 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : generate a sql query string using order by    
 * @		: it also can be merged with the " where - order - select - update - delete- etc " functions   
 * @uses : 
		model::orderBy("field",ASC or DESC);
		* the default order is ASC
 * 
 */

	public function orderBy($orderBy, $order = '') {
		SQLQuery::$_orderBy = " ORDER BY ". SQLQuery::$_table[get_called_model($this)].".".mysql_real_escape_string( $orderBy ) .' ' . $order ;
		unset( $orderBy, $order );
	}

/**
 * @name : orderByRand 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : generate a sql query string using order by rand
 * @uses : 
		model::orderByRand();
 * 
 */
	public function orderByRand($rand=null) {
		SQLQuery::$_orderBy = " ORDER BY RAND($rand) ";
	}

	public function getNumRows() {
		return mysql_num_rows(SQLQuery::$_result);
	}

    /** Free resources allocated by a query **/

	public function freeResult() {
		mysql_free_result(SQLQuery::$_result);
	}

    /** Get error string **/

	public function getError() {
		return mysql_error(SQLQuery::$_dbHandle);
	}

/**
 * @name : query 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : select one or more row's from table 
 * @		: it also can be merged with the "where - order - limit - etc " conditions  
 * @returns : array [$i][table name][field name]
 * @uses : 
		$sql = "select * from table where id = 123";
		for single result 
			$row = model::query($sql,1) ;
			echo $row[table name][field] ;
	for more one result 
			$sql = "select * from table where title like = 'title'";
			foreach( model::query($sql) as $row ){
				echo $row[table name][title];
			}
 * 
 */

	public function query($query, $singleResult = 0) {
		get_called_model() ;//!!!!!!!///
		if( SQLQuery::$_leftJoin ){
			$query .= SQLQuery::$_leftJoin  ;
		}
		if(SQLQuery::$_extraConditions){
			$query .= " WHERE " . SQLQuery::$_extraConditions ;
		}

		if( SQLQuery::$_orderBy ){
			$query .= SQLQuery::$_orderBy ;
		}
		if(SQLQuery::$_limit){
			$query .= SQLQuery::$_limit ;
		}
		if( SQLQuery::$_pagination ){
			$query = str_replace( SQLQuery::$_limit , '' , $query ) ;
			$query .= SQLQuery::$_pagination ;
		}
		if( SQLQuery::$_showSQL ){
			echo $query ;
		}

	//echo $query ;
	

		SQLQuery::$_result = mysql_query($query, SQLQuery::$_dbHandle) or die(mysql_error().'<br>'.$query) ;

		if( SQLQuery::$_pagination ){
			$paginationResults = mysql_query( str_replace( SQLQuery::$_pagination , '' , $query ) , SQLQuery::$_dbHandle); 
			SQLQuery::$_paginationPagesNum = ceil(mysql_num_rows( $paginationResults ) / SQLQuery::$_paginationItemsInPage ) ;
			mysql_free_result($paginationResults);
		}
		SQLQuery::clear();

		if (preg_match("/select/i",$query)) {
			$result = array();
			$table = array();
			$field = array();
			$tempResults = array();
			$numOfFields = mysql_num_fields(SQLQuery::$_result);
			for ($i = 0; $i < $numOfFields; ++$i) {
				array_push($table,mysql_field_table(SQLQuery::$_result, $i));
				array_push($field,mysql_field_name(SQLQuery::$_result, $i));
			}

			while ($row = mysql_fetch_row(SQLQuery::$_result)) {
				for ($i = 0;$i < $numOfFields; ++$i) {
					$table[$i] = trim($table[$i]);
					$tempResults[$table[$i]][$field[$i]] = $row[$i];
				}
				if ($singleResult == 1) {
		 			mysql_free_result(SQLQuery::$_result);
					return $tempResults;
				}
				array_push($result,$tempResults);
			}
		 	mysql_free_result(SQLQuery::$_result);
			return($result);
		}
		
	}

/**
 * @name : insert 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : generate a sql query string for inserting data    
 * @uses : 
		model::$_values = $_POST;
		model::insert();
 * @return : inserted id 
 * 
 */

	public function insert(){
		foreach( SQLQuery::$_values as $field=>$value ){
			$fields .= '`'.mysql_real_escape_string($field) . '` ,' ;
			$value =  str_replace(  array('<','>') , array('&lt;','&gt;') , $value ) ;
			//echo $value ;
			$values .= "'".mysql_real_escape_string( $value ) . "'," ;
		}
		$query = 'INSERT INTO '.SQLQuery::$_table[get_called_model($this)].' ('.substr($fields , 0 , -1 ).') VALUES ('.substr($values , 0 , -1 ).')';
		SQLQuery::query($query, SQLQuery::$_dbHandle);
		SQLQuery::$_values = null ;
		return mysql_insert_id();
	}

/**
 * @name : update 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2011
 * @todo : generate a sql query string for updating data    
 * @uses : 
		model::where("id",8);
		model::update();
 * 
 */

	public function update(){
		$query = "UPDATE `".SQLQuery::$_table[get_called_model($this)]."` SET  ";
		foreach( SQLQuery::$_values as $field=>$value ){
			$query .= '`'.mysql_real_escape_string($field) . '` = ' . "'" .mysql_real_escape_string(  str_replace(  array('<','>') , array('&#60;','&#62;') , $value ) ) . "' , ";
		}
		$query = substr($query , 0 , -2 ) ;
		$query = str_replace( array('<','>') , array('&#60;','&#62;') , $query );
		SQLQuery::query($query, SQLQuery::$_dbHandle);
		SQLQuery::$_values = null ;
		return true;
	}

	public function clear(){
		//SQLQuery::$_paginationPagesNum = null ;
		SQLQuery::$_neededFields = null ;
		SQLQuery::$_pagination = null ;
		SQLQuery::$_limit = null ;
		SQLQuery::$_orderBy = null ;
		SQLQuery::$_extraConditions = null ;
		SQLQuery::$_values = null ;
		SQLQuery::$_leftJoin = null ;
	}

	public function showSQL(){
		SQLQuery::$_showSQL = true ;
	}

/**
 * @name : leftJoin 
 * @author : Mohamed Hamada 
 * @copyright : eMarketing Egypt 2012
 * @todo : generate a sql query string using JOIN   
 * @		: it also can be merged with the " where - order - select - update - delete- etc " functions   
 * @uses : 
		model::leftJoin("leftTableName","rightTableJoineField","leftTableJoinField");
 * 
 */

	public function leftJoin( $leftTableName , $rightTableJoineField , $leftTableJoinField ) {
		SQLQuery::$_leftJoin .= " LEFT JOIN ".$leftTableName." ON " . SQLQuery::$_table[get_called_model($this)].".".$rightTableJoineField . " = " . $leftTableName.".".$leftTableJoinField ;

		unset( $leftTableName, $rightTableJoineField , $leftTableJoinField );
	}
}


