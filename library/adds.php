<?php

class adds {

	public function showAdd( $placeId ){


		$adds = db_adds::where("place_id",$placeId);
		$adds = db_adds::where("view",'1');
		$adds = db_adds::where("lang_id",loader::$langId);
		$adds = db_adds::selectAll();

		if( ! $adds ){
			return translate::localWords("your_add_here");
		}

		foreach( $adds as $add ){
			$addViewStatistix = db_adds_view_statistics::$_values = array( add_id=>$add[adds][id] , date=>date("Y-m-d G:i:s") );
			$addViewStatistix = db_adds_view_statistics::insert();
		}

		return $adds ;
	}


}
