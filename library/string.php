<?php

class string {

	public function limitWords( $text , $limit ){

		$text = strip_tags( $text , "<strong><a>" );
		
		if( strlen( $text ) > $limit  ){
			$text = substr( $text , 0 , $limit );
			$text = explode( " " , $text );
		

			for( $i = 0 ; $i < ( count($text) - 1 ) ; $i++ ){
				$outputText .= $text[$i] ." ";
			}
		
			unset($text);
			unset($limit);
			return $outputText ;
		}else{
			return $text ;
		}
		

	}


	/**
	* Converts elements divided by newline characters to list items
	* @param String $text
	* @param Array $htmlAttrs
	*/

	public function nl2li($text, array $htmlAttrs = null) {
		if (!empty($htmlAttrs)) {
		$attributes = array_walk($htmlAttrs, $key.' = "'.$value.'"' );

		$openingLi = '<li '.implode(' ', $attributes).'>';
		}else{
			$openingLi = '<li>';
		}

		$parsedText = '';

		$token = strtok($text, "\n");
		while($token !== false) {
			$parsedText .= $openingLi.$token.'</li>'.PHP_EOL;

			$token = strtok("\n");
		}

		return $parsedText;
	}

}
