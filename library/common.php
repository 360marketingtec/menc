<?php

class common {

	public function whyEgyptForTravel(){
		$staticData = db_site_statics::where("marker","why_egypt_for_travel");
		$staticData = db_site_statics::where("lang_id",loader::$langId);
		$staticData	= db_site_statics::select();
		return $staticData[site_statics][data] ;
	}

	public function getStaticCoods($marker){
		$staticData = db_site_statics::where("marker",$marker);
		$staticData = db_site_statics::where("lang_id",loader::$langId);
		$staticData	= db_site_statics::select();
		return $staticData[site_statics][data] ;
	}

	public function getAd($place){
		$ad = db_ads::where('lang_id',loader::$langId);
		$ad = db_ads::where('place',$place);
		$ad = db_ads::select();
		return $ads ;
	}

	public function headerAtricles(){
		
		$articles = db_articles::fields('id,title,url,nofollow,use_full_url');
		$articles = db_articles::where('view',1);
		$articles = db_articles::where("lang_id",loader::$langId);
		$articles = db_articles::where('view_header',1);
		$articles = db_articles::orderBy('order');
		$articles = db_articles::selectAll();

		return $articles ;
	}

	public function footerAtricles(){
		
		$articles = db_articles::fields('id,title,url,nofollow,use_full_url');
		$articles = db_articles::where('view',1);
		$articles = db_articles::where("lang_id",loader::$langId);
		$articles = db_articles::where('view_footer',1);
		$articles = db_articles::orderBy('order');
		$articles = db_articles::selectAll();

		return $articles ;
	}

	public function footerMenu(){
		
		$articles = db_articles::fields('id,title,url,nofollow,use_full_url');
		$articles = db_articles::where('view',1);
		$articles = db_articles::where("lang_id",loader::$langId);
		$articles = db_articles::where('view_footer_menu',1);
		$articles = db_articles::orderBy('order');
		$articles = db_articles::selectAll();

		return $articles ;
	}

	public function navMenuAtricles(){
		
		$articles = db_articles::fields('id,title,url,nofollow,use_full_url');
		$articles = db_articles::where('view',1);
		$articles = db_articles::where("lang_id",loader::$langId);
		$articles = db_articles::where('view_nav_menu',1);
		$articles = db_articles::orderBy('order');
		$articles = db_articles::selectAll();

		return $articles ;
	}

}
