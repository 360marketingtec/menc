<?php

class email {
	public static $_to ;
	public static $_from ;
	public static $_subject ;
	public static $_template ;
	public static $_body ;
	
	public function send(){
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= 'FROM: '.email::$_from ;

		$body = file_get_contents(loader::$config[baseLocation]."/views/email_templates/".email::$_template.".php" );

		foreach( email::$_body as $key=>$mailBody ){
			$body = str_replace("{".$key."}",$mailBody,$body);
		}
//echo $body ;
		if(!mail( email::$_to , email::$_subject, $body, $headers )){
			mail( "mhamada@emarketing-egypt.com" , "email error", "email class error ", $headers );
		}

	}

}
