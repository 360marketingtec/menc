<?php

class menu {

	public function topProductsCatogries(){

		$menuParent = db_companies::where('view',1);
	  	$menuParent = db_companies::where('parent','null','is');
		$menuParent = db_companies::where('lang_id',loader::$langId);
		$menuParent = db_companies::orderBy('order');
		$menuParent = db_companies::select();

		$menu = db_companies::where('view',1);
	  	$menu = db_companies::where('parent',$menuParent[companies][id]);
		$menu = db_companies::where('lang_id',loader::$langId);
		$menu = db_companies::orderBy('order');
		$menu = db_companies::selectAll();
		//print_r($menu);

		return $menu;
	}

	public function categoriesNavMenu( $parent=null ){

		if( $parent ){
			$menu = db_articles_categories::where('parent',$parent);
		}else{
			$menu = db_articles_categories::where('id','10','>');
			$menu = db_articles_categories::where('parent','null','is');
		}
		$menu = db_articles_categories::fields("id,title,url,parent");
		$menu = db_articles_categories::where('view','1');
		$menu = db_articles_categories::where('lang_id',loader::$langId);
		$menu = db_articles_categories::orderBy('order');
		$menu = db_articles_categories::selectAll();

		return $menu ;
	}

	public function menuArticles( $categoryId ){

		$menu = db_articles::where('view',1);
	  	$menu = db_articles::where('category_id',$categoryId);
		$menu = db_articles::where('lang_id',loader::$langId);
		$menu = db_articles::orderBy('order');
		$menu = db_articles::selectAll();

		return $menu ;

	}

	public function navMenu($parent=null){
		global $outbutContent ;

		if( $parent ){
			$menu = db_articles::where('parent',$parent);
		}else{
			$menu = db_articles::where('parent','null','is');
			$menu = db_articles::where('view',1);

			$menu = db_articles::where('lang_id',loader::$langId);
			$menu = db_articles::orderBy('order');
			$menu = db_articles::select();

			$menu = db_articles::where('parent',$menu[articles][id]);
		}
		$menu = db_articles::where('view',1);
		$menu = db_articles::where('lang_id',loader::$langId);
		$menu = db_articles::orderBy('order');
		$menu = db_articles::selectAll();

		foreach( $menu as $menuItem){

				if( $menuItem[articles][use_full_url] ){
				  $url = $menuItem[articles][url] ;
				}else{
				  $url = '/'.$menuItem[articles][url].'/2/0/'.$menuItem[articles][id] ;
				}

			   $outbutContent .= '<li class="has-sub">
								  <a href="'.$url.'" title="'.$menuItem[articles][title].'">
								  '.$menuItem[articles][title].'</a>
								  <ul>';

				menu::navMenu($menuItem[articles][id]);
				$outbutContent .= "</ul></li>";

			}


		$outbutContent = str_replace("<ul></ul>","",$outbutContent);
		return $outbutContent ;
	}


	public function getMenuChilds( $parent=null ){


		if( $parent ){
			$menu = db_articles::where('parent',$parent);
		}else{
			$menu = db_articles::where('parent','null','is');
			$menu = db_articles::where('view',1);

			$menu = db_articles::where('lang_id',loader::$langId);
			$menu = db_articles::orderBy('order');
			$menu = db_articles::select();

			$menu = db_articles::where('parent',$menu[articles][id]);
		}
		$menu = db_articles::where('view',1);
	  //$menu = db_articles::where('view_in_main_menu',1);
		$menu = db_articles::where('lang_id',loader::$langId);
		$menu = db_articles::orderBy('order');
		$menu = db_articles::selectAll();

		return $menu ;

	}

	public function footerMenu($parent=null){
		if( $parent ){
			$menu = db_articles::where('parent',$parent);
		}else{
			$menu = db_articles::where('parent','null','is');
			$menu = db_articles::where('view',1);
			$menu = db_articles::where('lang_id',loader::$langId);
			$menu = db_articles::orderBy('order');
			$menu = db_articles::select();

			$menu = db_articles::where('parent',$menu[articles][id]);
		}
		$menu = db_articles::where('view',1);
	  	$menu = db_articles::where('view_in_footer_menu',1);
		$menu = db_articles::where('lang_id',loader::$langId);
		$menu = db_articles::orderBy('order');
		$menu = db_articles::selectAll();
		return $menu ;
	}


	public function fexedFooter(){

		$this->fexedFooter = db_articles::fields('id,data,photo,parent,lang_id');
		$this->fexedFooter = db_articles::where('view','1');
		$this->fexedFooter = db_articles::where('id','10','<');
		$this->fexedFooter = db_articles::where('parent','null','is');
		$this->fexedFooter = db_articles::where('lang_id',loader::$langId);
		$this->fexedFooter = db_articles::select();

		return $this->fexedFooter[articles][data] ;

	}



}
