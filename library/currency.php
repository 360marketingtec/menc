<?php

/*
	@name : Currency Converter 
	@author : Mohamed Hamada 
	@copyright : 360Marketing 2013

	@How To Use
	echo currency::amount(100);  // 74.02 
	Defult Currency is From USD 
	Save the needed currency on session $_SESSION["currentCurrency"] = "EUR";

*/

class currency {

	public function amount( $amount = 0 ){

		if( ! $_SESSION["currentCurrency"] ){
			$_SESSION["currentCurrency"] = "USD";
		}

		if( $_REQUEST["changeCurrencyTo"] ){
			$_SESSION["currentCurrency"] = $_REQUEST["changeCurrencyTo"] ;
		}

		return currency::convert(intval(trim($amount)) , $_SESSION["currentCurrency"] );

	}

	public function convert( $amount , $outbutCurrency ){

		if( $outbutCurrency == 'USD' ){
			$outbutCurrencyString = '$';
		}elseif( $outbutCurrency == 'EUR' ){
			$outbutCurrencyString = '€';
		}elseif( $outbutCurrency == 'GBP' ){
			$outbutCurrencyString = '£';
		}else{
			$outbutCurrencyString = $outbutCurrency ;
		}

		if($_SESSION['currency_'.$outbutCurrency]){
			return round($amount * $_SESSION['currency_'.$outbutCurrency] ) . " " . $outbutCurrencyString ;
		}else{
			currency::getPersentage($outbutCurrency) ;
			return round($amount * $_SESSION['currency_'.$outbutCurrency]  ) . " " . $outbutCurrencyString ;
		}

	}

	public function getPersentage($outbutCurrency){
		
#		$url = "http://www.google.com/ig/calculator?hl=en&q=1USD=?$outbutCurrency";
#		$result = file_get_contents($url);

#		$result = str_replace("\xA0", ",", $result);
#    	$expl = explode('"', $result);
#    	$expl = explode(' ', $expl[3]);


		$url = "http://rate-exchange.appspot.com/currency?from=USD&to=$outbutCurrency";
		$result = @file_get_contents($url);
		$result = json_decode($result);

		if ( $result->rate == '' ){

			$url = loader::$config[baseLocation]."/public_html/tmp/currency?from=USD&to=$outbutCurrency";
			$result = file_get_contents($url);
			$result = json_decode($result);
			$_SESSION['currency_'.$outbutCurrency]  = $result->rate ;
		    return $result->rate ;

		} else {

			$bestand = loader::$config[baseLocation]."/public_html/tmp/currency?from=USD&to=$outbutCurrency" ;
			$file = fopen($bestand, 'w+') or die('cant open');
			$url = "http://rate-exchange.appspot.com/currency?from=USD&to=$outbutCurrency";
			fwrite($file, file_get_contents($url) );
			fclose($file);
			$_SESSION['currency_'.$outbutCurrency]  = $result->rate ;
		    return $result->rate ;

		}

	}
	
}
