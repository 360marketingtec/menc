<?php

class translate {

	public function localWords( $word ){
		
		if( file_exists(loader::$config[baseLocation]."/public_html/fixed_text/" . loader::$langId . "/" . $word .".txt") ){
			return file_get_contents(loader::$config[baseLocation]."/public_html/fixed_text/" . loader::$langId . "/" . $word .".txt", FILE_USE_INCLUDE_PATH);
		}
		
  //  $word = mysql_real_escape_string($word) ;
		$sql = "SELECT `translation` FROM statec_words_translation ";
		$sql .= " WHERE `word_id` = ( SELECT `id` FROM `statec_words` WHERE word = '" . $word . "')";
		$sql .= " AND lang_id = '" . loader::$langId ."'";
		$sql .= " LIMIT 1";
		$allResult = SQLQuery::query($sql,1) ;					

		if( ! $allResult['statec_words_translation']['translation'] ){
			$sql = "SELECT * FROM `statec_words` ";
			$sql .= " WHERE `word` = '" . mysql_real_escape_string( $word )."'" ;
			$statecWord = SQLQuery::query($sql) ; 
			

			if( count($statecWord) < 1 ){
			
				$sql = "INSERT INTO `statec_words` ( word ) VALUES ( '".mysql_real_escape_string( $word )."')";
				SQLQuery::query($sql) ;

			}
			return $word ;
			
		}
		
		// creat file   
		$myFile = loader::$config[baseLocation]."/public_html/fixed_text/" . loader::$langId . "/" . $word .".txt" ;
		$fh = fopen($myFile, 'w') or die("can't open file");
		fwrite($fh, $allResult['statec_words_translation']['translation']);
		fclose($fh);	
		
		return $allResult['statec_words_translation']['translation'];

	}
	
	public function numbers( $number ){
	
		if( loader::$langId == 2 ){
			$enNumber = array('0','1','2','3','4','5','6','7','8','9');
			$arNumber = array('٠','١','٢','٣','٤','٥','٦','٧','٨','٩');
			
			return  str_replace($enNumber,$arNumber, $number );
		}else{
			return $number ;
		}
		
	}

	public function checkTranslationUpdates(){
		$sql ="
			SELECT UPDATE_TIME 
			FROM information_schema.tables 
			WHERE TABLE_SCHEMA = '".loader::$config[database]."' 
			AND TABLE_NAME = 'statec_words_translation' 
		";
		$updatedDate = SQLQuery::query($sql,1);
		//echo $updatedDate[tables][UPDATE_TIME] ;

		$savedDate = file_get_contents(loader::$config[baseLocation]."/public_html/fixed_text/" . loader::$langId . "/apdate_date.txt");
		//echo $savedDate ;

		if( $updatedDate[tables][UPDATE_TIME] != $savedDate ){
			$dir = loader::$config[baseLocation]."/public_html/fixed_text/" . loader::$langId . "/" ;
			$translationDir = opendir($dir);

			while(false !== ($file = readdir($translationDir))) {
				if($file != "." && $file != "..") {
					unlink($dir.$file) ;
				}
			}
			closedir($translationDir);

			$myFile = loader::$config[baseLocation]."/public_html/fixed_text/" . loader::$langId . "/apdate_date.txt" ;
			$fh = fopen($myFile, 'w') or die("can't open file");
			fwrite($fh, $updatedDate[tables][UPDATE_TIME]);
			fclose($fh);	
		}
	}

}
