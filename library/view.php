<?php

class view {

	public function renderLayout(){
		include_once loader::$config[baseLocation].'/views/layouts/'.loader::$config[layout].'/home.php';
	}

	public function renderContentView( $requestedView = null ){

		if( $requestedView ){
			include_once loader::$config[baseLocation]."/views/".loader::$requestedController."/".$requestedView.".php"; 
		}else{
			include_once loader::$config[baseLocation]."/views/".loader::$requestedController."/".loader::$requestedAction[intval(loader::$urlInfo[count(loader::$urlInfo)-2])].".php"; 
		}
	}
	

}
