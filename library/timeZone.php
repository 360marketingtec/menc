<?php
class timeZone {

	function getCorrectTime($dbDate){

		$dateToTime = strtotime($dbDate);
		$diffToTime = $_SESSION['currentTimeZone']*60*60;
		$total = intval($dateToTime + $diffToTime);
		$dayTime = translate::localWords(date(" a " , $total ));
			
		$time = translate::numbers(date( "  g:i " ,  $total ) );
		$year = translate::numbers( date( "  Y " ,  $total ) );
		$day = translate::numbers( date( " j " ,  $total ) );
		
		return "<div class='GDates'><ul> <li>" . $dayTime . "</li><li> - " . $time.  "</li><li>". $year . "  </li><li class='GDatesMonth'>" .  translate::localWords(date( " F " , $total )) . "  </li><li> ". $day . "  </li><li>" . translate::localWords(date( " l " ,  $total )) . " </li></ul></div>";		 
	}
}

