<?php
require_once(getabspath("classes/cipherer.php"));




$tdatasite_statics_markers = array();	
	$tdatasite_statics_markers[".truncateText"] = true;
	$tdatasite_statics_markers[".NumberOfChars"] = 80; 
	$tdatasite_statics_markers[".ShortName"] = "site_statics_markers";
	$tdatasite_statics_markers[".OwnerID"] = "";
	$tdatasite_statics_markers[".OriginalTable"] = "site_statics_markers";

//	field labels
$fieldLabelssite_statics_markers = array();
$fieldToolTipssite_statics_markers = array();
$pageTitlessite_statics_markers = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelssite_statics_markers["English"] = array();
	$fieldToolTipssite_statics_markers["English"] = array();
	$pageTitlessite_statics_markers["English"] = array();
	$fieldLabelssite_statics_markers["English"]["id"] = "Id";
	$fieldToolTipssite_statics_markers["English"]["id"] = "";
	$fieldLabelssite_statics_markers["English"]["name"] = "Name";
	$fieldToolTipssite_statics_markers["English"]["name"] = "";
	if (count($fieldToolTipssite_statics_markers["English"]))
		$tdatasite_statics_markers[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelssite_statics_markers["Arabic"] = array();
	$fieldToolTipssite_statics_markers["Arabic"] = array();
	$pageTitlessite_statics_markers["Arabic"] = array();
	$fieldLabelssite_statics_markers["Arabic"]["id"] = "Id";
	$fieldToolTipssite_statics_markers["Arabic"]["id"] = "";
	$fieldLabelssite_statics_markers["Arabic"]["name"] = "Name";
	$fieldToolTipssite_statics_markers["Arabic"]["name"] = "";
	if (count($fieldToolTipssite_statics_markers["Arabic"]))
		$tdatasite_statics_markers[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelssite_statics_markers[""] = array();
	$fieldToolTipssite_statics_markers[""] = array();
	$pageTitlessite_statics_markers[""] = array();
	$fieldLabelssite_statics_markers[""]["id"] = "Id";
	$fieldToolTipssite_statics_markers[""]["id"] = "";
	$fieldLabelssite_statics_markers[""]["name"] = "Name";
	$fieldToolTipssite_statics_markers[""]["name"] = "";
	if (count($fieldToolTipssite_statics_markers[""]))
		$tdatasite_statics_markers[".isUseToolTips"] = true;
}
	
	
	$tdatasite_statics_markers[".NCSearch"] = true;



$tdatasite_statics_markers[".shortTableName"] = "site_statics_markers";
$tdatasite_statics_markers[".nSecOptions"] = 0;
$tdatasite_statics_markers[".recsPerRowList"] = 1;
$tdatasite_statics_markers[".mainTableOwnerID"] = "";
$tdatasite_statics_markers[".moveNext"] = 1;
$tdatasite_statics_markers[".nType"] = 0;

$tdatasite_statics_markers[".strOriginalTableName"] = "site_statics_markers";




$tdatasite_statics_markers[".showAddInPopup"] = false;

$tdatasite_statics_markers[".showEditInPopup"] = false;

$tdatasite_statics_markers[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatasite_statics_markers[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatasite_statics_markers[".fieldsForRegister"] = array();

$tdatasite_statics_markers[".listAjax"] = false;

	$tdatasite_statics_markers[".audit"] = false;

	$tdatasite_statics_markers[".locking"] = false;


$tdatasite_statics_markers[".list"] = true;

$tdatasite_statics_markers[".view"] = true;





$tdatasite_statics_markers[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatasite_statics_markers[".searchSaving"] = false;
//

$tdatasite_statics_markers[".showSearchPanel"] = true;
		$tdatasite_statics_markers[".flexibleSearch"] = true;		

if (isMobile())
	$tdatasite_statics_markers[".isUseAjaxSuggest"] = false;
else 
	$tdatasite_statics_markers[".isUseAjaxSuggest"] = true;

$tdatasite_statics_markers[".rowHighlite"] = true;



$tdatasite_statics_markers[".addPageEvents"] = false;

// use timepicker for search panel
$tdatasite_statics_markers[".isUseTimeForSearch"] = false;



$tdatasite_statics_markers[".useDetailsPreview"] = true;


$tdatasite_statics_markers[".allSearchFields"] = array();
$tdatasite_statics_markers[".filterFields"] = array();
$tdatasite_statics_markers[".requiredSearchFields"] = array();

$tdatasite_statics_markers[".allSearchFields"][] = "id";
	$tdatasite_statics_markers[".allSearchFields"][] = "name";
	

$tdatasite_statics_markers[".googleLikeFields"] = array();
$tdatasite_statics_markers[".googleLikeFields"][] = "id";
$tdatasite_statics_markers[".googleLikeFields"][] = "name";


$tdatasite_statics_markers[".advSearchFields"] = array();
$tdatasite_statics_markers[".advSearchFields"][] = "id";
$tdatasite_statics_markers[".advSearchFields"][] = "name";

$tdatasite_statics_markers[".tableType"] = "list";

$tdatasite_statics_markers[".printerPageOrientation"] = 0;
$tdatasite_statics_markers[".nPrinterPageScale"] = 100;

$tdatasite_statics_markers[".nPrinterSplitRecords"] = 40;

$tdatasite_statics_markers[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdatasite_statics_markers[".pageSize"] = 20;

$tdatasite_statics_markers[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatasite_statics_markers[".strOrderBy"] = $tstrOrderBy;

$tdatasite_statics_markers[".orderindexes"] = array();

$tdatasite_statics_markers[".sqlHead"] = "SELECT id,  	name";
$tdatasite_statics_markers[".sqlFrom"] = "FROM site_statics_markers";
$tdatasite_statics_markers[".sqlWhereExpr"] = "";
$tdatasite_statics_markers[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatasite_statics_markers[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatasite_statics_markers[".arrGroupsPerPage"] = $arrGPP;

$tdatasite_statics_markers[".highlightSearchResults"] = true;

$tableKeyssite_statics_markers = array();
$tableKeyssite_statics_markers[] = "id";
$tdatasite_statics_markers[".Keys"] = $tableKeyssite_statics_markers;

$tdatasite_statics_markers[".listFields"] = array();
$tdatasite_statics_markers[".listFields"][] = "id";
$tdatasite_statics_markers[".listFields"][] = "name";

$tdatasite_statics_markers[".hideMobileList"] = array();


$tdatasite_statics_markers[".viewFields"] = array();
$tdatasite_statics_markers[".viewFields"][] = "id";
$tdatasite_statics_markers[".viewFields"][] = "name";

$tdatasite_statics_markers[".addFields"] = array();

$tdatasite_statics_markers[".inlineAddFields"] = array();

$tdatasite_statics_markers[".editFields"] = array();

$tdatasite_statics_markers[".inlineEditFields"] = array();

$tdatasite_statics_markers[".exportFields"] = array();

$tdatasite_statics_markers[".importFields"] = array();

$tdatasite_statics_markers[".printFields"] = array();

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "site_statics_markers";
	$fdata["Label"] = GetFieldLabel("site_statics_markers","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatasite_statics_markers["id"] = $fdata;
//	name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "name";
	$fdata["GoodName"] = "name";
	$fdata["ownerTable"] = "site_statics_markers";
	$fdata["Label"] = GetFieldLabel("site_statics_markers","name"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "name"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "name";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatasite_statics_markers["name"] = $fdata;

	
$tables_data["site_statics_markers"]=&$tdatasite_statics_markers;
$field_labels["site_statics_markers"] = &$fieldLabelssite_statics_markers;
$fieldToolTips["site_statics_markers"] = &$fieldToolTipssite_statics_markers;
$page_titles["site_statics_markers"] = &$pageTitlessite_statics_markers;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["site_statics_markers"] = array();
//	site_statics
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="site_statics";
		$detailsParam["dOriginalTable"] = "site_statics";
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "site_statics";
	$detailsParam["dCaptionTable"] = GetTableCaption("site_statics");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();
	$detailsParam["dispChildCount"] = "1";
		$detailsParam["hideChild"] = false;
	$detailsParam["previewOnList"] = 1;
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
			
	$detailsTablesData["site_statics_markers"][$dIndex] = $detailsParam;
	
		
		$detailsTablesData["site_statics_markers"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["site_statics_markers"][$dIndex]["masterKeys"][]="name";

				$detailsTablesData["site_statics_markers"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["site_statics_markers"][$dIndex]["detailKeys"][]="marker";
	
// tables which are master tables for current table (detail)
$masterTablesData["site_statics_markers"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_site_statics_markers()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	name";
$proto0["m_strFrom"] = "FROM site_statics_markers";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "site_statics_markers",
	"m_srcTableName" => "site_statics_markers"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "site_statics_markers";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "name",
	"m_strTable" => "site_statics_markers",
	"m_srcTableName" => "site_statics_markers"
));

$proto7["m_sql"] = "name";
$proto7["m_srcTableName"] = "site_statics_markers";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto9=array();
$proto9["m_link"] = "SQLL_MAIN";
			$proto10=array();
$proto10["m_strName"] = "site_statics_markers";
$proto10["m_srcTableName"] = "site_statics_markers";
$proto10["m_columns"] = array();
$proto10["m_columns"][] = "id";
$proto10["m_columns"][] = "name";
$obj = new SQLTable($proto10);

$proto9["m_table"] = $obj;
$proto9["m_sql"] = "site_statics_markers";
$proto9["m_alias"] = "";
$proto9["m_srcTableName"] = "site_statics_markers";
$proto11=array();
$proto11["m_sql"] = "";
$proto11["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto11["m_column"]=$obj;
$proto11["m_contained"] = array();
$proto11["m_strCase"] = "";
$proto11["m_havingmode"] = false;
$proto11["m_inBrackets"] = false;
$proto11["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto11);

$proto9["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto9);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="site_statics_markers";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_site_statics_markers = createSqlQuery_site_statics_markers();


	
		
	
$tdatasite_statics_markers[".sqlquery"] = $queryData_site_statics_markers;

$tableEvents["site_statics_markers"] = new eventsBase;
$tdatasite_statics_markers[".hasEvents"] = false;

?>