<?php
require_once(getabspath("classes/cipherer.php"));




$tdatarequests = array();	
	$tdatarequests[".truncateText"] = true;
	$tdatarequests[".NumberOfChars"] = 80; 
	$tdatarequests[".ShortName"] = "requests";
	$tdatarequests[".OwnerID"] = "";
	$tdatarequests[".OriginalTable"] = "requests";

//	field labels
$fieldLabelsrequests = array();
$fieldToolTipsrequests = array();
$pageTitlesrequests = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsrequests["English"] = array();
	$fieldToolTipsrequests["English"] = array();
	$pageTitlesrequests["English"] = array();
	$fieldLabelsrequests["English"]["id"] = "Id";
	$fieldToolTipsrequests["English"]["id"] = "";
	$fieldLabelsrequests["English"]["name"] = "Name";
	$fieldToolTipsrequests["English"]["name"] = "";
	$fieldLabelsrequests["English"]["phone"] = "Phone";
	$fieldToolTipsrequests["English"]["phone"] = "";
	$fieldLabelsrequests["English"]["inquery"] = "Inquery";
	$fieldToolTipsrequests["English"]["inquery"] = "";
	$fieldLabelsrequests["English"]["email"] = "Email";
	$fieldToolTipsrequests["English"]["email"] = "";
	$fieldLabelsrequests["English"]["date"] = "Date";
	$fieldToolTipsrequests["English"]["date"] = "";
	$fieldLabelsrequests["English"]["ip"] = "Ip";
	$fieldToolTipsrequests["English"]["ip"] = "";
	$fieldLabelsrequests["English"]["referer"] = "Referer";
	$fieldToolTipsrequests["English"]["referer"] = "";
	$fieldLabelsrequests["English"]["url"] = "Url";
	$fieldToolTipsrequests["English"]["url"] = "";
	if (count($fieldToolTipsrequests["English"]))
		$tdatarequests[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelsrequests["Arabic"] = array();
	$fieldToolTipsrequests["Arabic"] = array();
	$pageTitlesrequests["Arabic"] = array();
	$fieldLabelsrequests["Arabic"]["id"] = "Id";
	$fieldToolTipsrequests["Arabic"]["id"] = "";
	$fieldLabelsrequests["Arabic"]["name"] = "Name";
	$fieldToolTipsrequests["Arabic"]["name"] = "";
	$fieldLabelsrequests["Arabic"]["phone"] = "Phone";
	$fieldToolTipsrequests["Arabic"]["phone"] = "";
	$fieldLabelsrequests["Arabic"]["inquery"] = "Inquery";
	$fieldToolTipsrequests["Arabic"]["inquery"] = "";
	$fieldLabelsrequests["Arabic"]["email"] = "Email";
	$fieldToolTipsrequests["Arabic"]["email"] = "";
	$fieldLabelsrequests["Arabic"]["date"] = "Date";
	$fieldToolTipsrequests["Arabic"]["date"] = "";
	$fieldLabelsrequests["Arabic"]["ip"] = "Ip";
	$fieldToolTipsrequests["Arabic"]["ip"] = "";
	$fieldLabelsrequests["Arabic"]["referer"] = "Referer";
	$fieldToolTipsrequests["Arabic"]["referer"] = "";
	$fieldLabelsrequests["Arabic"]["url"] = "Url";
	$fieldToolTipsrequests["Arabic"]["url"] = "";
	if (count($fieldToolTipsrequests["Arabic"]))
		$tdatarequests[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsrequests[""] = array();
	$fieldToolTipsrequests[""] = array();
	$pageTitlesrequests[""] = array();
	$fieldLabelsrequests[""]["id"] = "Id";
	$fieldToolTipsrequests[""]["id"] = "";
	$fieldLabelsrequests[""]["name"] = "Name";
	$fieldToolTipsrequests[""]["name"] = "";
	$fieldLabelsrequests[""]["phone"] = "Phone";
	$fieldToolTipsrequests[""]["phone"] = "";
	$fieldLabelsrequests[""]["inquery"] = "Inquery";
	$fieldToolTipsrequests[""]["inquery"] = "";
	$fieldLabelsrequests[""]["email"] = "Email";
	$fieldToolTipsrequests[""]["email"] = "";
	$fieldLabelsrequests[""]["date"] = "Date";
	$fieldToolTipsrequests[""]["date"] = "";
	$fieldLabelsrequests[""]["ip"] = "Ip";
	$fieldToolTipsrequests[""]["ip"] = "";
	$fieldLabelsrequests[""]["referer"] = "Referer";
	$fieldToolTipsrequests[""]["referer"] = "";
	$fieldLabelsrequests[""]["url"] = "Url";
	$fieldToolTipsrequests[""]["url"] = "";
	if (count($fieldToolTipsrequests[""]))
		$tdatarequests[".isUseToolTips"] = true;
}
	
	
	$tdatarequests[".NCSearch"] = true;



$tdatarequests[".shortTableName"] = "requests";
$tdatarequests[".nSecOptions"] = 0;
$tdatarequests[".recsPerRowList"] = 1;
$tdatarequests[".mainTableOwnerID"] = "";
$tdatarequests[".moveNext"] = 1;
$tdatarequests[".nType"] = 0;

$tdatarequests[".strOriginalTableName"] = "requests";




$tdatarequests[".showAddInPopup"] = false;

$tdatarequests[".showEditInPopup"] = false;

$tdatarequests[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatarequests[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatarequests[".fieldsForRegister"] = array();

$tdatarequests[".listAjax"] = false;

	$tdatarequests[".audit"] = false;

	$tdatarequests[".locking"] = false;


$tdatarequests[".list"] = true;

$tdatarequests[".view"] = true;


$tdatarequests[".exportTo"] = true;

$tdatarequests[".printFriendly"] = true;

$tdatarequests[".delete"] = true;

$tdatarequests[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatarequests[".searchSaving"] = false;
//

$tdatarequests[".showSearchPanel"] = true;
		$tdatarequests[".flexibleSearch"] = true;		

if (isMobile())
	$tdatarequests[".isUseAjaxSuggest"] = false;
else 
	$tdatarequests[".isUseAjaxSuggest"] = true;

$tdatarequests[".rowHighlite"] = true;



$tdatarequests[".addPageEvents"] = false;

// use timepicker for search panel
$tdatarequests[".isUseTimeForSearch"] = false;





$tdatarequests[".allSearchFields"] = array();
$tdatarequests[".filterFields"] = array();
$tdatarequests[".requiredSearchFields"] = array();

$tdatarequests[".allSearchFields"][] = "id";
	$tdatarequests[".allSearchFields"][] = "name";
	$tdatarequests[".allSearchFields"][] = "phone";
	$tdatarequests[".allSearchFields"][] = "inquery";
	$tdatarequests[".allSearchFields"][] = "email";
	$tdatarequests[".allSearchFields"][] = "date";
	$tdatarequests[".allSearchFields"][] = "ip";
	$tdatarequests[".allSearchFields"][] = "referer";
	$tdatarequests[".allSearchFields"][] = "url";
	

$tdatarequests[".googleLikeFields"] = array();
$tdatarequests[".googleLikeFields"][] = "id";
$tdatarequests[".googleLikeFields"][] = "name";
$tdatarequests[".googleLikeFields"][] = "phone";
$tdatarequests[".googleLikeFields"][] = "inquery";
$tdatarequests[".googleLikeFields"][] = "email";
$tdatarequests[".googleLikeFields"][] = "date";
$tdatarequests[".googleLikeFields"][] = "ip";
$tdatarequests[".googleLikeFields"][] = "referer";
$tdatarequests[".googleLikeFields"][] = "url";


$tdatarequests[".advSearchFields"] = array();
$tdatarequests[".advSearchFields"][] = "id";
$tdatarequests[".advSearchFields"][] = "name";
$tdatarequests[".advSearchFields"][] = "phone";
$tdatarequests[".advSearchFields"][] = "inquery";
$tdatarequests[".advSearchFields"][] = "email";
$tdatarequests[".advSearchFields"][] = "date";
$tdatarequests[".advSearchFields"][] = "ip";
$tdatarequests[".advSearchFields"][] = "referer";
$tdatarequests[".advSearchFields"][] = "url";

$tdatarequests[".tableType"] = "list";

$tdatarequests[".printerPageOrientation"] = 0;
$tdatarequests[".nPrinterPageScale"] = 100;

$tdatarequests[".nPrinterSplitRecords"] = 40;

$tdatarequests[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdatarequests[".pageSize"] = 20;

$tdatarequests[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatarequests[".strOrderBy"] = $tstrOrderBy;

$tdatarequests[".orderindexes"] = array();

$tdatarequests[".sqlHead"] = "SELECT id,  	name,  	phone,  	inquery,  	email,  	`date`,  	ip,  	referer,  	url";
$tdatarequests[".sqlFrom"] = "FROM requests";
$tdatarequests[".sqlWhereExpr"] = "";
$tdatarequests[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatarequests[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatarequests[".arrGroupsPerPage"] = $arrGPP;

$tdatarequests[".highlightSearchResults"] = true;

$tableKeysrequests = array();
$tableKeysrequests[] = "id";
$tdatarequests[".Keys"] = $tableKeysrequests;

$tdatarequests[".listFields"] = array();
$tdatarequests[".listFields"][] = "id";
$tdatarequests[".listFields"][] = "name";
$tdatarequests[".listFields"][] = "phone";
$tdatarequests[".listFields"][] = "inquery";
$tdatarequests[".listFields"][] = "email";
$tdatarequests[".listFields"][] = "date";
$tdatarequests[".listFields"][] = "ip";
$tdatarequests[".listFields"][] = "referer";
$tdatarequests[".listFields"][] = "url";

$tdatarequests[".hideMobileList"] = array();


$tdatarequests[".viewFields"] = array();
$tdatarequests[".viewFields"][] = "id";
$tdatarequests[".viewFields"][] = "name";
$tdatarequests[".viewFields"][] = "phone";
$tdatarequests[".viewFields"][] = "inquery";
$tdatarequests[".viewFields"][] = "email";
$tdatarequests[".viewFields"][] = "date";
$tdatarequests[".viewFields"][] = "ip";
$tdatarequests[".viewFields"][] = "referer";
$tdatarequests[".viewFields"][] = "url";

$tdatarequests[".addFields"] = array();

$tdatarequests[".inlineAddFields"] = array();

$tdatarequests[".editFields"] = array();

$tdatarequests[".inlineEditFields"] = array();

$tdatarequests[".exportFields"] = array();
$tdatarequests[".exportFields"][] = "id";
$tdatarequests[".exportFields"][] = "name";
$tdatarequests[".exportFields"][] = "phone";
$tdatarequests[".exportFields"][] = "inquery";
$tdatarequests[".exportFields"][] = "email";
$tdatarequests[".exportFields"][] = "date";
$tdatarequests[".exportFields"][] = "ip";
$tdatarequests[".exportFields"][] = "referer";
$tdatarequests[".exportFields"][] = "url";

$tdatarequests[".importFields"] = array();

$tdatarequests[".printFields"] = array();
$tdatarequests[".printFields"][] = "id";
$tdatarequests[".printFields"][] = "name";
$tdatarequests[".printFields"][] = "phone";
$tdatarequests[".printFields"][] = "inquery";
$tdatarequests[".printFields"][] = "email";
$tdatarequests[".printFields"][] = "date";
$tdatarequests[".printFields"][] = "ip";
$tdatarequests[".printFields"][] = "referer";
$tdatarequests[".printFields"][] = "url";

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "requests";
	$fdata["Label"] = GetFieldLabel("requests","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatarequests["id"] = $fdata;
//	name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "name";
	$fdata["GoodName"] = "name";
	$fdata["ownerTable"] = "requests";
	$fdata["Label"] = GetFieldLabel("requests","name"); 
	$fdata["FieldType"] = 200;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "name"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "name";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";
	
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatarequests["name"] = $fdata;
//	phone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "phone";
	$fdata["GoodName"] = "phone";
	$fdata["ownerTable"] = "requests";
	$fdata["Label"] = GetFieldLabel("requests","phone"); 
	$fdata["FieldType"] = 200;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "phone"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "phone";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=200";
	
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatarequests["phone"] = $fdata;
//	inquery
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "inquery";
	$fdata["GoodName"] = "inquery";
	$fdata["ownerTable"] = "requests";
	$fdata["Label"] = GetFieldLabel("requests","inquery"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "inquery"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "inquery";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatarequests["inquery"] = $fdata;
//	email
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "email";
	$fdata["GoodName"] = "email";
	$fdata["ownerTable"] = "requests";
	$fdata["Label"] = GetFieldLabel("requests","email"); 
	$fdata["FieldType"] = 200;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "email"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "email";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "email";
	
		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";
	
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatarequests["email"] = $fdata;
//	date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "date";
	$fdata["GoodName"] = "date";
	$fdata["ownerTable"] = "requests";
	$fdata["Label"] = GetFieldLabel("requests","date"); 
	$fdata["FieldType"] = 135;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "date"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`date`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Short Date");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Date");
	
		$edata["ShowTime"] = true; 
		
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		$edata["DateEditType"] = 13; 
	$edata["InitialYearFactor"] = 100; 
	$edata["LastYearFactor"] = 10; 
	
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatarequests["date"] = $fdata;
//	ip
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "ip";
	$fdata["GoodName"] = "ip";
	$fdata["ownerTable"] = "requests";
	$fdata["Label"] = GetFieldLabel("requests","ip"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "ip"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ip";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatarequests["ip"] = $fdata;
//	referer
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "referer";
	$fdata["GoodName"] = "referer";
	$fdata["ownerTable"] = "requests";
	$fdata["Label"] = GetFieldLabel("requests","referer"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "referer"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "referer";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatarequests["referer"] = $fdata;
//	url
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "url";
	$fdata["GoodName"] = "url";
	$fdata["ownerTable"] = "requests";
	$fdata["Label"] = GetFieldLabel("requests","url"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "url"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "url";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatarequests["url"] = $fdata;

	
$tables_data["requests"]=&$tdatarequests;
$field_labels["requests"] = &$fieldLabelsrequests;
$fieldToolTips["requests"] = &$fieldToolTipsrequests;
$page_titles["requests"] = &$pageTitlesrequests;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["requests"] = array();
	
// tables which are master tables for current table (detail)
$masterTablesData["requests"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_requests()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	name,  	phone,  	inquery,  	email,  	`date`,  	ip,  	referer,  	url";
$proto0["m_strFrom"] = "FROM requests";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "requests",
	"m_srcTableName" => "requests"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "requests";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "name",
	"m_strTable" => "requests",
	"m_srcTableName" => "requests"
));

$proto7["m_sql"] = "name";
$proto7["m_srcTableName"] = "requests";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "phone",
	"m_strTable" => "requests",
	"m_srcTableName" => "requests"
));

$proto9["m_sql"] = "phone";
$proto9["m_srcTableName"] = "requests";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "inquery",
	"m_strTable" => "requests",
	"m_srcTableName" => "requests"
));

$proto11["m_sql"] = "inquery";
$proto11["m_srcTableName"] = "requests";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "email",
	"m_strTable" => "requests",
	"m_srcTableName" => "requests"
));

$proto13["m_sql"] = "email";
$proto13["m_srcTableName"] = "requests";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "date",
	"m_strTable" => "requests",
	"m_srcTableName" => "requests"
));

$proto15["m_sql"] = "`date`";
$proto15["m_srcTableName"] = "requests";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "ip",
	"m_strTable" => "requests",
	"m_srcTableName" => "requests"
));

$proto17["m_sql"] = "ip";
$proto17["m_srcTableName"] = "requests";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "referer",
	"m_strTable" => "requests",
	"m_srcTableName" => "requests"
));

$proto19["m_sql"] = "referer";
$proto19["m_srcTableName"] = "requests";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "url",
	"m_strTable" => "requests",
	"m_srcTableName" => "requests"
));

$proto21["m_sql"] = "url";
$proto21["m_srcTableName"] = "requests";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto23=array();
$proto23["m_link"] = "SQLL_MAIN";
			$proto24=array();
$proto24["m_strName"] = "requests";
$proto24["m_srcTableName"] = "requests";
$proto24["m_columns"] = array();
$proto24["m_columns"][] = "id";
$proto24["m_columns"][] = "name";
$proto24["m_columns"][] = "phone";
$proto24["m_columns"][] = "inquery";
$proto24["m_columns"][] = "email";
$proto24["m_columns"][] = "date";
$proto24["m_columns"][] = "ip";
$proto24["m_columns"][] = "referer";
$proto24["m_columns"][] = "url";
$obj = new SQLTable($proto24);

$proto23["m_table"] = $obj;
$proto23["m_sql"] = "requests";
$proto23["m_alias"] = "";
$proto23["m_srcTableName"] = "requests";
$proto25=array();
$proto25["m_sql"] = "";
$proto25["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto25["m_column"]=$obj;
$proto25["m_contained"] = array();
$proto25["m_strCase"] = "";
$proto25["m_havingmode"] = false;
$proto25["m_inBrackets"] = false;
$proto25["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto25);

$proto23["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto23);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="requests";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_requests = createSqlQuery_requests();


	
									
	
$tdatarequests[".sqlquery"] = $queryData_requests;

$tableEvents["requests"] = new eventsBase;
$tdatarequests[".hasEvents"] = false;

?>