<?php
require_once(getabspath("classes/cipherer.php"));




$tdataevents = array();	
	$tdataevents[".truncateText"] = true;
	$tdataevents[".NumberOfChars"] = 80; 
	$tdataevents[".ShortName"] = "events";
	$tdataevents[".OwnerID"] = "";
	$tdataevents[".OriginalTable"] = "events";

//	field labels
$fieldLabelsevents = array();
$fieldToolTipsevents = array();
$pageTitlesevents = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsevents["English"] = array();
	$fieldToolTipsevents["English"] = array();
	$pageTitlesevents["English"] = array();
	$fieldLabelsevents["English"]["id"] = "Id";
	$fieldToolTipsevents["English"]["id"] = "";
	$fieldLabelsevents["English"]["lang_id"] = "Lang Id";
	$fieldToolTipsevents["English"]["lang_id"] = "";
	$fieldLabelsevents["English"]["view"] = "View";
	$fieldToolTipsevents["English"]["view"] = "";
	$fieldLabelsevents["English"]["title"] = "Title";
	$fieldToolTipsevents["English"]["title"] = "";
	$fieldLabelsevents["English"]["data"] = "Data";
	$fieldToolTipsevents["English"]["data"] = "";
	$fieldLabelsevents["English"]["meta_title"] = "Meta Title";
	$fieldToolTipsevents["English"]["meta_title"] = "";
	$fieldLabelsevents["English"]["meta_keywords"] = "Meta Keywords";
	$fieldToolTipsevents["English"]["meta_keywords"] = "";
	$fieldLabelsevents["English"]["date"] = "Date";
	$fieldToolTipsevents["English"]["date"] = "";
	$fieldLabelsevents["English"]["photo"] = "Photo";
	$fieldToolTipsevents["English"]["photo"] = "";
	$fieldLabelsevents["English"]["meta_desc"] = "Meta Desc";
	$fieldToolTipsevents["English"]["meta_desc"] = "";
	$fieldLabelsevents["English"]["hot_event"] = "Hot Event";
	$fieldToolTipsevents["English"]["hot_event"] = "";
	$fieldLabelsevents["English"]["url"] = "Url";
	$fieldToolTipsevents["English"]["url"] = "";
	if (count($fieldToolTipsevents["English"]))
		$tdataevents[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelsevents["Arabic"] = array();
	$fieldToolTipsevents["Arabic"] = array();
	$pageTitlesevents["Arabic"] = array();
	$fieldLabelsevents["Arabic"]["id"] = "Id";
	$fieldToolTipsevents["Arabic"]["id"] = "";
	$fieldLabelsevents["Arabic"]["lang_id"] = "Lang Id";
	$fieldToolTipsevents["Arabic"]["lang_id"] = "";
	$fieldLabelsevents["Arabic"]["view"] = "View";
	$fieldToolTipsevents["Arabic"]["view"] = "";
	$fieldLabelsevents["Arabic"]["title"] = "Title";
	$fieldToolTipsevents["Arabic"]["title"] = "";
	$fieldLabelsevents["Arabic"]["data"] = "Data";
	$fieldToolTipsevents["Arabic"]["data"] = "";
	$fieldLabelsevents["Arabic"]["meta_title"] = "Meta Title";
	$fieldToolTipsevents["Arabic"]["meta_title"] = "";
	$fieldLabelsevents["Arabic"]["meta_keywords"] = "Meta Keywords";
	$fieldToolTipsevents["Arabic"]["meta_keywords"] = "";
	$fieldLabelsevents["Arabic"]["date"] = "Date";
	$fieldToolTipsevents["Arabic"]["date"] = "";
	$fieldLabelsevents["Arabic"]["photo"] = "Photo";
	$fieldToolTipsevents["Arabic"]["photo"] = "";
	$fieldLabelsevents["Arabic"]["meta_desc"] = "Meta Desc";
	$fieldToolTipsevents["Arabic"]["meta_desc"] = "";
	$fieldLabelsevents["Arabic"]["hot_event"] = "Hot Event";
	$fieldToolTipsevents["Arabic"]["hot_event"] = "";
	$fieldLabelsevents["Arabic"]["url"] = "Url";
	$fieldToolTipsevents["Arabic"]["url"] = "";
	if (count($fieldToolTipsevents["Arabic"]))
		$tdataevents[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsevents[""] = array();
	$fieldToolTipsevents[""] = array();
	$pageTitlesevents[""] = array();
	$fieldLabelsevents[""]["photo"] = "Photo";
	$fieldToolTipsevents[""]["photo"] = "";
	$fieldLabelsevents[""]["meta_desc"] = "Meta Desc";
	$fieldToolTipsevents[""]["meta_desc"] = "";
	$fieldLabelsevents[""]["hot_event"] = "Hot Event";
	$fieldToolTipsevents[""]["hot_event"] = "";
	$fieldLabelsevents[""]["url"] = "Url";
	$fieldToolTipsevents[""]["url"] = "";
	if (count($fieldToolTipsevents[""]))
		$tdataevents[".isUseToolTips"] = true;
}
	
	
	$tdataevents[".NCSearch"] = true;



$tdataevents[".shortTableName"] = "events";
$tdataevents[".nSecOptions"] = 0;
$tdataevents[".recsPerRowList"] = 1;
$tdataevents[".mainTableOwnerID"] = "";
$tdataevents[".moveNext"] = 1;
$tdataevents[".nType"] = 0;

$tdataevents[".strOriginalTableName"] = "events";




$tdataevents[".showAddInPopup"] = false;

$tdataevents[".showEditInPopup"] = false;

$tdataevents[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataevents[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataevents[".fieldsForRegister"] = array();

$tdataevents[".listAjax"] = false;

	$tdataevents[".audit"] = true;

	$tdataevents[".locking"] = true;

$tdataevents[".edit"] = true;

$tdataevents[".list"] = true;

$tdataevents[".view"] = true;


$tdataevents[".exportTo"] = true;

$tdataevents[".printFriendly"] = true;

$tdataevents[".delete"] = true;

$tdataevents[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdataevents[".searchSaving"] = false;
//

$tdataevents[".showSearchPanel"] = true;
		$tdataevents[".flexibleSearch"] = true;		

if (isMobile())
	$tdataevents[".isUseAjaxSuggest"] = false;
else 
	$tdataevents[".isUseAjaxSuggest"] = true;

$tdataevents[".rowHighlite"] = true;



$tdataevents[".addPageEvents"] = false;

// use timepicker for search panel
$tdataevents[".isUseTimeForSearch"] = false;





$tdataevents[".allSearchFields"] = array();
$tdataevents[".filterFields"] = array();
$tdataevents[".requiredSearchFields"] = array();

$tdataevents[".allSearchFields"][] = "id";
	$tdataevents[".allSearchFields"][] = "lang_id";
	$tdataevents[".allSearchFields"][] = "url";
	$tdataevents[".allSearchFields"][] = "view";
	$tdataevents[".allSearchFields"][] = "hot_event";
	$tdataevents[".allSearchFields"][] = "date";
	$tdataevents[".allSearchFields"][] = "title";
	$tdataevents[".allSearchFields"][] = "data";
	$tdataevents[".allSearchFields"][] = "photo";
	$tdataevents[".allSearchFields"][] = "meta_title";
	$tdataevents[".allSearchFields"][] = "meta_desc";
	$tdataevents[".allSearchFields"][] = "meta_keywords";
	

$tdataevents[".googleLikeFields"] = array();
$tdataevents[".googleLikeFields"][] = "id";
$tdataevents[".googleLikeFields"][] = "title";
$tdataevents[".googleLikeFields"][] = "data";
$tdataevents[".googleLikeFields"][] = "photo";
$tdataevents[".googleLikeFields"][] = "meta_title";
$tdataevents[".googleLikeFields"][] = "meta_keywords";
$tdataevents[".googleLikeFields"][] = "meta_desc";
$tdataevents[".googleLikeFields"][] = "date";
$tdataevents[".googleLikeFields"][] = "view";
$tdataevents[".googleLikeFields"][] = "lang_id";
$tdataevents[".googleLikeFields"][] = "hot_event";
$tdataevents[".googleLikeFields"][] = "url";


$tdataevents[".advSearchFields"] = array();
$tdataevents[".advSearchFields"][] = "id";
$tdataevents[".advSearchFields"][] = "lang_id";
$tdataevents[".advSearchFields"][] = "url";
$tdataevents[".advSearchFields"][] = "view";
$tdataevents[".advSearchFields"][] = "hot_event";
$tdataevents[".advSearchFields"][] = "date";
$tdataevents[".advSearchFields"][] = "title";
$tdataevents[".advSearchFields"][] = "data";
$tdataevents[".advSearchFields"][] = "photo";
$tdataevents[".advSearchFields"][] = "meta_title";
$tdataevents[".advSearchFields"][] = "meta_desc";
$tdataevents[".advSearchFields"][] = "meta_keywords";

$tdataevents[".tableType"] = "list";

$tdataevents[".printerPageOrientation"] = 0;
$tdataevents[".nPrinterPageScale"] = 100;

$tdataevents[".nPrinterSplitRecords"] = 40;

$tdataevents[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdataevents[".pageSize"] = 20;

$tdataevents[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataevents[".strOrderBy"] = $tstrOrderBy;

$tdataevents[".orderindexes"] = array();

$tdataevents[".sqlHead"] = "SELECT id,  	title,  	`data`,  	photo,  	meta_title,  	meta_keywords,  	meta_desc,  	`date`,  	`view`,  	lang_id,  	hot_event,  	url";
$tdataevents[".sqlFrom"] = "FROM events";
$tdataevents[".sqlWhereExpr"] = "";
$tdataevents[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataevents[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataevents[".arrGroupsPerPage"] = $arrGPP;

$tdataevents[".highlightSearchResults"] = true;

$tableKeysevents = array();
$tableKeysevents[] = "id";
$tdataevents[".Keys"] = $tableKeysevents;

$tdataevents[".listFields"] = array();
$tdataevents[".listFields"][] = "id";
$tdataevents[".listFields"][] = "lang_id";
$tdataevents[".listFields"][] = "url";
$tdataevents[".listFields"][] = "view";
$tdataevents[".listFields"][] = "hot_event";
$tdataevents[".listFields"][] = "title";
$tdataevents[".listFields"][] = "photo";

$tdataevents[".hideMobileList"] = array();


$tdataevents[".viewFields"] = array();
$tdataevents[".viewFields"][] = "id";
$tdataevents[".viewFields"][] = "lang_id";
$tdataevents[".viewFields"][] = "url";
$tdataevents[".viewFields"][] = "view";
$tdataevents[".viewFields"][] = "hot_event";
$tdataevents[".viewFields"][] = "date";
$tdataevents[".viewFields"][] = "title";
$tdataevents[".viewFields"][] = "data";
$tdataevents[".viewFields"][] = "photo";
$tdataevents[".viewFields"][] = "meta_title";
$tdataevents[".viewFields"][] = "meta_desc";
$tdataevents[".viewFields"][] = "meta_keywords";

$tdataevents[".addFields"] = array();
$tdataevents[".addFields"][] = "lang_id";
$tdataevents[".addFields"][] = "url";
$tdataevents[".addFields"][] = "view";
$tdataevents[".addFields"][] = "hot_event";
$tdataevents[".addFields"][] = "date";
$tdataevents[".addFields"][] = "title";
$tdataevents[".addFields"][] = "data";
$tdataevents[".addFields"][] = "photo";
$tdataevents[".addFields"][] = "meta_title";
$tdataevents[".addFields"][] = "meta_desc";
$tdataevents[".addFields"][] = "meta_keywords";

$tdataevents[".inlineAddFields"] = array();

$tdataevents[".editFields"] = array();
$tdataevents[".editFields"][] = "lang_id";
$tdataevents[".editFields"][] = "url";
$tdataevents[".editFields"][] = "view";
$tdataevents[".editFields"][] = "hot_event";
$tdataevents[".editFields"][] = "date";
$tdataevents[".editFields"][] = "title";
$tdataevents[".editFields"][] = "data";
$tdataevents[".editFields"][] = "photo";
$tdataevents[".editFields"][] = "meta_title";
$tdataevents[".editFields"][] = "meta_desc";
$tdataevents[".editFields"][] = "meta_keywords";

$tdataevents[".inlineEditFields"] = array();

$tdataevents[".exportFields"] = array();
$tdataevents[".exportFields"][] = "id";
$tdataevents[".exportFields"][] = "lang_id";
$tdataevents[".exportFields"][] = "url";
$tdataevents[".exportFields"][] = "view";
$tdataevents[".exportFields"][] = "hot_event";
$tdataevents[".exportFields"][] = "date";
$tdataevents[".exportFields"][] = "title";
$tdataevents[".exportFields"][] = "data";
$tdataevents[".exportFields"][] = "photo";
$tdataevents[".exportFields"][] = "meta_title";
$tdataevents[".exportFields"][] = "meta_desc";
$tdataevents[".exportFields"][] = "meta_keywords";

$tdataevents[".importFields"] = array();

$tdataevents[".printFields"] = array();
$tdataevents[".printFields"][] = "id";
$tdataevents[".printFields"][] = "lang_id";
$tdataevents[".printFields"][] = "url";
$tdataevents[".printFields"][] = "view";
$tdataevents[".printFields"][] = "hot_event";
$tdataevents[".printFields"][] = "date";
$tdataevents[".printFields"][] = "title";
$tdataevents[".printFields"][] = "data";
$tdataevents[".printFields"][] = "photo";
$tdataevents[".printFields"][] = "meta_title";
$tdataevents[".printFields"][] = "meta_desc";
$tdataevents[".printFields"][] = "meta_keywords";

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataevents["id"] = $fdata;
//	title
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "title";
	$fdata["GoodName"] = "title";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","title"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "title"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "title";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataevents["title"] = $fdata;
//	data
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "data";
	$fdata["GoodName"] = "data";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","data"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "data"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`data`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "HTML");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		$edata["UseRTE"] = true; 
	
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 400;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataevents["data"] = $fdata;
//	photo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "photo";
	$fdata["GoodName"] = "photo";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","photo"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "photo"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "photo";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataevents["photo"] = $fdata;
//	meta_title
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "meta_title";
	$fdata["GoodName"] = "meta_title";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","meta_title"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_title"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_title";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataevents["meta_title"] = $fdata;
//	meta_keywords
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "meta_keywords";
	$fdata["GoodName"] = "meta_keywords";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","meta_keywords"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_keywords"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_keywords";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataevents["meta_keywords"] = $fdata;
//	meta_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "meta_desc";
	$fdata["GoodName"] = "meta_desc";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","meta_desc"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_desc"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_desc";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataevents["meta_desc"] = $fdata;
//	date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "date";
	$fdata["GoodName"] = "date";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","date"); 
	$fdata["FieldType"] = 135;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "date"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`date`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Short Date");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Date");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		$edata["DateEditType"] = 13; 
	$edata["InitialYearFactor"] = 100; 
	$edata["LastYearFactor"] = 10; 
	
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataevents["date"] = $fdata;
//	view
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "view";
	$fdata["GoodName"] = "view";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","view"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "view"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`view`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Checkbox");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Checkbox");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataevents["view"] = $fdata;
//	lang_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "lang_id";
	$fdata["GoodName"] = "lang_id";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","lang_id"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "lang_id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lang_id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Lookup wizard");
	
			
	
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "langyages";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
		
		
			
	$edata["LinkField"] = "id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "name";
	
		
	$edata["LookupOrderBy"] = "";
	
		
			
		
				
	
	
		
		$edata["SelectSize"] = 1;
		
// End Lookup Settings


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataevents["lang_id"] = $fdata;
//	hot_event
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "hot_event";
	$fdata["GoodName"] = "hot_event";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","hot_event"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "hot_event"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "hot_event";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Checkbox");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Checkbox");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataevents["hot_event"] = $fdata;
//	url
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "url";
	$fdata["GoodName"] = "url";
	$fdata["ownerTable"] = "events";
	$fdata["Label"] = GetFieldLabel("events","url"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "url"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "url";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataevents["url"] = $fdata;

	
$tables_data["events"]=&$tdataevents;
$field_labels["events"] = &$fieldLabelsevents;
$fieldToolTips["events"] = &$fieldToolTipsevents;
$page_titles["events"] = &$pageTitlesevents;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["events"] = array();
	
// tables which are master tables for current table (detail)
$masterTablesData["events"] = array();


	
				$strOriginalDetailsTable="langyages";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="langyages";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "langyages";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispInfo"]= "1";
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["events"][0] = $masterParams;	
				$masterTablesData["events"][0]["masterKeys"] = array();
	$masterTablesData["events"][0]["masterKeys"][]="id";
				$masterTablesData["events"][0]["detailKeys"] = array();
	$masterTablesData["events"][0]["detailKeys"][]="lang_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_events()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	title,  	`data`,  	photo,  	meta_title,  	meta_keywords,  	meta_desc,  	`date`,  	`view`,  	lang_id,  	hot_event,  	url";
$proto0["m_strFrom"] = "FROM events";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "events";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "title",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto7["m_sql"] = "title";
$proto7["m_srcTableName"] = "events";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto9["m_sql"] = "`data`";
$proto9["m_srcTableName"] = "events";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "photo",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto11["m_sql"] = "photo";
$proto11["m_srcTableName"] = "events";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_title",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto13["m_sql"] = "meta_title";
$proto13["m_srcTableName"] = "events";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_keywords",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto15["m_sql"] = "meta_keywords";
$proto15["m_srcTableName"] = "events";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_desc",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto17["m_sql"] = "meta_desc";
$proto17["m_srcTableName"] = "events";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "date",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto19["m_sql"] = "`date`";
$proto19["m_srcTableName"] = "events";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "view",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto21["m_sql"] = "`view`";
$proto21["m_srcTableName"] = "events";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto0["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "lang_id",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto23["m_sql"] = "lang_id";
$proto23["m_srcTableName"] = "events";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto0["m_fieldlist"][]=$obj;
						$proto25=array();
			$obj = new SQLField(array(
	"m_strName" => "hot_event",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto25["m_sql"] = "hot_event";
$proto25["m_srcTableName"] = "events";
$proto25["m_expr"]=$obj;
$proto25["m_alias"] = "";
$obj = new SQLFieldListItem($proto25);

$proto0["m_fieldlist"][]=$obj;
						$proto27=array();
			$obj = new SQLField(array(
	"m_strName" => "url",
	"m_strTable" => "events",
	"m_srcTableName" => "events"
));

$proto27["m_sql"] = "url";
$proto27["m_srcTableName"] = "events";
$proto27["m_expr"]=$obj;
$proto27["m_alias"] = "";
$obj = new SQLFieldListItem($proto27);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto29=array();
$proto29["m_link"] = "SQLL_MAIN";
			$proto30=array();
$proto30["m_strName"] = "events";
$proto30["m_srcTableName"] = "events";
$proto30["m_columns"] = array();
$proto30["m_columns"][] = "id";
$proto30["m_columns"][] = "title";
$proto30["m_columns"][] = "data";
$proto30["m_columns"][] = "photo";
$proto30["m_columns"][] = "meta_title";
$proto30["m_columns"][] = "meta_keywords";
$proto30["m_columns"][] = "meta_desc";
$proto30["m_columns"][] = "date";
$proto30["m_columns"][] = "view";
$proto30["m_columns"][] = "lang_id";
$proto30["m_columns"][] = "hot_event";
$proto30["m_columns"][] = "url";
$obj = new SQLTable($proto30);

$proto29["m_table"] = $obj;
$proto29["m_sql"] = "events";
$proto29["m_alias"] = "";
$proto29["m_srcTableName"] = "events";
$proto31=array();
$proto31["m_sql"] = "";
$proto31["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto31["m_column"]=$obj;
$proto31["m_contained"] = array();
$proto31["m_strCase"] = "";
$proto31["m_havingmode"] = false;
$proto31["m_inBrackets"] = false;
$proto31["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto31);

$proto29["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto29);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="events";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_events = createSqlQuery_events();


	
												
	
$tdataevents[".sqlquery"] = $queryData_events;

$tableEvents["events"] = new eventsBase;
$tdataevents[".hasEvents"] = false;

?>