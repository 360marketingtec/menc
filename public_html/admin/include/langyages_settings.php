<?php
require_once(getabspath("classes/cipherer.php"));




$tdatalangyages = array();	
	$tdatalangyages[".truncateText"] = true;
	$tdatalangyages[".NumberOfChars"] = 80; 
	$tdatalangyages[".ShortName"] = "langyages";
	$tdatalangyages[".OwnerID"] = "";
	$tdatalangyages[".OriginalTable"] = "langyages";

//	field labels
$fieldLabelslangyages = array();
$fieldToolTipslangyages = array();
$pageTitleslangyages = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelslangyages["English"] = array();
	$fieldToolTipslangyages["English"] = array();
	$pageTitleslangyages["English"] = array();
	$fieldLabelslangyages["English"]["id"] = "Id";
	$fieldToolTipslangyages["English"]["id"] = "";
	$fieldLabelslangyages["English"]["name"] = "Name";
	$fieldToolTipslangyages["English"]["name"] = "";
	if (count($fieldToolTipslangyages["English"]))
		$tdatalangyages[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelslangyages["Arabic"] = array();
	$fieldToolTipslangyages["Arabic"] = array();
	$pageTitleslangyages["Arabic"] = array();
	$fieldLabelslangyages["Arabic"]["id"] = "Id";
	$fieldToolTipslangyages["Arabic"]["id"] = "";
	$fieldLabelslangyages["Arabic"]["name"] = "Name";
	$fieldToolTipslangyages["Arabic"]["name"] = "";
	if (count($fieldToolTipslangyages["Arabic"]))
		$tdatalangyages[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelslangyages[""] = array();
	$fieldToolTipslangyages[""] = array();
	$pageTitleslangyages[""] = array();
	if (count($fieldToolTipslangyages[""]))
		$tdatalangyages[".isUseToolTips"] = true;
}
	
	
	$tdatalangyages[".NCSearch"] = true;



$tdatalangyages[".shortTableName"] = "langyages";
$tdatalangyages[".nSecOptions"] = 0;
$tdatalangyages[".recsPerRowList"] = 1;
$tdatalangyages[".mainTableOwnerID"] = "";
$tdatalangyages[".moveNext"] = 1;
$tdatalangyages[".nType"] = 0;

$tdatalangyages[".strOriginalTableName"] = "langyages";




$tdatalangyages[".showAddInPopup"] = false;

$tdatalangyages[".showEditInPopup"] = false;

$tdatalangyages[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatalangyages[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatalangyages[".fieldsForRegister"] = array();

$tdatalangyages[".listAjax"] = false;

	$tdatalangyages[".audit"] = true;

	$tdatalangyages[".locking"] = true;

$tdatalangyages[".edit"] = true;

$tdatalangyages[".list"] = true;

$tdatalangyages[".view"] = true;




$tdatalangyages[".delete"] = true;

$tdatalangyages[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatalangyages[".searchSaving"] = false;
//

$tdatalangyages[".showSearchPanel"] = true;
		$tdatalangyages[".flexibleSearch"] = true;		

if (isMobile())
	$tdatalangyages[".isUseAjaxSuggest"] = false;
else 
	$tdatalangyages[".isUseAjaxSuggest"] = true;

$tdatalangyages[".rowHighlite"] = true;



$tdatalangyages[".addPageEvents"] = false;

// use timepicker for search panel
$tdatalangyages[".isUseTimeForSearch"] = false;



$tdatalangyages[".useDetailsPreview"] = true;


$tdatalangyages[".allSearchFields"] = array();
$tdatalangyages[".filterFields"] = array();
$tdatalangyages[".requiredSearchFields"] = array();

$tdatalangyages[".allSearchFields"][] = "id";
	$tdatalangyages[".allSearchFields"][] = "name";
	

$tdatalangyages[".googleLikeFields"] = array();
$tdatalangyages[".googleLikeFields"][] = "id";
$tdatalangyages[".googleLikeFields"][] = "name";


$tdatalangyages[".advSearchFields"] = array();
$tdatalangyages[".advSearchFields"][] = "id";
$tdatalangyages[".advSearchFields"][] = "name";

$tdatalangyages[".tableType"] = "list";

$tdatalangyages[".printerPageOrientation"] = 0;
$tdatalangyages[".nPrinterPageScale"] = 100;

$tdatalangyages[".nPrinterSplitRecords"] = 40;

$tdatalangyages[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdatalangyages[".pageSize"] = 20;

$tdatalangyages[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatalangyages[".strOrderBy"] = $tstrOrderBy;

$tdatalangyages[".orderindexes"] = array();

$tdatalangyages[".sqlHead"] = "SELECT id,  	name";
$tdatalangyages[".sqlFrom"] = "FROM langyages";
$tdatalangyages[".sqlWhereExpr"] = "";
$tdatalangyages[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatalangyages[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatalangyages[".arrGroupsPerPage"] = $arrGPP;

$tdatalangyages[".highlightSearchResults"] = true;

$tableKeyslangyages = array();
$tableKeyslangyages[] = "id";
$tdatalangyages[".Keys"] = $tableKeyslangyages;

$tdatalangyages[".listFields"] = array();
$tdatalangyages[".listFields"][] = "id";
$tdatalangyages[".listFields"][] = "name";

$tdatalangyages[".hideMobileList"] = array();


$tdatalangyages[".viewFields"] = array();
$tdatalangyages[".viewFields"][] = "id";
$tdatalangyages[".viewFields"][] = "name";

$tdatalangyages[".addFields"] = array();
$tdatalangyages[".addFields"][] = "name";

$tdatalangyages[".inlineAddFields"] = array();

$tdatalangyages[".editFields"] = array();
$tdatalangyages[".editFields"][] = "name";

$tdatalangyages[".inlineEditFields"] = array();

$tdatalangyages[".exportFields"] = array();

$tdatalangyages[".importFields"] = array();

$tdatalangyages[".printFields"] = array();

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "langyages";
	$fdata["Label"] = GetFieldLabel("langyages","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatalangyages["id"] = $fdata;
//	name
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "name";
	$fdata["GoodName"] = "name";
	$fdata["ownerTable"] = "langyages";
	$fdata["Label"] = GetFieldLabel("langyages","name"); 
	$fdata["FieldType"] = 200;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "name"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "name";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";
	
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatalangyages["name"] = $fdata;

	
$tables_data["langyages"]=&$tdatalangyages;
$field_labels["langyages"] = &$fieldLabelslangyages;
$fieldToolTips["langyages"] = &$fieldToolTipslangyages;
$page_titles["langyages"] = &$pageTitleslangyages;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["langyages"] = array();
//	statec_words_translation
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="statec_words_translation";
		$detailsParam["dOriginalTable"] = "statec_words_translation";
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "statec_words_translation";
	$detailsParam["dCaptionTable"] = GetTableCaption("statec_words_translation");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();
	$detailsParam["dispChildCount"] = "1";
		$detailsParam["hideChild"] = false;
	$detailsParam["previewOnList"] = 1;
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
			
	$detailsTablesData["langyages"][$dIndex] = $detailsParam;
	
		
		$detailsTablesData["langyages"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["langyages"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["detailKeys"][]="lang_id";
//	events
	
	

		$dIndex = 1;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="events";
		$detailsParam["dOriginalTable"] = "events";
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "events";
	$detailsParam["dCaptionTable"] = GetTableCaption("events");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();
	$detailsParam["dispChildCount"] = "1";
		$detailsParam["hideChild"] = false;
	$detailsParam["previewOnList"] = 1;
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
			
	$detailsTablesData["langyages"][$dIndex] = $detailsParam;
	
		
		$detailsTablesData["langyages"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["langyages"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["detailKeys"][]="lang_id";
//	collegs
	
	

		$dIndex = 2;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="collegs";
		$detailsParam["dOriginalTable"] = "collegs";
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "collegs";
	$detailsParam["dCaptionTable"] = GetTableCaption("collegs");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();
	$detailsParam["dispChildCount"] = "1";
		$detailsParam["hideChild"] = false;
	$detailsParam["previewOnList"] = 1;
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
			
	$detailsTablesData["langyages"][$dIndex] = $detailsParam;
	
		
		$detailsTablesData["langyages"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["langyages"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["detailKeys"][]="lang_id";
//	articles
	
	

		$dIndex = 3;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="articles";
		$detailsParam["dOriginalTable"] = "articles";
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "articles";
	$detailsParam["dCaptionTable"] = GetTableCaption("articles");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();
	$detailsParam["dispChildCount"] = "1";
		$detailsParam["hideChild"] = false;
	$detailsParam["previewOnList"] = 1;
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
			
	$detailsTablesData["langyages"][$dIndex] = $detailsParam;
	
		
		$detailsTablesData["langyages"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["langyages"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["detailKeys"][]="lang_id";
//	home_slider
	
	

		$dIndex = 4;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="home_slider";
		$detailsParam["dOriginalTable"] = "home_slider";
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "home_slider";
	$detailsParam["dCaptionTable"] = GetTableCaption("home_slider");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();
	$detailsParam["dispChildCount"] = "1";
		$detailsParam["hideChild"] = false;
	$detailsParam["previewOnList"] = 1;
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
			
	$detailsTablesData["langyages"][$dIndex] = $detailsParam;
	
		
		$detailsTablesData["langyages"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["langyages"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["detailKeys"][]="lang_id";
//	site_statics
	
	

		$dIndex = 5;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="site_statics";
		$detailsParam["dOriginalTable"] = "site_statics";
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "site_statics";
	$detailsParam["dCaptionTable"] = GetTableCaption("site_statics");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();
	$detailsParam["dispChildCount"] = "1";
		$detailsParam["hideChild"] = false;
	$detailsParam["previewOnList"] = 1;
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
			
	$detailsTablesData["langyages"][$dIndex] = $detailsParam;
	
		
		$detailsTablesData["langyages"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["langyages"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["detailKeys"][]="lang_id";
//	news
	
	

		$dIndex = 6;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="news";
		$detailsParam["dOriginalTable"] = "news";
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "news";
	$detailsParam["dCaptionTable"] = GetTableCaption("news");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();
	$detailsParam["dispChildCount"] = "1";
		$detailsParam["hideChild"] = false;
	$detailsParam["previewOnList"] = 1;
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
			
	$detailsTablesData["langyages"][$dIndex] = $detailsParam;
	
		
		$detailsTablesData["langyages"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["langyages"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["detailKeys"][]="lang_id";
//	home_main_dialogs
	
	

		$dIndex = 7;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="home_main_dialogs";
		$detailsParam["dOriginalTable"] = "home_main_dialogs";
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "home_main_dialogs";
	$detailsParam["dCaptionTable"] = GetTableCaption("home_main_dialogs");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();
	$detailsParam["dispChildCount"] = "1";
		$detailsParam["hideChild"] = false;
	$detailsParam["previewOnList"] = 1;
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
			
	$detailsTablesData["langyages"][$dIndex] = $detailsParam;
	
		
		$detailsTablesData["langyages"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["langyages"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["detailKeys"][]="lang_id";
//	home_bottom_dialogs
	
	

		$dIndex = 8;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="home_bottom_dialogs";
		$detailsParam["dOriginalTable"] = "home_bottom_dialogs";
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "home_bottom_dialogs";
	$detailsParam["dCaptionTable"] = GetTableCaption("home_bottom_dialogs");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();
	$detailsParam["dispChildCount"] = "1";
		$detailsParam["hideChild"] = false;
	$detailsParam["previewOnList"] = 1;
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
			
	$detailsTablesData["langyages"][$dIndex] = $detailsParam;
	
		
		$detailsTablesData["langyages"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["langyages"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["langyages"][$dIndex]["detailKeys"][]="lang_id";
	
// tables which are master tables for current table (detail)
$masterTablesData["langyages"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_langyages()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	name";
$proto0["m_strFrom"] = "FROM langyages";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "langyages",
	"m_srcTableName" => "langyages"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "langyages";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "name",
	"m_strTable" => "langyages",
	"m_srcTableName" => "langyages"
));

$proto7["m_sql"] = "name";
$proto7["m_srcTableName"] = "langyages";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto9=array();
$proto9["m_link"] = "SQLL_MAIN";
			$proto10=array();
$proto10["m_strName"] = "langyages";
$proto10["m_srcTableName"] = "langyages";
$proto10["m_columns"] = array();
$proto10["m_columns"][] = "id";
$proto10["m_columns"][] = "name";
$obj = new SQLTable($proto10);

$proto9["m_table"] = $obj;
$proto9["m_sql"] = "langyages";
$proto9["m_alias"] = "";
$proto9["m_srcTableName"] = "langyages";
$proto11=array();
$proto11["m_sql"] = "";
$proto11["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto11["m_column"]=$obj;
$proto11["m_contained"] = array();
$proto11["m_strCase"] = "";
$proto11["m_havingmode"] = false;
$proto11["m_inBrackets"] = false;
$proto11["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto11);

$proto9["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto9);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="langyages";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_langyages = createSqlQuery_langyages();


	
		
	
$tdatalangyages[".sqlquery"] = $queryData_langyages;

$tableEvents["langyages"] = new eventsBase;
$tdatalangyages[".hasEvents"] = false;

?>