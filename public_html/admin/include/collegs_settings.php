<?php
require_once(getabspath("classes/cipherer.php"));




$tdatacollegs = array();	
	$tdatacollegs[".truncateText"] = true;
	$tdatacollegs[".NumberOfChars"] = 80; 
	$tdatacollegs[".ShortName"] = "collegs";
	$tdatacollegs[".OwnerID"] = "";
	$tdatacollegs[".OriginalTable"] = "collegs";

//	field labels
$fieldLabelscollegs = array();
$fieldToolTipscollegs = array();
$pageTitlescollegs = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelscollegs["English"] = array();
	$fieldToolTipscollegs["English"] = array();
	$pageTitlescollegs["English"] = array();
	$fieldLabelscollegs["English"]["id"] = "Id";
	$fieldToolTipscollegs["English"]["id"] = "";
	$fieldLabelscollegs["English"]["lang_id"] = "Lang Id";
	$fieldToolTipscollegs["English"]["lang_id"] = "";
	$fieldLabelscollegs["English"]["view"] = "View";
	$fieldToolTipscollegs["English"]["view"] = "";
	$fieldLabelscollegs["English"]["order"] = "Order";
	$fieldToolTipscollegs["English"]["order"] = "";
	$fieldLabelscollegs["English"]["parent"] = "Parent";
	$fieldToolTipscollegs["English"]["parent"] = "";
	$fieldLabelscollegs["English"]["title"] = "Title";
	$fieldToolTipscollegs["English"]["title"] = "";
	$fieldLabelscollegs["English"]["photo"] = "Photo";
	$fieldToolTipscollegs["English"]["photo"] = "";
	$fieldLabelscollegs["English"]["slider"] = "Slider";
	$fieldToolTipscollegs["English"]["slider"] = "";
	$fieldLabelscollegs["English"]["data"] = "Data";
	$fieldToolTipscollegs["English"]["data"] = "";
	$fieldLabelscollegs["English"]["meta_title"] = "Meta Title";
	$fieldToolTipscollegs["English"]["meta_title"] = "";
	$fieldLabelscollegs["English"]["meta_description"] = "Meta Description";
	$fieldToolTipscollegs["English"]["meta_description"] = "";
	$fieldLabelscollegs["English"]["meta_keywords"] = "Meta Keywords";
	$fieldToolTipscollegs["English"]["meta_keywords"] = "";
	if (count($fieldToolTipscollegs["English"]))
		$tdatacollegs[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelscollegs["Arabic"] = array();
	$fieldToolTipscollegs["Arabic"] = array();
	$pageTitlescollegs["Arabic"] = array();
	$fieldLabelscollegs["Arabic"]["id"] = "Id";
	$fieldToolTipscollegs["Arabic"]["id"] = "";
	$fieldLabelscollegs["Arabic"]["lang_id"] = "Lang Id";
	$fieldToolTipscollegs["Arabic"]["lang_id"] = "";
	$fieldLabelscollegs["Arabic"]["view"] = "View";
	$fieldToolTipscollegs["Arabic"]["view"] = "";
	$fieldLabelscollegs["Arabic"]["order"] = "Order";
	$fieldToolTipscollegs["Arabic"]["order"] = "";
	$fieldLabelscollegs["Arabic"]["parent"] = "Parent";
	$fieldToolTipscollegs["Arabic"]["parent"] = "";
	$fieldLabelscollegs["Arabic"]["title"] = "Title";
	$fieldToolTipscollegs["Arabic"]["title"] = "";
	$fieldLabelscollegs["Arabic"]["photo"] = "Photo";
	$fieldToolTipscollegs["Arabic"]["photo"] = "";
	$fieldLabelscollegs["Arabic"]["slider"] = "Slider";
	$fieldToolTipscollegs["Arabic"]["slider"] = "";
	$fieldLabelscollegs["Arabic"]["data"] = "Data";
	$fieldToolTipscollegs["Arabic"]["data"] = "";
	$fieldLabelscollegs["Arabic"]["meta_title"] = "Meta Title";
	$fieldToolTipscollegs["Arabic"]["meta_title"] = "";
	$fieldLabelscollegs["Arabic"]["meta_description"] = "Meta Description";
	$fieldToolTipscollegs["Arabic"]["meta_description"] = "";
	$fieldLabelscollegs["Arabic"]["meta_keywords"] = "Meta Keywords";
	$fieldToolTipscollegs["Arabic"]["meta_keywords"] = "";
	if (count($fieldToolTipscollegs["Arabic"]))
		$tdatacollegs[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelscollegs[""] = array();
	$fieldToolTipscollegs[""] = array();
	$pageTitlescollegs[""] = array();
	if (count($fieldToolTipscollegs[""]))
		$tdatacollegs[".isUseToolTips"] = true;
}
	
	
	$tdatacollegs[".NCSearch"] = true;



$tdatacollegs[".shortTableName"] = "collegs";
$tdatacollegs[".nSecOptions"] = 0;
$tdatacollegs[".recsPerRowList"] = 1;
$tdatacollegs[".mainTableOwnerID"] = "";
$tdatacollegs[".moveNext"] = 1;
$tdatacollegs[".nType"] = 0;

$tdatacollegs[".strOriginalTableName"] = "collegs";




$tdatacollegs[".showAddInPopup"] = false;

$tdatacollegs[".showEditInPopup"] = false;

$tdatacollegs[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatacollegs[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatacollegs[".fieldsForRegister"] = array();

$tdatacollegs[".listAjax"] = false;

	$tdatacollegs[".audit"] = true;

	$tdatacollegs[".locking"] = true;

$tdatacollegs[".edit"] = true;

$tdatacollegs[".list"] = true;

$tdatacollegs[".view"] = true;


$tdatacollegs[".exportTo"] = true;

$tdatacollegs[".printFriendly"] = true;

$tdatacollegs[".delete"] = true;

$tdatacollegs[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatacollegs[".searchSaving"] = false;
//

$tdatacollegs[".showSearchPanel"] = true;
		$tdatacollegs[".flexibleSearch"] = true;		

if (isMobile())
	$tdatacollegs[".isUseAjaxSuggest"] = false;
else 
	$tdatacollegs[".isUseAjaxSuggest"] = true;

$tdatacollegs[".rowHighlite"] = true;



$tdatacollegs[".addPageEvents"] = false;

// use timepicker for search panel
$tdatacollegs[".isUseTimeForSearch"] = false;





$tdatacollegs[".allSearchFields"] = array();
$tdatacollegs[".filterFields"] = array();
$tdatacollegs[".requiredSearchFields"] = array();

$tdatacollegs[".allSearchFields"][] = "id";
	$tdatacollegs[".allSearchFields"][] = "lang_id";
	$tdatacollegs[".allSearchFields"][] = "view";
	$tdatacollegs[".allSearchFields"][] = "order";
	$tdatacollegs[".allSearchFields"][] = "parent";
	$tdatacollegs[".allSearchFields"][] = "title";
	$tdatacollegs[".allSearchFields"][] = "photo";
	$tdatacollegs[".allSearchFields"][] = "slider";
	$tdatacollegs[".allSearchFields"][] = "data";
	$tdatacollegs[".allSearchFields"][] = "meta_title";
	$tdatacollegs[".allSearchFields"][] = "meta_description";
	$tdatacollegs[".allSearchFields"][] = "meta_keywords";
	

$tdatacollegs[".googleLikeFields"] = array();
$tdatacollegs[".googleLikeFields"][] = "id";
$tdatacollegs[".googleLikeFields"][] = "lang_id";
$tdatacollegs[".googleLikeFields"][] = "view";
$tdatacollegs[".googleLikeFields"][] = "order";
$tdatacollegs[".googleLikeFields"][] = "parent";
$tdatacollegs[".googleLikeFields"][] = "title";
$tdatacollegs[".googleLikeFields"][] = "photo";
$tdatacollegs[".googleLikeFields"][] = "slider";
$tdatacollegs[".googleLikeFields"][] = "data";
$tdatacollegs[".googleLikeFields"][] = "meta_title";
$tdatacollegs[".googleLikeFields"][] = "meta_description";
$tdatacollegs[".googleLikeFields"][] = "meta_keywords";


$tdatacollegs[".advSearchFields"] = array();
$tdatacollegs[".advSearchFields"][] = "id";
$tdatacollegs[".advSearchFields"][] = "lang_id";
$tdatacollegs[".advSearchFields"][] = "view";
$tdatacollegs[".advSearchFields"][] = "order";
$tdatacollegs[".advSearchFields"][] = "parent";
$tdatacollegs[".advSearchFields"][] = "title";
$tdatacollegs[".advSearchFields"][] = "photo";
$tdatacollegs[".advSearchFields"][] = "slider";
$tdatacollegs[".advSearchFields"][] = "data";
$tdatacollegs[".advSearchFields"][] = "meta_title";
$tdatacollegs[".advSearchFields"][] = "meta_description";
$tdatacollegs[".advSearchFields"][] = "meta_keywords";

$tdatacollegs[".tableType"] = "list";

$tdatacollegs[".printerPageOrientation"] = 0;
$tdatacollegs[".nPrinterPageScale"] = 100;

$tdatacollegs[".nPrinterSplitRecords"] = 40;

$tdatacollegs[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdatacollegs[".pageSize"] = 20;

$tdatacollegs[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatacollegs[".strOrderBy"] = $tstrOrderBy;

$tdatacollegs[".orderindexes"] = array();

$tdatacollegs[".sqlHead"] = "SELECT id,  	lang_id,  	`view`,  	`order`,  	parent,  	title,  	photo,  	slider,  	`data`,  	meta_title,  	meta_description,  	meta_keywords";
$tdatacollegs[".sqlFrom"] = "FROM collegs";
$tdatacollegs[".sqlWhereExpr"] = "";
$tdatacollegs[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatacollegs[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatacollegs[".arrGroupsPerPage"] = $arrGPP;

$tdatacollegs[".highlightSearchResults"] = true;

$tableKeyscollegs = array();
$tableKeyscollegs[] = "id";
$tdatacollegs[".Keys"] = $tableKeyscollegs;

$tdatacollegs[".listFields"] = array();
$tdatacollegs[".listFields"][] = "id";
$tdatacollegs[".listFields"][] = "lang_id";
$tdatacollegs[".listFields"][] = "view";
$tdatacollegs[".listFields"][] = "order";
$tdatacollegs[".listFields"][] = "parent";
$tdatacollegs[".listFields"][] = "title";

$tdatacollegs[".hideMobileList"] = array();


$tdatacollegs[".viewFields"] = array();
$tdatacollegs[".viewFields"][] = "id";
$tdatacollegs[".viewFields"][] = "lang_id";
$tdatacollegs[".viewFields"][] = "view";
$tdatacollegs[".viewFields"][] = "order";
$tdatacollegs[".viewFields"][] = "parent";
$tdatacollegs[".viewFields"][] = "title";
$tdatacollegs[".viewFields"][] = "photo";
$tdatacollegs[".viewFields"][] = "slider";
$tdatacollegs[".viewFields"][] = "data";
$tdatacollegs[".viewFields"][] = "meta_title";
$tdatacollegs[".viewFields"][] = "meta_description";
$tdatacollegs[".viewFields"][] = "meta_keywords";

$tdatacollegs[".addFields"] = array();
$tdatacollegs[".addFields"][] = "lang_id";
$tdatacollegs[".addFields"][] = "view";
$tdatacollegs[".addFields"][] = "order";
$tdatacollegs[".addFields"][] = "parent";
$tdatacollegs[".addFields"][] = "title";
$tdatacollegs[".addFields"][] = "photo";
$tdatacollegs[".addFields"][] = "slider";
$tdatacollegs[".addFields"][] = "data";
$tdatacollegs[".addFields"][] = "meta_title";
$tdatacollegs[".addFields"][] = "meta_description";
$tdatacollegs[".addFields"][] = "meta_keywords";

$tdatacollegs[".inlineAddFields"] = array();

$tdatacollegs[".editFields"] = array();
$tdatacollegs[".editFields"][] = "lang_id";
$tdatacollegs[".editFields"][] = "view";
$tdatacollegs[".editFields"][] = "order";
$tdatacollegs[".editFields"][] = "parent";
$tdatacollegs[".editFields"][] = "title";
$tdatacollegs[".editFields"][] = "photo";
$tdatacollegs[".editFields"][] = "slider";
$tdatacollegs[".editFields"][] = "data";
$tdatacollegs[".editFields"][] = "meta_title";
$tdatacollegs[".editFields"][] = "meta_description";
$tdatacollegs[".editFields"][] = "meta_keywords";

$tdatacollegs[".inlineEditFields"] = array();

$tdatacollegs[".exportFields"] = array();
$tdatacollegs[".exportFields"][] = "id";
$tdatacollegs[".exportFields"][] = "lang_id";
$tdatacollegs[".exportFields"][] = "view";
$tdatacollegs[".exportFields"][] = "order";
$tdatacollegs[".exportFields"][] = "parent";
$tdatacollegs[".exportFields"][] = "title";
$tdatacollegs[".exportFields"][] = "photo";
$tdatacollegs[".exportFields"][] = "slider";
$tdatacollegs[".exportFields"][] = "data";
$tdatacollegs[".exportFields"][] = "meta_title";
$tdatacollegs[".exportFields"][] = "meta_description";
$tdatacollegs[".exportFields"][] = "meta_keywords";

$tdatacollegs[".importFields"] = array();

$tdatacollegs[".printFields"] = array();
$tdatacollegs[".printFields"][] = "id";
$tdatacollegs[".printFields"][] = "lang_id";
$tdatacollegs[".printFields"][] = "view";
$tdatacollegs[".printFields"][] = "order";
$tdatacollegs[".printFields"][] = "parent";
$tdatacollegs[".printFields"][] = "title";
$tdatacollegs[".printFields"][] = "photo";
$tdatacollegs[".printFields"][] = "slider";
$tdatacollegs[".printFields"][] = "data";
$tdatacollegs[".printFields"][] = "meta_title";
$tdatacollegs[".printFields"][] = "meta_description";
$tdatacollegs[".printFields"][] = "meta_keywords";

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatacollegs["id"] = $fdata;
//	lang_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "lang_id";
	$fdata["GoodName"] = "lang_id";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","lang_id"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "lang_id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lang_id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Lookup wizard");
	
			
	
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "langyages";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
		
		
			
	$edata["LinkField"] = "id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "name";
	
		
	$edata["LookupOrderBy"] = "";
	
		
			
		
				
	
	
		
		$edata["SelectSize"] = 1;
		
// End Lookup Settings


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatacollegs["lang_id"] = $fdata;
//	view
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "view";
	$fdata["GoodName"] = "view";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","view"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "view"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`view`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Checkbox");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Checkbox");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatacollegs["view"] = $fdata;
//	order
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "order";
	$fdata["GoodName"] = "order";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","order"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "order"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`order`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatacollegs["order"] = $fdata;
//	parent
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "parent";
	$fdata["GoodName"] = "parent";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","parent"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "parent"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "parent";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Lookup wizard");
	
			
	
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "collegs";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
		
		
			
	$edata["LinkField"] = "id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "title";
	
		
	$edata["LookupOrderBy"] = "";
	
		
			
		
				
	
	
		
		$edata["SelectSize"] = 1;
		
// End Lookup Settings


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatacollegs["parent"] = $fdata;
//	title
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "title";
	$fdata["GoodName"] = "title";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","title"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "title"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "title";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatacollegs["title"] = $fdata;
//	photo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "photo";
	$fdata["GoodName"] = "photo";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","photo"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "photo"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "photo";
	
		$fdata["DeleteAssociatedFile"] = true;
	
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "../uploads/";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Document Download");
	
		
		
		
								$vdata["ShowIcon"] = true; 
			
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Document upload");
	
			
	
	


		
		
		
		
							$edata["acceptFileTypes"] = "bmp";
						$edata["acceptFileTypes"] .= "|gif";
						$edata["acceptFileTypes"] .= "|jpg";
						$edata["acceptFileTypes"] .= "|png";
		$edata["acceptFileTypes"] = "(".$edata["acceptFileTypes"].")$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatacollegs["photo"] = $fdata;
//	slider
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "slider";
	$fdata["GoodName"] = "slider";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","slider"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "slider"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "slider";
	
		$fdata["DeleteAssociatedFile"] = true;
	
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "../uploads/";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Document Download");
	
		
		
		
								$vdata["ShowIcon"] = true; 
			
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Document upload");
	
			
	
	


		
		
		
		
							$edata["acceptFileTypes"] = "bmp";
						$edata["acceptFileTypes"] .= "|gif";
						$edata["acceptFileTypes"] .= "|jpg";
						$edata["acceptFileTypes"] .= "|png";
		$edata["acceptFileTypes"] = "(".$edata["acceptFileTypes"].")$";
	
		$edata["maxNumberOfFiles"] = 0;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatacollegs["slider"] = $fdata;
//	data
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "data";
	$fdata["GoodName"] = "data";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","data"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "data"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`data`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "HTML");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		$edata["UseRTE"] = true; 
	
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 400;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatacollegs["data"] = $fdata;
//	meta_title
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "meta_title";
	$fdata["GoodName"] = "meta_title";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","meta_title"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_title"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_title";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatacollegs["meta_title"] = $fdata;
//	meta_description
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "meta_description";
	$fdata["GoodName"] = "meta_description";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","meta_description"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_description"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_description";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatacollegs["meta_description"] = $fdata;
//	meta_keywords
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "meta_keywords";
	$fdata["GoodName"] = "meta_keywords";
	$fdata["ownerTable"] = "collegs";
	$fdata["Label"] = GetFieldLabel("collegs","meta_keywords"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_keywords"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_keywords";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatacollegs["meta_keywords"] = $fdata;

	
$tables_data["collegs"]=&$tdatacollegs;
$field_labels["collegs"] = &$fieldLabelscollegs;
$fieldToolTips["collegs"] = &$fieldToolTipscollegs;
$page_titles["collegs"] = &$pageTitlescollegs;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["collegs"] = array();
	
// tables which are master tables for current table (detail)
$masterTablesData["collegs"] = array();


	
				$strOriginalDetailsTable="langyages";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="langyages";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "langyages";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispInfo"]= "1";
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["collegs"][0] = $masterParams;	
				$masterTablesData["collegs"][0]["masterKeys"] = array();
	$masterTablesData["collegs"][0]["masterKeys"][]="id";
				$masterTablesData["collegs"][0]["detailKeys"] = array();
	$masterTablesData["collegs"][0]["detailKeys"][]="lang_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_collegs()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	lang_id,  	`view`,  	`order`,  	parent,  	title,  	photo,  	slider,  	`data`,  	meta_title,  	meta_description,  	meta_keywords";
$proto0["m_strFrom"] = "FROM collegs";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "collegs";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "lang_id",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto7["m_sql"] = "lang_id";
$proto7["m_srcTableName"] = "collegs";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "view",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto9["m_sql"] = "`view`";
$proto9["m_srcTableName"] = "collegs";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "order",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto11["m_sql"] = "`order`";
$proto11["m_srcTableName"] = "collegs";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "parent",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto13["m_sql"] = "parent";
$proto13["m_srcTableName"] = "collegs";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "title",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto15["m_sql"] = "title";
$proto15["m_srcTableName"] = "collegs";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "photo",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto17["m_sql"] = "photo";
$proto17["m_srcTableName"] = "collegs";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "slider",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto19["m_sql"] = "slider";
$proto19["m_srcTableName"] = "collegs";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto21["m_sql"] = "`data`";
$proto21["m_srcTableName"] = "collegs";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto0["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_title",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto23["m_sql"] = "meta_title";
$proto23["m_srcTableName"] = "collegs";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto0["m_fieldlist"][]=$obj;
						$proto25=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_description",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto25["m_sql"] = "meta_description";
$proto25["m_srcTableName"] = "collegs";
$proto25["m_expr"]=$obj;
$proto25["m_alias"] = "";
$obj = new SQLFieldListItem($proto25);

$proto0["m_fieldlist"][]=$obj;
						$proto27=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_keywords",
	"m_strTable" => "collegs",
	"m_srcTableName" => "collegs"
));

$proto27["m_sql"] = "meta_keywords";
$proto27["m_srcTableName"] = "collegs";
$proto27["m_expr"]=$obj;
$proto27["m_alias"] = "";
$obj = new SQLFieldListItem($proto27);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto29=array();
$proto29["m_link"] = "SQLL_MAIN";
			$proto30=array();
$proto30["m_strName"] = "collegs";
$proto30["m_srcTableName"] = "collegs";
$proto30["m_columns"] = array();
$proto30["m_columns"][] = "id";
$proto30["m_columns"][] = "lang_id";
$proto30["m_columns"][] = "view";
$proto30["m_columns"][] = "order";
$proto30["m_columns"][] = "parent";
$proto30["m_columns"][] = "title";
$proto30["m_columns"][] = "photo";
$proto30["m_columns"][] = "slider";
$proto30["m_columns"][] = "data";
$proto30["m_columns"][] = "meta_title";
$proto30["m_columns"][] = "meta_description";
$proto30["m_columns"][] = "meta_keywords";
$obj = new SQLTable($proto30);

$proto29["m_table"] = $obj;
$proto29["m_sql"] = "collegs";
$proto29["m_alias"] = "";
$proto29["m_srcTableName"] = "collegs";
$proto31=array();
$proto31["m_sql"] = "";
$proto31["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto31["m_column"]=$obj;
$proto31["m_contained"] = array();
$proto31["m_strCase"] = "";
$proto31["m_havingmode"] = false;
$proto31["m_inBrackets"] = false;
$proto31["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto31);

$proto29["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto29);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="collegs";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_collegs = createSqlQuery_collegs();


	
												
	
$tdatacollegs[".sqlquery"] = $queryData_collegs;

$tableEvents["collegs"] = new eventsBase;
$tdatacollegs[".hasEvents"] = false;

?>