<?php
require_once(getabspath("classes/cipherer.php"));




$tdatasite_statics = array();	
	$tdatasite_statics[".truncateText"] = true;
	$tdatasite_statics[".NumberOfChars"] = 80; 
	$tdatasite_statics[".ShortName"] = "site_statics";
	$tdatasite_statics[".OwnerID"] = "";
	$tdatasite_statics[".OriginalTable"] = "site_statics";

//	field labels
$fieldLabelssite_statics = array();
$fieldToolTipssite_statics = array();
$pageTitlessite_statics = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelssite_statics["English"] = array();
	$fieldToolTipssite_statics["English"] = array();
	$pageTitlessite_statics["English"] = array();
	$fieldLabelssite_statics["English"]["id"] = "Id";
	$fieldToolTipssite_statics["English"]["id"] = "";
	$fieldLabelssite_statics["English"]["marker"] = "Marker";
	$fieldToolTipssite_statics["English"]["marker"] = "";
	$fieldLabelssite_statics["English"]["lang_id"] = "Lang Id";
	$fieldToolTipssite_statics["English"]["lang_id"] = "";
	$fieldLabelssite_statics["English"]["data"] = "Data";
	$fieldToolTipssite_statics["English"]["data"] = "";
	if (count($fieldToolTipssite_statics["English"]))
		$tdatasite_statics[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelssite_statics["Arabic"] = array();
	$fieldToolTipssite_statics["Arabic"] = array();
	$pageTitlessite_statics["Arabic"] = array();
	$fieldLabelssite_statics["Arabic"]["id"] = "Id";
	$fieldToolTipssite_statics["Arabic"]["id"] = "";
	$fieldLabelssite_statics["Arabic"]["marker"] = "Marker";
	$fieldToolTipssite_statics["Arabic"]["marker"] = "";
	$fieldLabelssite_statics["Arabic"]["lang_id"] = "Lang Id";
	$fieldToolTipssite_statics["Arabic"]["lang_id"] = "";
	$fieldLabelssite_statics["Arabic"]["data"] = "Data";
	$fieldToolTipssite_statics["Arabic"]["data"] = "";
	if (count($fieldToolTipssite_statics["Arabic"]))
		$tdatasite_statics[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelssite_statics[""] = array();
	$fieldToolTipssite_statics[""] = array();
	$pageTitlessite_statics[""] = array();
	$fieldLabelssite_statics[""]["id"] = "Id";
	$fieldToolTipssite_statics[""]["id"] = "";
	$fieldLabelssite_statics[""]["marker"] = "Marker";
	$fieldToolTipssite_statics[""]["marker"] = "";
	$fieldLabelssite_statics[""]["lang_id"] = "Lang Id";
	$fieldToolTipssite_statics[""]["lang_id"] = "";
	$fieldLabelssite_statics[""]["data"] = "Data";
	$fieldToolTipssite_statics[""]["data"] = "";
	if (count($fieldToolTipssite_statics[""]))
		$tdatasite_statics[".isUseToolTips"] = true;
}
	
	
	$tdatasite_statics[".NCSearch"] = true;



$tdatasite_statics[".shortTableName"] = "site_statics";
$tdatasite_statics[".nSecOptions"] = 0;
$tdatasite_statics[".recsPerRowList"] = 1;
$tdatasite_statics[".mainTableOwnerID"] = "";
$tdatasite_statics[".moveNext"] = 1;
$tdatasite_statics[".nType"] = 0;

$tdatasite_statics[".strOriginalTableName"] = "site_statics";




$tdatasite_statics[".showAddInPopup"] = false;

$tdatasite_statics[".showEditInPopup"] = false;

$tdatasite_statics[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatasite_statics[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatasite_statics[".fieldsForRegister"] = array();

$tdatasite_statics[".listAjax"] = false;

	$tdatasite_statics[".audit"] = false;

	$tdatasite_statics[".locking"] = false;

$tdatasite_statics[".edit"] = true;

$tdatasite_statics[".list"] = true;

$tdatasite_statics[".view"] = true;




$tdatasite_statics[".delete"] = true;

$tdatasite_statics[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatasite_statics[".searchSaving"] = false;
//

$tdatasite_statics[".showSearchPanel"] = true;
		$tdatasite_statics[".flexibleSearch"] = true;		

if (isMobile())
	$tdatasite_statics[".isUseAjaxSuggest"] = false;
else 
	$tdatasite_statics[".isUseAjaxSuggest"] = true;

$tdatasite_statics[".rowHighlite"] = true;



$tdatasite_statics[".addPageEvents"] = false;

// use timepicker for search panel
$tdatasite_statics[".isUseTimeForSearch"] = false;





$tdatasite_statics[".allSearchFields"] = array();
$tdatasite_statics[".filterFields"] = array();
$tdatasite_statics[".requiredSearchFields"] = array();

$tdatasite_statics[".allSearchFields"][] = "id";
	$tdatasite_statics[".allSearchFields"][] = "marker";
	$tdatasite_statics[".allSearchFields"][] = "lang_id";
	$tdatasite_statics[".allSearchFields"][] = "data";
	

$tdatasite_statics[".googleLikeFields"] = array();
$tdatasite_statics[".googleLikeFields"][] = "id";
$tdatasite_statics[".googleLikeFields"][] = "marker";
$tdatasite_statics[".googleLikeFields"][] = "lang_id";
$tdatasite_statics[".googleLikeFields"][] = "data";


$tdatasite_statics[".advSearchFields"] = array();
$tdatasite_statics[".advSearchFields"][] = "id";
$tdatasite_statics[".advSearchFields"][] = "marker";
$tdatasite_statics[".advSearchFields"][] = "lang_id";
$tdatasite_statics[".advSearchFields"][] = "data";

$tdatasite_statics[".tableType"] = "list";

$tdatasite_statics[".printerPageOrientation"] = 0;
$tdatasite_statics[".nPrinterPageScale"] = 100;

$tdatasite_statics[".nPrinterSplitRecords"] = 40;

$tdatasite_statics[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdatasite_statics[".pageSize"] = 20;

$tdatasite_statics[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatasite_statics[".strOrderBy"] = $tstrOrderBy;

$tdatasite_statics[".orderindexes"] = array();

$tdatasite_statics[".sqlHead"] = "SELECT id,  	marker,  	lang_id,  	`data`";
$tdatasite_statics[".sqlFrom"] = "FROM site_statics";
$tdatasite_statics[".sqlWhereExpr"] = "";
$tdatasite_statics[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatasite_statics[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatasite_statics[".arrGroupsPerPage"] = $arrGPP;

$tdatasite_statics[".highlightSearchResults"] = true;

$tableKeyssite_statics = array();
$tableKeyssite_statics[] = "id";
$tdatasite_statics[".Keys"] = $tableKeyssite_statics;

$tdatasite_statics[".listFields"] = array();
$tdatasite_statics[".listFields"][] = "id";
$tdatasite_statics[".listFields"][] = "marker";
$tdatasite_statics[".listFields"][] = "lang_id";
$tdatasite_statics[".listFields"][] = "data";

$tdatasite_statics[".hideMobileList"] = array();


$tdatasite_statics[".viewFields"] = array();
$tdatasite_statics[".viewFields"][] = "id";
$tdatasite_statics[".viewFields"][] = "marker";
$tdatasite_statics[".viewFields"][] = "lang_id";
$tdatasite_statics[".viewFields"][] = "data";

$tdatasite_statics[".addFields"] = array();
$tdatasite_statics[".addFields"][] = "marker";
$tdatasite_statics[".addFields"][] = "lang_id";
$tdatasite_statics[".addFields"][] = "data";

$tdatasite_statics[".inlineAddFields"] = array();

$tdatasite_statics[".editFields"] = array();
$tdatasite_statics[".editFields"][] = "marker";
$tdatasite_statics[".editFields"][] = "lang_id";
$tdatasite_statics[".editFields"][] = "data";

$tdatasite_statics[".inlineEditFields"] = array();

$tdatasite_statics[".exportFields"] = array();

$tdatasite_statics[".importFields"] = array();

$tdatasite_statics[".printFields"] = array();

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "site_statics";
	$fdata["Label"] = GetFieldLabel("site_statics","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatasite_statics["id"] = $fdata;
//	marker
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "marker";
	$fdata["GoodName"] = "marker";
	$fdata["ownerTable"] = "site_statics";
	$fdata["Label"] = GetFieldLabel("site_statics","marker"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "marker"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "marker";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatasite_statics["marker"] = $fdata;
//	lang_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "lang_id";
	$fdata["GoodName"] = "lang_id";
	$fdata["ownerTable"] = "site_statics";
	$fdata["Label"] = GetFieldLabel("site_statics","lang_id"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "lang_id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lang_id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatasite_statics["lang_id"] = $fdata;
//	data
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "data";
	$fdata["GoodName"] = "data";
	$fdata["ownerTable"] = "site_statics";
	$fdata["Label"] = GetFieldLabel("site_statics","data"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "data"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`data`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 500;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatasite_statics["data"] = $fdata;

	
$tables_data["site_statics"]=&$tdatasite_statics;
$field_labels["site_statics"] = &$fieldLabelssite_statics;
$fieldToolTips["site_statics"] = &$fieldToolTipssite_statics;
$page_titles["site_statics"] = &$pageTitlessite_statics;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["site_statics"] = array();
	
// tables which are master tables for current table (detail)
$masterTablesData["site_statics"] = array();


	
				$strOriginalDetailsTable="langyages";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="langyages";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "langyages";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispInfo"]= "1";
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["site_statics"][0] = $masterParams;	
				$masterTablesData["site_statics"][0]["masterKeys"] = array();
	$masterTablesData["site_statics"][0]["masterKeys"][]="id";
				$masterTablesData["site_statics"][0]["detailKeys"] = array();
	$masterTablesData["site_statics"][0]["detailKeys"][]="lang_id";
		
	
				$strOriginalDetailsTable="site_statics_markers";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="site_statics_markers";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "site_statics_markers";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispInfo"]= "1";
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["site_statics"][1] = $masterParams;	
				$masterTablesData["site_statics"][1]["masterKeys"] = array();
	$masterTablesData["site_statics"][1]["masterKeys"][]="name";
				$masterTablesData["site_statics"][1]["detailKeys"] = array();
	$masterTablesData["site_statics"][1]["detailKeys"][]="marker";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_site_statics()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	marker,  	lang_id,  	`data`";
$proto0["m_strFrom"] = "FROM site_statics";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "site_statics",
	"m_srcTableName" => "site_statics"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "site_statics";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "marker",
	"m_strTable" => "site_statics",
	"m_srcTableName" => "site_statics"
));

$proto7["m_sql"] = "marker";
$proto7["m_srcTableName"] = "site_statics";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "lang_id",
	"m_strTable" => "site_statics",
	"m_srcTableName" => "site_statics"
));

$proto9["m_sql"] = "lang_id";
$proto9["m_srcTableName"] = "site_statics";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "site_statics",
	"m_srcTableName" => "site_statics"
));

$proto11["m_sql"] = "`data`";
$proto11["m_srcTableName"] = "site_statics";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto13=array();
$proto13["m_link"] = "SQLL_MAIN";
			$proto14=array();
$proto14["m_strName"] = "site_statics";
$proto14["m_srcTableName"] = "site_statics";
$proto14["m_columns"] = array();
$proto14["m_columns"][] = "id";
$proto14["m_columns"][] = "marker";
$proto14["m_columns"][] = "lang_id";
$proto14["m_columns"][] = "data";
$obj = new SQLTable($proto14);

$proto13["m_table"] = $obj;
$proto13["m_sql"] = "site_statics";
$proto13["m_alias"] = "";
$proto13["m_srcTableName"] = "site_statics";
$proto15=array();
$proto15["m_sql"] = "";
$proto15["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto15["m_column"]=$obj;
$proto15["m_contained"] = array();
$proto15["m_strCase"] = "";
$proto15["m_havingmode"] = false;
$proto15["m_inBrackets"] = false;
$proto15["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto15);

$proto13["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto13);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="site_statics";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_site_statics = createSqlQuery_site_statics();


	
				
	
$tdatasite_statics[".sqlquery"] = $queryData_site_statics;

$tableEvents["site_statics"] = new eventsBase;
$tdatasite_statics[".hasEvents"] = false;

?>