<?php

/**
* getLookupMainTableSettings - tests whether the lookup link exists between the tables
*
*  returns array with ProjectSettings class for main table if the link exists in project settings.
*  returns NULL otherwise
*/
function getLookupMainTableSettings($lookupTable, $mainTableShortName, $mainField, $desiredPage = "")
{
	global $lookupTableLinks;
	if(!isset($lookupTableLinks[$lookupTable]))
		return null;
	if(!isset($lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField]))
		return null;
	$arr = &$lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField];
	$effectivePage = $desiredPage;
	if(!isset($arr[$effectivePage]))
	{
		$effectivePage = PAGE_EDIT;
		if(!isset($arr[$effectivePage]))
		{
			if($desiredPage == "" && 0 < count($arr))
			{
				$effectivePage = $arr[0];
			}
			else
				return null;
		}
	}
	return new ProjectSettings($arr[$effectivePage]["table"], $effectivePage);
}

/** 
* $lookupTableLinks array stores all lookup links between tables in the project
*/
function InitLookupLinks()
{
	global $lookupTableLinks;

	$lookupTableLinks = array();

	$lookupTableLinks["articles"]["articles.parent"]["edit"] = array("table" => "articles", "field" => "parent", "page" => "edit");
	$lookupTableLinks["langyages"]["articles.lang_id"]["edit"] = array("table" => "articles", "field" => "lang_id", "page" => "edit");
	$lookupTableLinks["langyages"]["collegs.lang_id"]["edit"] = array("table" => "collegs", "field" => "lang_id", "page" => "edit");
	$lookupTableLinks["collegs"]["collegs.parent"]["edit"] = array("table" => "collegs", "field" => "parent", "page" => "edit");
	$lookupTableLinks["langyages"]["events.lang_id"]["edit"] = array("table" => "events", "field" => "lang_id", "page" => "edit");
	$lookupTableLinks["langyages"]["statec_words_translation.lang_id"]["edit"] = array("table" => "statec_words_translation", "field" => "lang_id", "page" => "edit");
	$lookupTableLinks["statec_words"]["statec_words_translation.word_id"]["edit"] = array("table" => "statec_words_translation", "field" => "word_id", "page" => "edit");
	$lookupTableLinks["langyages"]["home_slider.lang_id"]["edit"] = array("table" => "home_slider", "field" => "lang_id", "page" => "edit");
	$lookupTableLinks["langyages"]["home_bottom_dialogs.lang_id"]["edit"] = array("table" => "home_bottom_dialogs", "field" => "lang_id", "page" => "edit");
	$lookupTableLinks["langyages"]["home_main_dialogs.lang_id"]["edit"] = array("table" => "home_main_dialogs", "field" => "lang_id", "page" => "edit");
	$lookupTableLinks["langyages"]["news.lang_id"]["edit"] = array("table" => "news", "field" => "lang_id", "page" => "edit");
}

?>