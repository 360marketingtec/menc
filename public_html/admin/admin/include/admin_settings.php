<?php
require_once(getabspath("classes/cipherer.php"));




$tdataadmin = array();	
	$tdataadmin[".truncateText"] = true;
	$tdataadmin[".NumberOfChars"] = 80; 
	$tdataadmin[".ShortName"] = "admin";
	$tdataadmin[".OwnerID"] = "";
	$tdataadmin[".OriginalTable"] = "admin";

//	field labels
$fieldLabelsadmin = array();
$fieldToolTipsadmin = array();
$pageTitlesadmin = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsadmin["English"] = array();
	$fieldToolTipsadmin["English"] = array();
	$pageTitlesadmin["English"] = array();
	$fieldLabelsadmin["English"]["id"] = "Id";
	$fieldToolTipsadmin["English"]["id"] = "";
	$fieldLabelsadmin["English"]["username"] = "Username";
	$fieldToolTipsadmin["English"]["username"] = "";
	$fieldLabelsadmin["English"]["password"] = "Password";
	$fieldToolTipsadmin["English"]["password"] = "";
	if (count($fieldToolTipsadmin["English"]))
		$tdataadmin[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelsadmin["Arabic"] = array();
	$fieldToolTipsadmin["Arabic"] = array();
	$pageTitlesadmin["Arabic"] = array();
	$fieldLabelsadmin["Arabic"]["id"] = "Id";
	$fieldToolTipsadmin["Arabic"]["id"] = "";
	$fieldLabelsadmin["Arabic"]["username"] = "Username";
	$fieldToolTipsadmin["Arabic"]["username"] = "";
	$fieldLabelsadmin["Arabic"]["password"] = "Password";
	$fieldToolTipsadmin["Arabic"]["password"] = "";
	if (count($fieldToolTipsadmin["Arabic"]))
		$tdataadmin[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsadmin[""] = array();
	$fieldToolTipsadmin[""] = array();
	$pageTitlesadmin[""] = array();
	$fieldLabelsadmin[""]["id"] = "Id";
	$fieldToolTipsadmin[""]["id"] = "";
	if (count($fieldToolTipsadmin[""]))
		$tdataadmin[".isUseToolTips"] = true;
}
	
	
	$tdataadmin[".NCSearch"] = true;



$tdataadmin[".shortTableName"] = "admin";
$tdataadmin[".nSecOptions"] = 0;
$tdataadmin[".recsPerRowList"] = 1;
$tdataadmin[".mainTableOwnerID"] = "";
$tdataadmin[".moveNext"] = 1;
$tdataadmin[".nType"] = 0;

$tdataadmin[".strOriginalTableName"] = "admin";




$tdataadmin[".showAddInPopup"] = false;

$tdataadmin[".showEditInPopup"] = false;

$tdataadmin[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataadmin[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataadmin[".fieldsForRegister"] = array();

$tdataadmin[".listAjax"] = false;

	$tdataadmin[".audit"] = true;

	$tdataadmin[".locking"] = true;

$tdataadmin[".edit"] = true;

$tdataadmin[".list"] = true;

$tdataadmin[".inlineEdit"] = true;
$tdataadmin[".inlineAdd"] = true;
$tdataadmin[".view"] = true;

$tdataadmin[".import"] = true;

$tdataadmin[".exportTo"] = true;

$tdataadmin[".printFriendly"] = true;

$tdataadmin[".delete"] = true;

$tdataadmin[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdataadmin[".searchSaving"] = false;
//

$tdataadmin[".showSearchPanel"] = true;
		$tdataadmin[".flexibleSearch"] = true;		

if (isMobile())
	$tdataadmin[".isUseAjaxSuggest"] = false;
else 
	$tdataadmin[".isUseAjaxSuggest"] = true;

$tdataadmin[".rowHighlite"] = true;



$tdataadmin[".addPageEvents"] = false;

// use timepicker for search panel
$tdataadmin[".isUseTimeForSearch"] = false;





$tdataadmin[".allSearchFields"] = array();
$tdataadmin[".filterFields"] = array();
$tdataadmin[".requiredSearchFields"] = array();

$tdataadmin[".allSearchFields"][] = "id";
	$tdataadmin[".allSearchFields"][] = "username";
	$tdataadmin[".allSearchFields"][] = "password";
	

$tdataadmin[".googleLikeFields"] = array();
$tdataadmin[".googleLikeFields"][] = "id";
$tdataadmin[".googleLikeFields"][] = "username";
$tdataadmin[".googleLikeFields"][] = "password";


$tdataadmin[".advSearchFields"] = array();
$tdataadmin[".advSearchFields"][] = "id";
$tdataadmin[".advSearchFields"][] = "username";
$tdataadmin[".advSearchFields"][] = "password";

$tdataadmin[".tableType"] = "list";

$tdataadmin[".printerPageOrientation"] = 0;
$tdataadmin[".nPrinterPageScale"] = 100;

$tdataadmin[".nPrinterSplitRecords"] = 40;

$tdataadmin[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdataadmin[".pageSize"] = 20;

$tdataadmin[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataadmin[".strOrderBy"] = $tstrOrderBy;

$tdataadmin[".orderindexes"] = array();

$tdataadmin[".sqlHead"] = "SELECT id,  	username,  	password";
$tdataadmin[".sqlFrom"] = "FROM `admin`";
$tdataadmin[".sqlWhereExpr"] = "";
$tdataadmin[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataadmin[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataadmin[".arrGroupsPerPage"] = $arrGPP;

$tdataadmin[".highlightSearchResults"] = true;

$tableKeysadmin = array();
$tableKeysadmin[] = "id";
$tdataadmin[".Keys"] = $tableKeysadmin;

$tdataadmin[".listFields"] = array();
$tdataadmin[".listFields"][] = "id";
$tdataadmin[".listFields"][] = "username";
$tdataadmin[".listFields"][] = "password";

$tdataadmin[".hideMobileList"] = array();


$tdataadmin[".viewFields"] = array();
$tdataadmin[".viewFields"][] = "id";
$tdataadmin[".viewFields"][] = "username";
$tdataadmin[".viewFields"][] = "password";

$tdataadmin[".addFields"] = array();
$tdataadmin[".addFields"][] = "username";
$tdataadmin[".addFields"][] = "password";

$tdataadmin[".inlineAddFields"] = array();
$tdataadmin[".inlineAddFields"][] = "username";
$tdataadmin[".inlineAddFields"][] = "password";

$tdataadmin[".editFields"] = array();
$tdataadmin[".editFields"][] = "username";
$tdataadmin[".editFields"][] = "password";

$tdataadmin[".inlineEditFields"] = array();
$tdataadmin[".inlineEditFields"][] = "username";
$tdataadmin[".inlineEditFields"][] = "password";

$tdataadmin[".exportFields"] = array();
$tdataadmin[".exportFields"][] = "id";
$tdataadmin[".exportFields"][] = "username";
$tdataadmin[".exportFields"][] = "password";

$tdataadmin[".importFields"] = array();
$tdataadmin[".importFields"][] = "id";
$tdataadmin[".importFields"][] = "username";
$tdataadmin[".importFields"][] = "password";

$tdataadmin[".printFields"] = array();
$tdataadmin[".printFields"][] = "id";
$tdataadmin[".printFields"][] = "username";
$tdataadmin[".printFields"][] = "password";

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "admin";
	$fdata["Label"] = GetFieldLabel("admin","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataadmin["id"] = $fdata;
//	username
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "username";
	$fdata["GoodName"] = "username";
	$fdata["ownerTable"] = "admin";
	$fdata["Label"] = GetFieldLabel("admin","username"); 
	$fdata["FieldType"] = 200;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		$fdata["bInlineAdd"] = true; 
	
		$fdata["bEditPage"] = true; 
	
		$fdata["bInlineEdit"] = true; 
	
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "username"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "username";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";
	
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataadmin["username"] = $fdata;
//	password
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "password";
	$fdata["GoodName"] = "password";
	$fdata["ownerTable"] = "admin";
	$fdata["Label"] = GetFieldLabel("admin","password"); 
	$fdata["FieldType"] = 200;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		$fdata["bInlineAdd"] = true; 
	
		$fdata["bEditPage"] = true; 
	
		$fdata["bInlineEdit"] = true; 
	
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "password"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "password";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";
	
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataadmin["password"] = $fdata;

	
$tables_data["admin"]=&$tdataadmin;
$field_labels["admin"] = &$fieldLabelsadmin;
$fieldToolTips["admin"] = &$fieldToolTipsadmin;
$page_titles["admin"] = &$pageTitlesadmin;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["admin"] = array();
	
// tables which are master tables for current table (detail)
$masterTablesData["admin"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_admin()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	username,  	password";
$proto0["m_strFrom"] = "FROM `admin`";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "admin",
	"m_srcTableName" => "admin"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "admin";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "username",
	"m_strTable" => "admin",
	"m_srcTableName" => "admin"
));

$proto7["m_sql"] = "username";
$proto7["m_srcTableName"] = "admin";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "password",
	"m_strTable" => "admin",
	"m_srcTableName" => "admin"
));

$proto9["m_sql"] = "password";
$proto9["m_srcTableName"] = "admin";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto11=array();
$proto11["m_link"] = "SQLL_MAIN";
			$proto12=array();
$proto12["m_strName"] = "admin";
$proto12["m_srcTableName"] = "admin";
$proto12["m_columns"] = array();
$proto12["m_columns"][] = "id";
$proto12["m_columns"][] = "username";
$proto12["m_columns"][] = "password";
$obj = new SQLTable($proto12);

$proto11["m_table"] = $obj;
$proto11["m_sql"] = "`admin`";
$proto11["m_alias"] = "";
$proto11["m_srcTableName"] = "admin";
$proto13=array();
$proto13["m_sql"] = "";
$proto13["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto13["m_column"]=$obj;
$proto13["m_contained"] = array();
$proto13["m_strCase"] = "";
$proto13["m_havingmode"] = false;
$proto13["m_inBrackets"] = false;
$proto13["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto13);

$proto11["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto11);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="admin";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_admin = createSqlQuery_admin();


	
			
	
$tdataadmin[".sqlquery"] = $queryData_admin;

$tableEvents["admin"] = new eventsBase;
$tdataadmin[".hasEvents"] = false;

?>