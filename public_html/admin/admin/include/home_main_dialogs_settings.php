<?php
require_once(getabspath("classes/cipherer.php"));




$tdatahome_main_dialogs = array();	
	$tdatahome_main_dialogs[".truncateText"] = true;
	$tdatahome_main_dialogs[".NumberOfChars"] = 80; 
	$tdatahome_main_dialogs[".ShortName"] = "home_main_dialogs";
	$tdatahome_main_dialogs[".OwnerID"] = "";
	$tdatahome_main_dialogs[".OriginalTable"] = "home_main_dialogs";

//	field labels
$fieldLabelshome_main_dialogs = array();
$fieldToolTipshome_main_dialogs = array();
$pageTitleshome_main_dialogs = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelshome_main_dialogs["English"] = array();
	$fieldToolTipshome_main_dialogs["English"] = array();
	$pageTitleshome_main_dialogs["English"] = array();
	$fieldLabelshome_main_dialogs["English"]["id"] = "Id";
	$fieldToolTipshome_main_dialogs["English"]["id"] = "";
	$fieldLabelshome_main_dialogs["English"]["title"] = "Title";
	$fieldToolTipshome_main_dialogs["English"]["title"] = "";
	$fieldLabelshome_main_dialogs["English"]["desc"] = "Desc";
	$fieldToolTipshome_main_dialogs["English"]["desc"] = "";
	$fieldLabelshome_main_dialogs["English"]["photo"] = "Photo";
	$fieldToolTipshome_main_dialogs["English"]["photo"] = "";
	$fieldLabelshome_main_dialogs["English"]["link"] = "Link";
	$fieldToolTipshome_main_dialogs["English"]["link"] = "";
	$fieldLabelshome_main_dialogs["English"]["order"] = "Order";
	$fieldToolTipshome_main_dialogs["English"]["order"] = "";
	$fieldLabelshome_main_dialogs["English"]["view"] = "View";
	$fieldToolTipshome_main_dialogs["English"]["view"] = "";
	$fieldLabelshome_main_dialogs["English"]["lang_id"] = "Lang Id";
	$fieldToolTipshome_main_dialogs["English"]["lang_id"] = "";
	if (count($fieldToolTipshome_main_dialogs["English"]))
		$tdatahome_main_dialogs[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelshome_main_dialogs["Arabic"] = array();
	$fieldToolTipshome_main_dialogs["Arabic"] = array();
	$pageTitleshome_main_dialogs["Arabic"] = array();
	$fieldLabelshome_main_dialogs["Arabic"]["id"] = "Id";
	$fieldToolTipshome_main_dialogs["Arabic"]["id"] = "";
	$fieldLabelshome_main_dialogs["Arabic"]["title"] = "Title";
	$fieldToolTipshome_main_dialogs["Arabic"]["title"] = "";
	$fieldLabelshome_main_dialogs["Arabic"]["desc"] = "Desc";
	$fieldToolTipshome_main_dialogs["Arabic"]["desc"] = "";
	$fieldLabelshome_main_dialogs["Arabic"]["photo"] = "Photo";
	$fieldToolTipshome_main_dialogs["Arabic"]["photo"] = "";
	$fieldLabelshome_main_dialogs["Arabic"]["link"] = "Link";
	$fieldToolTipshome_main_dialogs["Arabic"]["link"] = "";
	$fieldLabelshome_main_dialogs["Arabic"]["order"] = "Order";
	$fieldToolTipshome_main_dialogs["Arabic"]["order"] = "";
	$fieldLabelshome_main_dialogs["Arabic"]["view"] = "View";
	$fieldToolTipshome_main_dialogs["Arabic"]["view"] = "";
	$fieldLabelshome_main_dialogs["Arabic"]["lang_id"] = "Lang Id";
	$fieldToolTipshome_main_dialogs["Arabic"]["lang_id"] = "";
	if (count($fieldToolTipshome_main_dialogs["Arabic"]))
		$tdatahome_main_dialogs[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelshome_main_dialogs[""] = array();
	$fieldToolTipshome_main_dialogs[""] = array();
	$pageTitleshome_main_dialogs[""] = array();
	$fieldLabelshome_main_dialogs[""]["id"] = "Id";
	$fieldToolTipshome_main_dialogs[""]["id"] = "";
	$fieldLabelshome_main_dialogs[""]["title"] = "Title";
	$fieldToolTipshome_main_dialogs[""]["title"] = "";
	$fieldLabelshome_main_dialogs[""]["desc"] = "Desc";
	$fieldToolTipshome_main_dialogs[""]["desc"] = "";
	$fieldLabelshome_main_dialogs[""]["photo"] = "Photo";
	$fieldToolTipshome_main_dialogs[""]["photo"] = "";
	$fieldLabelshome_main_dialogs[""]["link"] = "Link";
	$fieldToolTipshome_main_dialogs[""]["link"] = "";
	$fieldLabelshome_main_dialogs[""]["order"] = "Order";
	$fieldToolTipshome_main_dialogs[""]["order"] = "";
	$fieldLabelshome_main_dialogs[""]["view"] = "View";
	$fieldToolTipshome_main_dialogs[""]["view"] = "";
	$fieldLabelshome_main_dialogs[""]["lang_id"] = "Lang Id";
	$fieldToolTipshome_main_dialogs[""]["lang_id"] = "";
	if (count($fieldToolTipshome_main_dialogs[""]))
		$tdatahome_main_dialogs[".isUseToolTips"] = true;
}
	
	
	$tdatahome_main_dialogs[".NCSearch"] = true;



$tdatahome_main_dialogs[".shortTableName"] = "home_main_dialogs";
$tdatahome_main_dialogs[".nSecOptions"] = 0;
$tdatahome_main_dialogs[".recsPerRowList"] = 1;
$tdatahome_main_dialogs[".mainTableOwnerID"] = "";
$tdatahome_main_dialogs[".moveNext"] = 1;
$tdatahome_main_dialogs[".nType"] = 0;

$tdatahome_main_dialogs[".strOriginalTableName"] = "home_main_dialogs";




$tdatahome_main_dialogs[".showAddInPopup"] = false;

$tdatahome_main_dialogs[".showEditInPopup"] = false;

$tdatahome_main_dialogs[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatahome_main_dialogs[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatahome_main_dialogs[".fieldsForRegister"] = array();

$tdatahome_main_dialogs[".listAjax"] = false;

	$tdatahome_main_dialogs[".audit"] = false;

	$tdatahome_main_dialogs[".locking"] = false;

$tdatahome_main_dialogs[".edit"] = true;

$tdatahome_main_dialogs[".list"] = true;

$tdatahome_main_dialogs[".view"] = true;


$tdatahome_main_dialogs[".exportTo"] = true;

$tdatahome_main_dialogs[".printFriendly"] = true;

$tdatahome_main_dialogs[".delete"] = true;

$tdatahome_main_dialogs[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatahome_main_dialogs[".searchSaving"] = false;
//

$tdatahome_main_dialogs[".showSearchPanel"] = true;
		$tdatahome_main_dialogs[".flexibleSearch"] = true;		

if (isMobile())
	$tdatahome_main_dialogs[".isUseAjaxSuggest"] = false;
else 
	$tdatahome_main_dialogs[".isUseAjaxSuggest"] = true;

$tdatahome_main_dialogs[".rowHighlite"] = true;



$tdatahome_main_dialogs[".addPageEvents"] = false;

// use timepicker for search panel
$tdatahome_main_dialogs[".isUseTimeForSearch"] = false;





$tdatahome_main_dialogs[".allSearchFields"] = array();
$tdatahome_main_dialogs[".filterFields"] = array();
$tdatahome_main_dialogs[".requiredSearchFields"] = array();

$tdatahome_main_dialogs[".allSearchFields"][] = "id";
	$tdatahome_main_dialogs[".allSearchFields"][] = "lang_id";
	$tdatahome_main_dialogs[".allSearchFields"][] = "view";
	$tdatahome_main_dialogs[".allSearchFields"][] = "order";
	$tdatahome_main_dialogs[".allSearchFields"][] = "title";
	$tdatahome_main_dialogs[".allSearchFields"][] = "desc";
	$tdatahome_main_dialogs[".allSearchFields"][] = "photo";
	$tdatahome_main_dialogs[".allSearchFields"][] = "link";
	

$tdatahome_main_dialogs[".googleLikeFields"] = array();
$tdatahome_main_dialogs[".googleLikeFields"][] = "id";
$tdatahome_main_dialogs[".googleLikeFields"][] = "title";
$tdatahome_main_dialogs[".googleLikeFields"][] = "desc";
$tdatahome_main_dialogs[".googleLikeFields"][] = "photo";
$tdatahome_main_dialogs[".googleLikeFields"][] = "link";
$tdatahome_main_dialogs[".googleLikeFields"][] = "order";
$tdatahome_main_dialogs[".googleLikeFields"][] = "view";
$tdatahome_main_dialogs[".googleLikeFields"][] = "lang_id";


$tdatahome_main_dialogs[".advSearchFields"] = array();
$tdatahome_main_dialogs[".advSearchFields"][] = "id";
$tdatahome_main_dialogs[".advSearchFields"][] = "lang_id";
$tdatahome_main_dialogs[".advSearchFields"][] = "view";
$tdatahome_main_dialogs[".advSearchFields"][] = "order";
$tdatahome_main_dialogs[".advSearchFields"][] = "title";
$tdatahome_main_dialogs[".advSearchFields"][] = "desc";
$tdatahome_main_dialogs[".advSearchFields"][] = "photo";
$tdatahome_main_dialogs[".advSearchFields"][] = "link";

$tdatahome_main_dialogs[".tableType"] = "list";

$tdatahome_main_dialogs[".printerPageOrientation"] = 0;
$tdatahome_main_dialogs[".nPrinterPageScale"] = 100;

$tdatahome_main_dialogs[".nPrinterSplitRecords"] = 40;

$tdatahome_main_dialogs[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdatahome_main_dialogs[".pageSize"] = 20;

$tdatahome_main_dialogs[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatahome_main_dialogs[".strOrderBy"] = $tstrOrderBy;

$tdatahome_main_dialogs[".orderindexes"] = array();

$tdatahome_main_dialogs[".sqlHead"] = "SELECT id,  	title,  	`desc`,  	photo,  	link,  	`order`,  	`view`,  	lang_id";
$tdatahome_main_dialogs[".sqlFrom"] = "FROM home_main_dialogs";
$tdatahome_main_dialogs[".sqlWhereExpr"] = "";
$tdatahome_main_dialogs[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatahome_main_dialogs[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatahome_main_dialogs[".arrGroupsPerPage"] = $arrGPP;

$tdatahome_main_dialogs[".highlightSearchResults"] = true;

$tableKeyshome_main_dialogs = array();
$tableKeyshome_main_dialogs[] = "id";
$tdatahome_main_dialogs[".Keys"] = $tableKeyshome_main_dialogs;

$tdatahome_main_dialogs[".listFields"] = array();
$tdatahome_main_dialogs[".listFields"][] = "id";
$tdatahome_main_dialogs[".listFields"][] = "lang_id";
$tdatahome_main_dialogs[".listFields"][] = "view";
$tdatahome_main_dialogs[".listFields"][] = "order";
$tdatahome_main_dialogs[".listFields"][] = "title";

$tdatahome_main_dialogs[".hideMobileList"] = array();


$tdatahome_main_dialogs[".viewFields"] = array();
$tdatahome_main_dialogs[".viewFields"][] = "id";
$tdatahome_main_dialogs[".viewFields"][] = "lang_id";
$tdatahome_main_dialogs[".viewFields"][] = "view";
$tdatahome_main_dialogs[".viewFields"][] = "order";
$tdatahome_main_dialogs[".viewFields"][] = "title";
$tdatahome_main_dialogs[".viewFields"][] = "desc";
$tdatahome_main_dialogs[".viewFields"][] = "photo";
$tdatahome_main_dialogs[".viewFields"][] = "link";

$tdatahome_main_dialogs[".addFields"] = array();
$tdatahome_main_dialogs[".addFields"][] = "lang_id";
$tdatahome_main_dialogs[".addFields"][] = "view";
$tdatahome_main_dialogs[".addFields"][] = "order";
$tdatahome_main_dialogs[".addFields"][] = "title";
$tdatahome_main_dialogs[".addFields"][] = "desc";
$tdatahome_main_dialogs[".addFields"][] = "photo";
$tdatahome_main_dialogs[".addFields"][] = "link";

$tdatahome_main_dialogs[".inlineAddFields"] = array();

$tdatahome_main_dialogs[".editFields"] = array();
$tdatahome_main_dialogs[".editFields"][] = "lang_id";
$tdatahome_main_dialogs[".editFields"][] = "view";
$tdatahome_main_dialogs[".editFields"][] = "order";
$tdatahome_main_dialogs[".editFields"][] = "title";
$tdatahome_main_dialogs[".editFields"][] = "desc";
$tdatahome_main_dialogs[".editFields"][] = "photo";
$tdatahome_main_dialogs[".editFields"][] = "link";

$tdatahome_main_dialogs[".inlineEditFields"] = array();

$tdatahome_main_dialogs[".exportFields"] = array();
$tdatahome_main_dialogs[".exportFields"][] = "id";
$tdatahome_main_dialogs[".exportFields"][] = "lang_id";
$tdatahome_main_dialogs[".exportFields"][] = "view";
$tdatahome_main_dialogs[".exportFields"][] = "order";
$tdatahome_main_dialogs[".exportFields"][] = "title";
$tdatahome_main_dialogs[".exportFields"][] = "desc";
$tdatahome_main_dialogs[".exportFields"][] = "photo";
$tdatahome_main_dialogs[".exportFields"][] = "link";

$tdatahome_main_dialogs[".importFields"] = array();

$tdatahome_main_dialogs[".printFields"] = array();
$tdatahome_main_dialogs[".printFields"][] = "id";
$tdatahome_main_dialogs[".printFields"][] = "lang_id";
$tdatahome_main_dialogs[".printFields"][] = "view";
$tdatahome_main_dialogs[".printFields"][] = "order";
$tdatahome_main_dialogs[".printFields"][] = "title";
$tdatahome_main_dialogs[".printFields"][] = "desc";
$tdatahome_main_dialogs[".printFields"][] = "photo";
$tdatahome_main_dialogs[".printFields"][] = "link";

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "home_main_dialogs";
	$fdata["Label"] = GetFieldLabel("home_main_dialogs","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatahome_main_dialogs["id"] = $fdata;
//	title
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "title";
	$fdata["GoodName"] = "title";
	$fdata["ownerTable"] = "home_main_dialogs";
	$fdata["Label"] = GetFieldLabel("home_main_dialogs","title"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "title"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "title";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatahome_main_dialogs["title"] = $fdata;
//	desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "desc";
	$fdata["GoodName"] = "desc";
	$fdata["ownerTable"] = "home_main_dialogs";
	$fdata["Label"] = GetFieldLabel("home_main_dialogs","desc"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "desc"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`desc`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatahome_main_dialogs["desc"] = $fdata;
//	photo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "photo";
	$fdata["GoodName"] = "photo";
	$fdata["ownerTable"] = "home_main_dialogs";
	$fdata["Label"] = GetFieldLabel("home_main_dialogs","photo"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "photo"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "photo";
	
		$fdata["DeleteAssociatedFile"] = true;
	
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "../uploads/";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Document Download");
	
		
		
		
								$vdata["ShowIcon"] = true; 
			
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Document upload");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
							$edata["acceptFileTypes"] = "bmp";
						$edata["acceptFileTypes"] .= "|gif";
						$edata["acceptFileTypes"] .= "|jpg";
						$edata["acceptFileTypes"] .= "|png";
		$edata["acceptFileTypes"] = "(".$edata["acceptFileTypes"].")$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatahome_main_dialogs["photo"] = $fdata;
//	link
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "link";
	$fdata["GoodName"] = "link";
	$fdata["ownerTable"] = "home_main_dialogs";
	$fdata["Label"] = GetFieldLabel("home_main_dialogs","link"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "link"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatahome_main_dialogs["link"] = $fdata;
//	order
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "order";
	$fdata["GoodName"] = "order";
	$fdata["ownerTable"] = "home_main_dialogs";
	$fdata["Label"] = GetFieldLabel("home_main_dialogs","order"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "order"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`order`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatahome_main_dialogs["order"] = $fdata;
//	view
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "view";
	$fdata["GoodName"] = "view";
	$fdata["ownerTable"] = "home_main_dialogs";
	$fdata["Label"] = GetFieldLabel("home_main_dialogs","view"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "view"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`view`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Checkbox");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Checkbox");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatahome_main_dialogs["view"] = $fdata;
//	lang_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "lang_id";
	$fdata["GoodName"] = "lang_id";
	$fdata["ownerTable"] = "home_main_dialogs";
	$fdata["Label"] = GetFieldLabel("home_main_dialogs","lang_id"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "lang_id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lang_id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Lookup wizard");
	
			
	
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "langyages";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
		
		
			
	$edata["LinkField"] = "id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "name";
	
		
	$edata["LookupOrderBy"] = "";
	
		
			
		
				
	
	
		
		$edata["SelectSize"] = 1;
		
// End Lookup Settings


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatahome_main_dialogs["lang_id"] = $fdata;

	
$tables_data["home_main_dialogs"]=&$tdatahome_main_dialogs;
$field_labels["home_main_dialogs"] = &$fieldLabelshome_main_dialogs;
$fieldToolTips["home_main_dialogs"] = &$fieldToolTipshome_main_dialogs;
$page_titles["home_main_dialogs"] = &$pageTitleshome_main_dialogs;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["home_main_dialogs"] = array();
	
// tables which are master tables for current table (detail)
$masterTablesData["home_main_dialogs"] = array();


	
				$strOriginalDetailsTable="langyages";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="langyages";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "langyages";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispInfo"]= "1";
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["home_main_dialogs"][0] = $masterParams;	
				$masterTablesData["home_main_dialogs"][0]["masterKeys"] = array();
	$masterTablesData["home_main_dialogs"][0]["masterKeys"][]="id";
				$masterTablesData["home_main_dialogs"][0]["detailKeys"] = array();
	$masterTablesData["home_main_dialogs"][0]["detailKeys"][]="lang_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_home_main_dialogs()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	title,  	`desc`,  	photo,  	link,  	`order`,  	`view`,  	lang_id";
$proto0["m_strFrom"] = "FROM home_main_dialogs";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "home_main_dialogs",
	"m_srcTableName" => "home_main_dialogs"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "home_main_dialogs";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "title",
	"m_strTable" => "home_main_dialogs",
	"m_srcTableName" => "home_main_dialogs"
));

$proto7["m_sql"] = "title";
$proto7["m_srcTableName"] = "home_main_dialogs";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "desc",
	"m_strTable" => "home_main_dialogs",
	"m_srcTableName" => "home_main_dialogs"
));

$proto9["m_sql"] = "`desc`";
$proto9["m_srcTableName"] = "home_main_dialogs";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "photo",
	"m_strTable" => "home_main_dialogs",
	"m_srcTableName" => "home_main_dialogs"
));

$proto11["m_sql"] = "photo";
$proto11["m_srcTableName"] = "home_main_dialogs";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "link",
	"m_strTable" => "home_main_dialogs",
	"m_srcTableName" => "home_main_dialogs"
));

$proto13["m_sql"] = "link";
$proto13["m_srcTableName"] = "home_main_dialogs";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "order",
	"m_strTable" => "home_main_dialogs",
	"m_srcTableName" => "home_main_dialogs"
));

$proto15["m_sql"] = "`order`";
$proto15["m_srcTableName"] = "home_main_dialogs";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "view",
	"m_strTable" => "home_main_dialogs",
	"m_srcTableName" => "home_main_dialogs"
));

$proto17["m_sql"] = "`view`";
$proto17["m_srcTableName"] = "home_main_dialogs";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "lang_id",
	"m_strTable" => "home_main_dialogs",
	"m_srcTableName" => "home_main_dialogs"
));

$proto19["m_sql"] = "lang_id";
$proto19["m_srcTableName"] = "home_main_dialogs";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto21=array();
$proto21["m_link"] = "SQLL_MAIN";
			$proto22=array();
$proto22["m_strName"] = "home_main_dialogs";
$proto22["m_srcTableName"] = "home_main_dialogs";
$proto22["m_columns"] = array();
$proto22["m_columns"][] = "id";
$proto22["m_columns"][] = "title";
$proto22["m_columns"][] = "desc";
$proto22["m_columns"][] = "photo";
$proto22["m_columns"][] = "link";
$proto22["m_columns"][] = "order";
$proto22["m_columns"][] = "view";
$proto22["m_columns"][] = "lang_id";
$obj = new SQLTable($proto22);

$proto21["m_table"] = $obj;
$proto21["m_sql"] = "home_main_dialogs";
$proto21["m_alias"] = "";
$proto21["m_srcTableName"] = "home_main_dialogs";
$proto23=array();
$proto23["m_sql"] = "";
$proto23["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto23["m_column"]=$obj;
$proto23["m_contained"] = array();
$proto23["m_strCase"] = "";
$proto23["m_havingmode"] = false;
$proto23["m_inBrackets"] = false;
$proto23["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto23);

$proto21["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto21);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="home_main_dialogs";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_home_main_dialogs = createSqlQuery_home_main_dialogs();


	
								
	
$tdatahome_main_dialogs[".sqlquery"] = $queryData_home_main_dialogs;

$tableEvents["home_main_dialogs"] = new eventsBase;
$tdatahome_main_dialogs[".hasEvents"] = false;

?>