<?php
require_once(getabspath("classes/cipherer.php"));




$tdatastatec_words = array();	
	$tdatastatec_words[".truncateText"] = true;
	$tdatastatec_words[".NumberOfChars"] = 80; 
	$tdatastatec_words[".ShortName"] = "statec_words";
	$tdatastatec_words[".OwnerID"] = "";
	$tdatastatec_words[".OriginalTable"] = "statec_words";

//	field labels
$fieldLabelsstatec_words = array();
$fieldToolTipsstatec_words = array();
$pageTitlesstatec_words = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsstatec_words["English"] = array();
	$fieldToolTipsstatec_words["English"] = array();
	$pageTitlesstatec_words["English"] = array();
	$fieldLabelsstatec_words["English"]["id"] = "Id";
	$fieldToolTipsstatec_words["English"]["id"] = "";
	$fieldLabelsstatec_words["English"]["word"] = "Word";
	$fieldToolTipsstatec_words["English"]["word"] = "";
	if (count($fieldToolTipsstatec_words["English"]))
		$tdatastatec_words[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelsstatec_words["Arabic"] = array();
	$fieldToolTipsstatec_words["Arabic"] = array();
	$pageTitlesstatec_words["Arabic"] = array();
	$fieldLabelsstatec_words["Arabic"]["id"] = "Id";
	$fieldToolTipsstatec_words["Arabic"]["id"] = "";
	$fieldLabelsstatec_words["Arabic"]["word"] = "Word";
	$fieldToolTipsstatec_words["Arabic"]["word"] = "";
	if (count($fieldToolTipsstatec_words["Arabic"]))
		$tdatastatec_words[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsstatec_words[""] = array();
	$fieldToolTipsstatec_words[""] = array();
	$pageTitlesstatec_words[""] = array();
	if (count($fieldToolTipsstatec_words[""]))
		$tdatastatec_words[".isUseToolTips"] = true;
}
	
	
	$tdatastatec_words[".NCSearch"] = true;



$tdatastatec_words[".shortTableName"] = "statec_words";
$tdatastatec_words[".nSecOptions"] = 0;
$tdatastatec_words[".recsPerRowList"] = 1;
$tdatastatec_words[".mainTableOwnerID"] = "";
$tdatastatec_words[".moveNext"] = 1;
$tdatastatec_words[".nType"] = 0;

$tdatastatec_words[".strOriginalTableName"] = "statec_words";




$tdatastatec_words[".showAddInPopup"] = false;

$tdatastatec_words[".showEditInPopup"] = false;

$tdatastatec_words[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatastatec_words[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatastatec_words[".fieldsForRegister"] = array();

$tdatastatec_words[".listAjax"] = false;

	$tdatastatec_words[".audit"] = true;

	$tdatastatec_words[".locking"] = true;


$tdatastatec_words[".list"] = true;

$tdatastatec_words[".view"] = true;





$tdatastatec_words[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatastatec_words[".searchSaving"] = false;
//

$tdatastatec_words[".showSearchPanel"] = true;
		$tdatastatec_words[".flexibleSearch"] = true;		

if (isMobile())
	$tdatastatec_words[".isUseAjaxSuggest"] = false;
else 
	$tdatastatec_words[".isUseAjaxSuggest"] = true;

$tdatastatec_words[".rowHighlite"] = true;



$tdatastatec_words[".addPageEvents"] = false;

// use timepicker for search panel
$tdatastatec_words[".isUseTimeForSearch"] = false;



$tdatastatec_words[".useDetailsPreview"] = true;


$tdatastatec_words[".allSearchFields"] = array();
$tdatastatec_words[".filterFields"] = array();
$tdatastatec_words[".requiredSearchFields"] = array();

$tdatastatec_words[".allSearchFields"][] = "id";
	$tdatastatec_words[".allSearchFields"][] = "word";
	

$tdatastatec_words[".googleLikeFields"] = array();
$tdatastatec_words[".googleLikeFields"][] = "id";
$tdatastatec_words[".googleLikeFields"][] = "word";


$tdatastatec_words[".advSearchFields"] = array();
$tdatastatec_words[".advSearchFields"][] = "id";
$tdatastatec_words[".advSearchFields"][] = "word";

$tdatastatec_words[".tableType"] = "list";

$tdatastatec_words[".printerPageOrientation"] = 0;
$tdatastatec_words[".nPrinterPageScale"] = 100;

$tdatastatec_words[".nPrinterSplitRecords"] = 40;

$tdatastatec_words[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdatastatec_words[".pageSize"] = 20;

$tdatastatec_words[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatastatec_words[".strOrderBy"] = $tstrOrderBy;

$tdatastatec_words[".orderindexes"] = array();

$tdatastatec_words[".sqlHead"] = "SELECT id,  	word";
$tdatastatec_words[".sqlFrom"] = "FROM statec_words";
$tdatastatec_words[".sqlWhereExpr"] = "";
$tdatastatec_words[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatastatec_words[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatastatec_words[".arrGroupsPerPage"] = $arrGPP;

$tdatastatec_words[".highlightSearchResults"] = true;

$tableKeysstatec_words = array();
$tableKeysstatec_words[] = "id";
$tdatastatec_words[".Keys"] = $tableKeysstatec_words;

$tdatastatec_words[".listFields"] = array();
$tdatastatec_words[".listFields"][] = "id";
$tdatastatec_words[".listFields"][] = "word";

$tdatastatec_words[".hideMobileList"] = array();


$tdatastatec_words[".viewFields"] = array();
$tdatastatec_words[".viewFields"][] = "id";
$tdatastatec_words[".viewFields"][] = "word";

$tdatastatec_words[".addFields"] = array();

$tdatastatec_words[".inlineAddFields"] = array();

$tdatastatec_words[".editFields"] = array();

$tdatastatec_words[".inlineEditFields"] = array();

$tdatastatec_words[".exportFields"] = array();

$tdatastatec_words[".importFields"] = array();

$tdatastatec_words[".printFields"] = array();

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "statec_words";
	$fdata["Label"] = GetFieldLabel("statec_words","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatastatec_words["id"] = $fdata;
//	word
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "word";
	$fdata["GoodName"] = "word";
	$fdata["ownerTable"] = "statec_words";
	$fdata["Label"] = GetFieldLabel("statec_words","word"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "word"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "word";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 200;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatastatec_words["word"] = $fdata;

	
$tables_data["statec_words"]=&$tdatastatec_words;
$field_labels["statec_words"] = &$fieldLabelsstatec_words;
$fieldToolTips["statec_words"] = &$fieldToolTipsstatec_words;
$page_titles["statec_words"] = &$pageTitlesstatec_words;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["statec_words"] = array();
//	statec_words_translation
	
	

		$dIndex = 0;
	$detailsParam = array();
	$detailsParam["dDataSourceTable"]="statec_words_translation";
		$detailsParam["dOriginalTable"] = "statec_words_translation";
				$detailsParam["dType"]=PAGE_LIST;
	$detailsParam["dShortTable"] = "statec_words_translation";
	$detailsParam["dCaptionTable"] = GetTableCaption("statec_words_translation");
	$detailsParam["masterKeys"] =array();
	$detailsParam["detailKeys"] =array();
	$detailsParam["dispChildCount"] = "1";
		$detailsParam["hideChild"] = false;
	$detailsParam["previewOnList"] = 1;
	$detailsParam["previewOnAdd"] = 0;
	$detailsParam["previewOnEdit"] = 0;
	$detailsParam["previewOnView"] = 0;
			
	$detailsTablesData["statec_words"][$dIndex] = $detailsParam;
	
		
		$detailsTablesData["statec_words"][$dIndex]["masterKeys"] = array();

	$detailsTablesData["statec_words"][$dIndex]["masterKeys"][]="id";

				$detailsTablesData["statec_words"][$dIndex]["detailKeys"] = array();

	$detailsTablesData["statec_words"][$dIndex]["detailKeys"][]="word_id";
	
// tables which are master tables for current table (detail)
$masterTablesData["statec_words"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_statec_words()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	word";
$proto0["m_strFrom"] = "FROM statec_words";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "statec_words",
	"m_srcTableName" => "statec_words"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "statec_words";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "word",
	"m_strTable" => "statec_words",
	"m_srcTableName" => "statec_words"
));

$proto7["m_sql"] = "word";
$proto7["m_srcTableName"] = "statec_words";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto9=array();
$proto9["m_link"] = "SQLL_MAIN";
			$proto10=array();
$proto10["m_strName"] = "statec_words";
$proto10["m_srcTableName"] = "statec_words";
$proto10["m_columns"] = array();
$proto10["m_columns"][] = "id";
$proto10["m_columns"][] = "word";
$obj = new SQLTable($proto10);

$proto9["m_table"] = $obj;
$proto9["m_sql"] = "statec_words";
$proto9["m_alias"] = "";
$proto9["m_srcTableName"] = "statec_words";
$proto11=array();
$proto11["m_sql"] = "";
$proto11["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto11["m_column"]=$obj;
$proto11["m_contained"] = array();
$proto11["m_strCase"] = "";
$proto11["m_havingmode"] = false;
$proto11["m_inBrackets"] = false;
$proto11["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto11);

$proto9["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto9);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="statec_words";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_statec_words = createSqlQuery_statec_words();


	
		
	
$tdatastatec_words[".sqlquery"] = $queryData_statec_words;

$tableEvents["statec_words"] = new eventsBase;
$tdatastatec_words[".hasEvents"] = false;

?>