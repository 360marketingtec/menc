<?php
require_once(getabspath("classes/cipherer.php"));




$tdatanews = array();	
	$tdatanews[".truncateText"] = true;
	$tdatanews[".NumberOfChars"] = 80; 
	$tdatanews[".ShortName"] = "news";
	$tdatanews[".OwnerID"] = "";
	$tdatanews[".OriginalTable"] = "news";

//	field labels
$fieldLabelsnews = array();
$fieldToolTipsnews = array();
$pageTitlesnews = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsnews["English"] = array();
	$fieldToolTipsnews["English"] = array();
	$pageTitlesnews["English"] = array();
	$fieldLabelsnews["English"]["id"] = "Id";
	$fieldToolTipsnews["English"]["id"] = "";
	$fieldLabelsnews["English"]["title"] = "Title";
	$fieldToolTipsnews["English"]["title"] = "";
	$fieldLabelsnews["English"]["data"] = "Data";
	$fieldToolTipsnews["English"]["data"] = "";
	$fieldLabelsnews["English"]["photo"] = "Photo";
	$fieldToolTipsnews["English"]["photo"] = "";
	$fieldLabelsnews["English"]["meta_title"] = "Meta Title";
	$fieldToolTipsnews["English"]["meta_title"] = "";
	$fieldLabelsnews["English"]["meta_keywords"] = "Meta Keywords";
	$fieldToolTipsnews["English"]["meta_keywords"] = "";
	$fieldLabelsnews["English"]["meta_desc"] = "Meta Desc";
	$fieldToolTipsnews["English"]["meta_desc"] = "";
	$fieldLabelsnews["English"]["date"] = "Date";
	$fieldToolTipsnews["English"]["date"] = "";
	$fieldLabelsnews["English"]["view"] = "View";
	$fieldToolTipsnews["English"]["view"] = "";
	$fieldLabelsnews["English"]["lang_id"] = "Lang Id";
	$fieldToolTipsnews["English"]["lang_id"] = "";
	$fieldLabelsnews["English"]["hot_news"] = "Hot News";
	$fieldToolTipsnews["English"]["hot_news"] = "";
	$fieldLabelsnews["English"]["url"] = "Url";
	$fieldToolTipsnews["English"]["url"] = "";
	if (count($fieldToolTipsnews["English"]))
		$tdatanews[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelsnews["Arabic"] = array();
	$fieldToolTipsnews["Arabic"] = array();
	$pageTitlesnews["Arabic"] = array();
	$fieldLabelsnews["Arabic"]["id"] = "Id";
	$fieldToolTipsnews["Arabic"]["id"] = "";
	$fieldLabelsnews["Arabic"]["title"] = "Title";
	$fieldToolTipsnews["Arabic"]["title"] = "";
	$fieldLabelsnews["Arabic"]["data"] = "Data";
	$fieldToolTipsnews["Arabic"]["data"] = "";
	$fieldLabelsnews["Arabic"]["photo"] = "Photo";
	$fieldToolTipsnews["Arabic"]["photo"] = "";
	$fieldLabelsnews["Arabic"]["meta_title"] = "Meta Title";
	$fieldToolTipsnews["Arabic"]["meta_title"] = "";
	$fieldLabelsnews["Arabic"]["meta_keywords"] = "Meta Keywords";
	$fieldToolTipsnews["Arabic"]["meta_keywords"] = "";
	$fieldLabelsnews["Arabic"]["meta_desc"] = "Meta Desc";
	$fieldToolTipsnews["Arabic"]["meta_desc"] = "";
	$fieldLabelsnews["Arabic"]["date"] = "Date";
	$fieldToolTipsnews["Arabic"]["date"] = "";
	$fieldLabelsnews["Arabic"]["view"] = "View";
	$fieldToolTipsnews["Arabic"]["view"] = "";
	$fieldLabelsnews["Arabic"]["lang_id"] = "Lang Id";
	$fieldToolTipsnews["Arabic"]["lang_id"] = "";
	$fieldLabelsnews["Arabic"]["hot_news"] = "Hot News";
	$fieldToolTipsnews["Arabic"]["hot_news"] = "";
	$fieldLabelsnews["Arabic"]["url"] = "Url";
	$fieldToolTipsnews["Arabic"]["url"] = "";
	if (count($fieldToolTipsnews["Arabic"]))
		$tdatanews[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsnews[""] = array();
	$fieldToolTipsnews[""] = array();
	$pageTitlesnews[""] = array();
	$fieldLabelsnews[""]["id"] = "Id";
	$fieldToolTipsnews[""]["id"] = "";
	$fieldLabelsnews[""]["title"] = "Title";
	$fieldToolTipsnews[""]["title"] = "";
	$fieldLabelsnews[""]["data"] = "Data";
	$fieldToolTipsnews[""]["data"] = "";
	$fieldLabelsnews[""]["photo"] = "Photo";
	$fieldToolTipsnews[""]["photo"] = "";
	$fieldLabelsnews[""]["meta_title"] = "Meta Title";
	$fieldToolTipsnews[""]["meta_title"] = "";
	$fieldLabelsnews[""]["meta_keywords"] = "Meta Keywords";
	$fieldToolTipsnews[""]["meta_keywords"] = "";
	$fieldLabelsnews[""]["meta_desc"] = "Meta Desc";
	$fieldToolTipsnews[""]["meta_desc"] = "";
	$fieldLabelsnews[""]["date"] = "Date";
	$fieldToolTipsnews[""]["date"] = "";
	$fieldLabelsnews[""]["view"] = "View";
	$fieldToolTipsnews[""]["view"] = "";
	$fieldLabelsnews[""]["lang_id"] = "Lang Id";
	$fieldToolTipsnews[""]["lang_id"] = "";
	$fieldLabelsnews[""]["hot_news"] = "Hot News";
	$fieldToolTipsnews[""]["hot_news"] = "";
	$fieldLabelsnews[""]["url"] = "Url";
	$fieldToolTipsnews[""]["url"] = "";
	if (count($fieldToolTipsnews[""]))
		$tdatanews[".isUseToolTips"] = true;
}
	
	
	$tdatanews[".NCSearch"] = true;



$tdatanews[".shortTableName"] = "news";
$tdatanews[".nSecOptions"] = 0;
$tdatanews[".recsPerRowList"] = 1;
$tdatanews[".mainTableOwnerID"] = "";
$tdatanews[".moveNext"] = 1;
$tdatanews[".nType"] = 0;

$tdatanews[".strOriginalTableName"] = "news";




$tdatanews[".showAddInPopup"] = false;

$tdatanews[".showEditInPopup"] = false;

$tdatanews[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatanews[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatanews[".fieldsForRegister"] = array();

$tdatanews[".listAjax"] = false;

	$tdatanews[".audit"] = false;

	$tdatanews[".locking"] = false;

$tdatanews[".edit"] = true;

$tdatanews[".list"] = true;

$tdatanews[".view"] = true;

$tdatanews[".import"] = true;

$tdatanews[".exportTo"] = true;

$tdatanews[".printFriendly"] = true;

$tdatanews[".delete"] = true;

$tdatanews[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatanews[".searchSaving"] = false;
//

$tdatanews[".showSearchPanel"] = true;
		$tdatanews[".flexibleSearch"] = true;		

if (isMobile())
	$tdatanews[".isUseAjaxSuggest"] = false;
else 
	$tdatanews[".isUseAjaxSuggest"] = true;

$tdatanews[".rowHighlite"] = true;



$tdatanews[".addPageEvents"] = false;

// use timepicker for search panel
$tdatanews[".isUseTimeForSearch"] = false;





$tdatanews[".allSearchFields"] = array();
$tdatanews[".filterFields"] = array();
$tdatanews[".requiredSearchFields"] = array();

$tdatanews[".allSearchFields"][] = "id";
	$tdatanews[".allSearchFields"][] = "lang_id";
	$tdatanews[".allSearchFields"][] = "title";
	$tdatanews[".allSearchFields"][] = "url";
	$tdatanews[".allSearchFields"][] = "data";
	$tdatanews[".allSearchFields"][] = "photo";
	$tdatanews[".allSearchFields"][] = "view";
	$tdatanews[".allSearchFields"][] = "hot_news";
	$tdatanews[".allSearchFields"][] = "date";
	$tdatanews[".allSearchFields"][] = "meta_title";
	$tdatanews[".allSearchFields"][] = "meta_keywords";
	$tdatanews[".allSearchFields"][] = "meta_desc";
	

$tdatanews[".googleLikeFields"] = array();
$tdatanews[".googleLikeFields"][] = "id";
$tdatanews[".googleLikeFields"][] = "title";
$tdatanews[".googleLikeFields"][] = "data";
$tdatanews[".googleLikeFields"][] = "photo";
$tdatanews[".googleLikeFields"][] = "meta_title";
$tdatanews[".googleLikeFields"][] = "meta_keywords";
$tdatanews[".googleLikeFields"][] = "meta_desc";
$tdatanews[".googleLikeFields"][] = "date";
$tdatanews[".googleLikeFields"][] = "view";
$tdatanews[".googleLikeFields"][] = "lang_id";
$tdatanews[".googleLikeFields"][] = "hot_news";
$tdatanews[".googleLikeFields"][] = "url";


$tdatanews[".advSearchFields"] = array();
$tdatanews[".advSearchFields"][] = "id";
$tdatanews[".advSearchFields"][] = "lang_id";
$tdatanews[".advSearchFields"][] = "title";
$tdatanews[".advSearchFields"][] = "url";
$tdatanews[".advSearchFields"][] = "data";
$tdatanews[".advSearchFields"][] = "photo";
$tdatanews[".advSearchFields"][] = "view";
$tdatanews[".advSearchFields"][] = "hot_news";
$tdatanews[".advSearchFields"][] = "date";
$tdatanews[".advSearchFields"][] = "meta_title";
$tdatanews[".advSearchFields"][] = "meta_keywords";
$tdatanews[".advSearchFields"][] = "meta_desc";

$tdatanews[".tableType"] = "list";

$tdatanews[".printerPageOrientation"] = 0;
$tdatanews[".nPrinterPageScale"] = 100;

$tdatanews[".nPrinterSplitRecords"] = 40;

$tdatanews[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdatanews[".pageSize"] = 20;

$tdatanews[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatanews[".strOrderBy"] = $tstrOrderBy;

$tdatanews[".orderindexes"] = array();

$tdatanews[".sqlHead"] = "SELECT id,  	title,  	`data`,  	photo,  	meta_title,  	meta_keywords,  	meta_desc,  	`date`,  	`view`,  	lang_id,  	hot_news,  	url";
$tdatanews[".sqlFrom"] = "FROM news";
$tdatanews[".sqlWhereExpr"] = "";
$tdatanews[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatanews[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatanews[".arrGroupsPerPage"] = $arrGPP;

$tdatanews[".highlightSearchResults"] = true;

$tableKeysnews = array();
$tableKeysnews[] = "id";
$tdatanews[".Keys"] = $tableKeysnews;

$tdatanews[".listFields"] = array();
$tdatanews[".listFields"][] = "id";
$tdatanews[".listFields"][] = "lang_id";
$tdatanews[".listFields"][] = "title";
$tdatanews[".listFields"][] = "view";
$tdatanews[".listFields"][] = "date";

$tdatanews[".hideMobileList"] = array();


$tdatanews[".viewFields"] = array();
$tdatanews[".viewFields"][] = "id";
$tdatanews[".viewFields"][] = "lang_id";
$tdatanews[".viewFields"][] = "title";
$tdatanews[".viewFields"][] = "url";
$tdatanews[".viewFields"][] = "data";
$tdatanews[".viewFields"][] = "photo";
$tdatanews[".viewFields"][] = "view";
$tdatanews[".viewFields"][] = "hot_news";
$tdatanews[".viewFields"][] = "date";
$tdatanews[".viewFields"][] = "meta_title";
$tdatanews[".viewFields"][] = "meta_keywords";
$tdatanews[".viewFields"][] = "meta_desc";

$tdatanews[".addFields"] = array();
$tdatanews[".addFields"][] = "lang_id";
$tdatanews[".addFields"][] = "title";
$tdatanews[".addFields"][] = "url";
$tdatanews[".addFields"][] = "data";
$tdatanews[".addFields"][] = "photo";
$tdatanews[".addFields"][] = "view";
$tdatanews[".addFields"][] = "hot_news";
$tdatanews[".addFields"][] = "date";
$tdatanews[".addFields"][] = "meta_title";
$tdatanews[".addFields"][] = "meta_keywords";
$tdatanews[".addFields"][] = "meta_desc";

$tdatanews[".inlineAddFields"] = array();

$tdatanews[".editFields"] = array();
$tdatanews[".editFields"][] = "lang_id";
$tdatanews[".editFields"][] = "title";
$tdatanews[".editFields"][] = "url";
$tdatanews[".editFields"][] = "data";
$tdatanews[".editFields"][] = "photo";
$tdatanews[".editFields"][] = "view";
$tdatanews[".editFields"][] = "hot_news";
$tdatanews[".editFields"][] = "date";
$tdatanews[".editFields"][] = "meta_title";
$tdatanews[".editFields"][] = "meta_keywords";
$tdatanews[".editFields"][] = "meta_desc";

$tdatanews[".inlineEditFields"] = array();

$tdatanews[".exportFields"] = array();
$tdatanews[".exportFields"][] = "id";
$tdatanews[".exportFields"][] = "lang_id";
$tdatanews[".exportFields"][] = "title";
$tdatanews[".exportFields"][] = "url";
$tdatanews[".exportFields"][] = "data";
$tdatanews[".exportFields"][] = "photo";
$tdatanews[".exportFields"][] = "view";
$tdatanews[".exportFields"][] = "hot_news";
$tdatanews[".exportFields"][] = "date";
$tdatanews[".exportFields"][] = "meta_title";
$tdatanews[".exportFields"][] = "meta_keywords";
$tdatanews[".exportFields"][] = "meta_desc";

$tdatanews[".importFields"] = array();
$tdatanews[".importFields"][] = "id";
$tdatanews[".importFields"][] = "title";
$tdatanews[".importFields"][] = "data";
$tdatanews[".importFields"][] = "photo";
$tdatanews[".importFields"][] = "meta_title";
$tdatanews[".importFields"][] = "meta_keywords";
$tdatanews[".importFields"][] = "meta_desc";
$tdatanews[".importFields"][] = "date";
$tdatanews[".importFields"][] = "view";
$tdatanews[".importFields"][] = "lang_id";
$tdatanews[".importFields"][] = "hot_news";
$tdatanews[".importFields"][] = "url";

$tdatanews[".printFields"] = array();
$tdatanews[".printFields"][] = "id";
$tdatanews[".printFields"][] = "lang_id";
$tdatanews[".printFields"][] = "title";
$tdatanews[".printFields"][] = "url";
$tdatanews[".printFields"][] = "data";
$tdatanews[".printFields"][] = "photo";
$tdatanews[".printFields"][] = "view";
$tdatanews[".printFields"][] = "hot_news";
$tdatanews[".printFields"][] = "date";
$tdatanews[".printFields"][] = "meta_title";
$tdatanews[".printFields"][] = "meta_keywords";
$tdatanews[".printFields"][] = "meta_desc";

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatanews["id"] = $fdata;
//	title
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "title";
	$fdata["GoodName"] = "title";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","title"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "title"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "title";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatanews["title"] = $fdata;
//	data
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "data";
	$fdata["GoodName"] = "data";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","data"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "data"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`data`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "HTML");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		$edata["UseRTE"] = true; 
	
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatanews["data"] = $fdata;
//	photo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "photo";
	$fdata["GoodName"] = "photo";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","photo"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "photo"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "photo";
	
		$fdata["DeleteAssociatedFile"] = true;
	
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "../uploads/";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Document Download");
	
		
		
		
								$vdata["ShowIcon"] = true; 
			
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Document upload");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
							$edata["acceptFileTypes"] = "bmp";
						$edata["acceptFileTypes"] .= "|gif";
						$edata["acceptFileTypes"] .= "|jpg";
						$edata["acceptFileTypes"] .= "|png";
		$edata["acceptFileTypes"] = "(".$edata["acceptFileTypes"].")$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatanews["photo"] = $fdata;
//	meta_title
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "meta_title";
	$fdata["GoodName"] = "meta_title";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","meta_title"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_title"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_title";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatanews["meta_title"] = $fdata;
//	meta_keywords
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "meta_keywords";
	$fdata["GoodName"] = "meta_keywords";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","meta_keywords"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_keywords"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_keywords";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatanews["meta_keywords"] = $fdata;
//	meta_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "meta_desc";
	$fdata["GoodName"] = "meta_desc";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","meta_desc"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_desc"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_desc";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatanews["meta_desc"] = $fdata;
//	date
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "date";
	$fdata["GoodName"] = "date";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","date"); 
	$fdata["FieldType"] = 135;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "date"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`date`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Short Date");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Date");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		$edata["DateEditType"] = 13; 
	$edata["InitialYearFactor"] = 100; 
	$edata["LastYearFactor"] = 10; 
	
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatanews["date"] = $fdata;
//	view
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "view";
	$fdata["GoodName"] = "view";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","view"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "view"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`view`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Checkbox");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Checkbox");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatanews["view"] = $fdata;
//	lang_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "lang_id";
	$fdata["GoodName"] = "lang_id";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","lang_id"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "lang_id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lang_id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Lookup wizard");
	
			
	
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "langyages";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
		
		
			
	$edata["LinkField"] = "id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "name";
	
		
	$edata["LookupOrderBy"] = "";
	
		
			
		
				
	
	
		
		$edata["SelectSize"] = 1;
		
// End Lookup Settings


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatanews["lang_id"] = $fdata;
//	hot_news
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "hot_news";
	$fdata["GoodName"] = "hot_news";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","hot_news"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "hot_news"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "hot_news";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Checkbox");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Checkbox");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatanews["hot_news"] = $fdata;
//	url
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "url";
	$fdata["GoodName"] = "url";
	$fdata["ownerTable"] = "news";
	$fdata["Label"] = GetFieldLabel("news","url"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "url"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "url";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatanews["url"] = $fdata;

	
$tables_data["news"]=&$tdatanews;
$field_labels["news"] = &$fieldLabelsnews;
$fieldToolTips["news"] = &$fieldToolTipsnews;
$page_titles["news"] = &$pageTitlesnews;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["news"] = array();
	
// tables which are master tables for current table (detail)
$masterTablesData["news"] = array();


	
				$strOriginalDetailsTable="langyages";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="langyages";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "langyages";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispInfo"]= "1";
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["news"][0] = $masterParams;	
				$masterTablesData["news"][0]["masterKeys"] = array();
	$masterTablesData["news"][0]["masterKeys"][]="id";
				$masterTablesData["news"][0]["detailKeys"] = array();
	$masterTablesData["news"][0]["detailKeys"][]="lang_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_news()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	title,  	`data`,  	photo,  	meta_title,  	meta_keywords,  	meta_desc,  	`date`,  	`view`,  	lang_id,  	hot_news,  	url";
$proto0["m_strFrom"] = "FROM news";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "news";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "title",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto7["m_sql"] = "title";
$proto7["m_srcTableName"] = "news";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto9["m_sql"] = "`data`";
$proto9["m_srcTableName"] = "news";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "photo",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto11["m_sql"] = "photo";
$proto11["m_srcTableName"] = "news";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_title",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto13["m_sql"] = "meta_title";
$proto13["m_srcTableName"] = "news";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_keywords",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto15["m_sql"] = "meta_keywords";
$proto15["m_srcTableName"] = "news";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_desc",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto17["m_sql"] = "meta_desc";
$proto17["m_srcTableName"] = "news";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "date",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto19["m_sql"] = "`date`";
$proto19["m_srcTableName"] = "news";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "view",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto21["m_sql"] = "`view`";
$proto21["m_srcTableName"] = "news";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto0["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "lang_id",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto23["m_sql"] = "lang_id";
$proto23["m_srcTableName"] = "news";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto0["m_fieldlist"][]=$obj;
						$proto25=array();
			$obj = new SQLField(array(
	"m_strName" => "hot_news",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto25["m_sql"] = "hot_news";
$proto25["m_srcTableName"] = "news";
$proto25["m_expr"]=$obj;
$proto25["m_alias"] = "";
$obj = new SQLFieldListItem($proto25);

$proto0["m_fieldlist"][]=$obj;
						$proto27=array();
			$obj = new SQLField(array(
	"m_strName" => "url",
	"m_strTable" => "news",
	"m_srcTableName" => "news"
));

$proto27["m_sql"] = "url";
$proto27["m_srcTableName"] = "news";
$proto27["m_expr"]=$obj;
$proto27["m_alias"] = "";
$obj = new SQLFieldListItem($proto27);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto29=array();
$proto29["m_link"] = "SQLL_MAIN";
			$proto30=array();
$proto30["m_strName"] = "news";
$proto30["m_srcTableName"] = "news";
$proto30["m_columns"] = array();
$proto30["m_columns"][] = "id";
$proto30["m_columns"][] = "title";
$proto30["m_columns"][] = "data";
$proto30["m_columns"][] = "photo";
$proto30["m_columns"][] = "meta_title";
$proto30["m_columns"][] = "meta_keywords";
$proto30["m_columns"][] = "meta_desc";
$proto30["m_columns"][] = "date";
$proto30["m_columns"][] = "view";
$proto30["m_columns"][] = "lang_id";
$proto30["m_columns"][] = "hot_news";
$proto30["m_columns"][] = "url";
$obj = new SQLTable($proto30);

$proto29["m_table"] = $obj;
$proto29["m_sql"] = "news";
$proto29["m_alias"] = "";
$proto29["m_srcTableName"] = "news";
$proto31=array();
$proto31["m_sql"] = "";
$proto31["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto31["m_column"]=$obj;
$proto31["m_contained"] = array();
$proto31["m_strCase"] = "";
$proto31["m_havingmode"] = false;
$proto31["m_inBrackets"] = false;
$proto31["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto31);

$proto29["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto29);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="news";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_news = createSqlQuery_news();


	
												
	
$tdatanews[".sqlquery"] = $queryData_news;

$tableEvents["news"] = new eventsBase;
$tdatanews[".hasEvents"] = false;

?>