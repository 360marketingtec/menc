<?php
require_once(getabspath("classes/cipherer.php"));




$tdatastatec_words_translation = array();	
	$tdatastatec_words_translation[".truncateText"] = true;
	$tdatastatec_words_translation[".NumberOfChars"] = 80; 
	$tdatastatec_words_translation[".ShortName"] = "statec_words_translation";
	$tdatastatec_words_translation[".OwnerID"] = "";
	$tdatastatec_words_translation[".OriginalTable"] = "statec_words_translation";

//	field labels
$fieldLabelsstatec_words_translation = array();
$fieldToolTipsstatec_words_translation = array();
$pageTitlesstatec_words_translation = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsstatec_words_translation["English"] = array();
	$fieldToolTipsstatec_words_translation["English"] = array();
	$pageTitlesstatec_words_translation["English"] = array();
	$fieldLabelsstatec_words_translation["English"]["id"] = "Id";
	$fieldToolTipsstatec_words_translation["English"]["id"] = "";
	$fieldLabelsstatec_words_translation["English"]["lang_id"] = "Lang Id";
	$fieldToolTipsstatec_words_translation["English"]["lang_id"] = "";
	$fieldLabelsstatec_words_translation["English"]["word_id"] = "Word Id";
	$fieldToolTipsstatec_words_translation["English"]["word_id"] = "";
	$fieldLabelsstatec_words_translation["English"]["translation"] = "Translation";
	$fieldToolTipsstatec_words_translation["English"]["translation"] = "";
	if (count($fieldToolTipsstatec_words_translation["English"]))
		$tdatastatec_words_translation[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelsstatec_words_translation["Arabic"] = array();
	$fieldToolTipsstatec_words_translation["Arabic"] = array();
	$pageTitlesstatec_words_translation["Arabic"] = array();
	$fieldLabelsstatec_words_translation["Arabic"]["id"] = "Id";
	$fieldToolTipsstatec_words_translation["Arabic"]["id"] = "";
	$fieldLabelsstatec_words_translation["Arabic"]["lang_id"] = "Lang Id";
	$fieldToolTipsstatec_words_translation["Arabic"]["lang_id"] = "";
	$fieldLabelsstatec_words_translation["Arabic"]["word_id"] = "Word Id";
	$fieldToolTipsstatec_words_translation["Arabic"]["word_id"] = "";
	$fieldLabelsstatec_words_translation["Arabic"]["translation"] = "Translation";
	$fieldToolTipsstatec_words_translation["Arabic"]["translation"] = "";
	if (count($fieldToolTipsstatec_words_translation["Arabic"]))
		$tdatastatec_words_translation[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsstatec_words_translation[""] = array();
	$fieldToolTipsstatec_words_translation[""] = array();
	$pageTitlesstatec_words_translation[""] = array();
	$fieldLabelsstatec_words_translation[""]["id"] = "Id";
	$fieldToolTipsstatec_words_translation[""]["id"] = "";
	if (count($fieldToolTipsstatec_words_translation[""]))
		$tdatastatec_words_translation[".isUseToolTips"] = true;
}
	
	
	$tdatastatec_words_translation[".NCSearch"] = true;



$tdatastatec_words_translation[".shortTableName"] = "statec_words_translation";
$tdatastatec_words_translation[".nSecOptions"] = 0;
$tdatastatec_words_translation[".recsPerRowList"] = 1;
$tdatastatec_words_translation[".mainTableOwnerID"] = "";
$tdatastatec_words_translation[".moveNext"] = 1;
$tdatastatec_words_translation[".nType"] = 0;

$tdatastatec_words_translation[".strOriginalTableName"] = "statec_words_translation";




$tdatastatec_words_translation[".showAddInPopup"] = false;

$tdatastatec_words_translation[".showEditInPopup"] = false;

$tdatastatec_words_translation[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatastatec_words_translation[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatastatec_words_translation[".fieldsForRegister"] = array();

$tdatastatec_words_translation[".listAjax"] = false;

	$tdatastatec_words_translation[".audit"] = true;

	$tdatastatec_words_translation[".locking"] = true;

$tdatastatec_words_translation[".edit"] = true;

$tdatastatec_words_translation[".list"] = true;

$tdatastatec_words_translation[".inlineEdit"] = true;
$tdatastatec_words_translation[".inlineAdd"] = true;
$tdatastatec_words_translation[".view"] = true;




$tdatastatec_words_translation[".delete"] = true;

$tdatastatec_words_translation[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatastatec_words_translation[".searchSaving"] = false;
//

$tdatastatec_words_translation[".showSearchPanel"] = true;
		$tdatastatec_words_translation[".flexibleSearch"] = true;		

if (isMobile())
	$tdatastatec_words_translation[".isUseAjaxSuggest"] = false;
else 
	$tdatastatec_words_translation[".isUseAjaxSuggest"] = true;

$tdatastatec_words_translation[".rowHighlite"] = true;



$tdatastatec_words_translation[".addPageEvents"] = false;

// use timepicker for search panel
$tdatastatec_words_translation[".isUseTimeForSearch"] = false;





$tdatastatec_words_translation[".allSearchFields"] = array();
$tdatastatec_words_translation[".filterFields"] = array();
$tdatastatec_words_translation[".requiredSearchFields"] = array();

$tdatastatec_words_translation[".allSearchFields"][] = "id";
	$tdatastatec_words_translation[".allSearchFields"][] = "lang_id";
	$tdatastatec_words_translation[".allSearchFields"][] = "word_id";
	$tdatastatec_words_translation[".allSearchFields"][] = "translation";
	

$tdatastatec_words_translation[".googleLikeFields"] = array();
$tdatastatec_words_translation[".googleLikeFields"][] = "id";
$tdatastatec_words_translation[".googleLikeFields"][] = "lang_id";
$tdatastatec_words_translation[".googleLikeFields"][] = "word_id";
$tdatastatec_words_translation[".googleLikeFields"][] = "translation";


$tdatastatec_words_translation[".advSearchFields"] = array();
$tdatastatec_words_translation[".advSearchFields"][] = "id";
$tdatastatec_words_translation[".advSearchFields"][] = "lang_id";
$tdatastatec_words_translation[".advSearchFields"][] = "word_id";
$tdatastatec_words_translation[".advSearchFields"][] = "translation";

$tdatastatec_words_translation[".tableType"] = "list";

$tdatastatec_words_translation[".printerPageOrientation"] = 0;
$tdatastatec_words_translation[".nPrinterPageScale"] = 100;

$tdatastatec_words_translation[".nPrinterSplitRecords"] = 40;

$tdatastatec_words_translation[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdatastatec_words_translation[".pageSize"] = 20;

$tdatastatec_words_translation[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatastatec_words_translation[".strOrderBy"] = $tstrOrderBy;

$tdatastatec_words_translation[".orderindexes"] = array();

$tdatastatec_words_translation[".sqlHead"] = "SELECT id,  	lang_id,  	word_id,  	`translation`";
$tdatastatec_words_translation[".sqlFrom"] = "FROM statec_words_translation";
$tdatastatec_words_translation[".sqlWhereExpr"] = "";
$tdatastatec_words_translation[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatastatec_words_translation[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatastatec_words_translation[".arrGroupsPerPage"] = $arrGPP;

$tdatastatec_words_translation[".highlightSearchResults"] = true;

$tableKeysstatec_words_translation = array();
$tableKeysstatec_words_translation[] = "id";
$tdatastatec_words_translation[".Keys"] = $tableKeysstatec_words_translation;

$tdatastatec_words_translation[".listFields"] = array();
$tdatastatec_words_translation[".listFields"][] = "id";
$tdatastatec_words_translation[".listFields"][] = "lang_id";
$tdatastatec_words_translation[".listFields"][] = "word_id";
$tdatastatec_words_translation[".listFields"][] = "translation";

$tdatastatec_words_translation[".hideMobileList"] = array();


$tdatastatec_words_translation[".viewFields"] = array();
$tdatastatec_words_translation[".viewFields"][] = "id";
$tdatastatec_words_translation[".viewFields"][] = "lang_id";
$tdatastatec_words_translation[".viewFields"][] = "word_id";
$tdatastatec_words_translation[".viewFields"][] = "translation";

$tdatastatec_words_translation[".addFields"] = array();
$tdatastatec_words_translation[".addFields"][] = "lang_id";
$tdatastatec_words_translation[".addFields"][] = "word_id";
$tdatastatec_words_translation[".addFields"][] = "translation";

$tdatastatec_words_translation[".inlineAddFields"] = array();
$tdatastatec_words_translation[".inlineAddFields"][] = "lang_id";
$tdatastatec_words_translation[".inlineAddFields"][] = "word_id";
$tdatastatec_words_translation[".inlineAddFields"][] = "translation";

$tdatastatec_words_translation[".editFields"] = array();
$tdatastatec_words_translation[".editFields"][] = "lang_id";
$tdatastatec_words_translation[".editFields"][] = "word_id";
$tdatastatec_words_translation[".editFields"][] = "translation";

$tdatastatec_words_translation[".inlineEditFields"] = array();
$tdatastatec_words_translation[".inlineEditFields"][] = "lang_id";
$tdatastatec_words_translation[".inlineEditFields"][] = "word_id";
$tdatastatec_words_translation[".inlineEditFields"][] = "translation";

$tdatastatec_words_translation[".exportFields"] = array();

$tdatastatec_words_translation[".importFields"] = array();

$tdatastatec_words_translation[".printFields"] = array();

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "statec_words_translation";
	$fdata["Label"] = GetFieldLabel("statec_words_translation","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatastatec_words_translation["id"] = $fdata;
//	lang_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "lang_id";
	$fdata["GoodName"] = "lang_id";
	$fdata["ownerTable"] = "statec_words_translation";
	$fdata["Label"] = GetFieldLabel("statec_words_translation","lang_id"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		$fdata["bInlineAdd"] = true; 
	
		$fdata["bEditPage"] = true; 
	
		$fdata["bInlineEdit"] = true; 
	
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "lang_id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lang_id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Lookup wizard");
	
			
	
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "langyages";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
		
		
			
	$edata["LinkField"] = "id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "name";
	
		
	$edata["LookupOrderBy"] = "";
	
		
			
		
				
	
	
		
		$edata["SelectSize"] = 1;
		
// End Lookup Settings


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatastatec_words_translation["lang_id"] = $fdata;
//	word_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "word_id";
	$fdata["GoodName"] = "word_id";
	$fdata["ownerTable"] = "statec_words_translation";
	$fdata["Label"] = GetFieldLabel("statec_words_translation","word_id"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		$fdata["bInlineAdd"] = true; 
	
		$fdata["bEditPage"] = true; 
	
		$fdata["bInlineEdit"] = true; 
	
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "word_id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "word_id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Lookup wizard");
	
			
	
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "statec_words";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
		
		
			
	$edata["LinkField"] = "id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "word";
	
		
	$edata["LookupOrderBy"] = "";
	
		
			
		
				
	
	
		
		$edata["SelectSize"] = 1;
		
// End Lookup Settings


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatastatec_words_translation["word_id"] = $fdata;
//	translation
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "translation";
	$fdata["GoodName"] = "translation";
	$fdata["ownerTable"] = "statec_words_translation";
	$fdata["Label"] = GetFieldLabel("statec_words_translation","translation"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		$fdata["bInlineAdd"] = true; 
	
		$fdata["bEditPage"] = true; 
	
		$fdata["bInlineEdit"] = true; 
	
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		
		
		$fdata["strField"] = "translation"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`translation`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatastatec_words_translation["translation"] = $fdata;

	
$tables_data["statec_words_translation"]=&$tdatastatec_words_translation;
$field_labels["statec_words_translation"] = &$fieldLabelsstatec_words_translation;
$fieldToolTips["statec_words_translation"] = &$fieldToolTipsstatec_words_translation;
$page_titles["statec_words_translation"] = &$pageTitlesstatec_words_translation;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["statec_words_translation"] = array();
	
// tables which are master tables for current table (detail)
$masterTablesData["statec_words_translation"] = array();


	
				$strOriginalDetailsTable="statec_words";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="statec_words";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "statec_words";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispInfo"]= "1";
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["statec_words_translation"][0] = $masterParams;	
				$masterTablesData["statec_words_translation"][0]["masterKeys"] = array();
	$masterTablesData["statec_words_translation"][0]["masterKeys"][]="id";
				$masterTablesData["statec_words_translation"][0]["detailKeys"] = array();
	$masterTablesData["statec_words_translation"][0]["detailKeys"][]="word_id";
		
	
				$strOriginalDetailsTable="langyages";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="langyages";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "langyages";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispInfo"]= "1";
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["statec_words_translation"][1] = $masterParams;	
				$masterTablesData["statec_words_translation"][1]["masterKeys"] = array();
	$masterTablesData["statec_words_translation"][1]["masterKeys"][]="id";
				$masterTablesData["statec_words_translation"][1]["detailKeys"] = array();
	$masterTablesData["statec_words_translation"][1]["detailKeys"][]="lang_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_statec_words_translation()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	lang_id,  	word_id,  	`translation`";
$proto0["m_strFrom"] = "FROM statec_words_translation";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "statec_words_translation",
	"m_srcTableName" => "statec_words_translation"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "statec_words_translation";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "lang_id",
	"m_strTable" => "statec_words_translation",
	"m_srcTableName" => "statec_words_translation"
));

$proto7["m_sql"] = "lang_id";
$proto7["m_srcTableName"] = "statec_words_translation";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "word_id",
	"m_strTable" => "statec_words_translation",
	"m_srcTableName" => "statec_words_translation"
));

$proto9["m_sql"] = "word_id";
$proto9["m_srcTableName"] = "statec_words_translation";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "translation",
	"m_strTable" => "statec_words_translation",
	"m_srcTableName" => "statec_words_translation"
));

$proto11["m_sql"] = "`translation`";
$proto11["m_srcTableName"] = "statec_words_translation";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto13=array();
$proto13["m_link"] = "SQLL_MAIN";
			$proto14=array();
$proto14["m_strName"] = "statec_words_translation";
$proto14["m_srcTableName"] = "statec_words_translation";
$proto14["m_columns"] = array();
$proto14["m_columns"][] = "id";
$proto14["m_columns"][] = "lang_id";
$proto14["m_columns"][] = "word_id";
$proto14["m_columns"][] = "translation";
$obj = new SQLTable($proto14);

$proto13["m_table"] = $obj;
$proto13["m_sql"] = "statec_words_translation";
$proto13["m_alias"] = "";
$proto13["m_srcTableName"] = "statec_words_translation";
$proto15=array();
$proto15["m_sql"] = "";
$proto15["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto15["m_column"]=$obj;
$proto15["m_contained"] = array();
$proto15["m_strCase"] = "";
$proto15["m_havingmode"] = false;
$proto15["m_inBrackets"] = false;
$proto15["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto15);

$proto13["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto13);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="statec_words_translation";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_statec_words_translation = createSqlQuery_statec_words_translation();


	
				
	
$tdatastatec_words_translation[".sqlquery"] = $queryData_statec_words_translation;

$tableEvents["statec_words_translation"] = new eventsBase;
$tdatastatec_words_translation[".hasEvents"] = false;

?>