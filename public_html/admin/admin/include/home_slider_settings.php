<?php
require_once(getabspath("classes/cipherer.php"));




$tdatahome_slider = array();	
	$tdatahome_slider[".truncateText"] = true;
	$tdatahome_slider[".NumberOfChars"] = 80; 
	$tdatahome_slider[".ShortName"] = "home_slider";
	$tdatahome_slider[".OwnerID"] = "";
	$tdatahome_slider[".OriginalTable"] = "home_slider";

//	field labels
$fieldLabelshome_slider = array();
$fieldToolTipshome_slider = array();
$pageTitleshome_slider = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelshome_slider["English"] = array();
	$fieldToolTipshome_slider["English"] = array();
	$pageTitleshome_slider["English"] = array();
	$fieldLabelshome_slider["English"]["id"] = "Id";
	$fieldToolTipshome_slider["English"]["id"] = "";
	$fieldLabelshome_slider["English"]["title"] = "Title";
	$fieldToolTipshome_slider["English"]["title"] = "";
	$fieldLabelshome_slider["English"]["desc"] = "Desc";
	$fieldToolTipshome_slider["English"]["desc"] = "";
	$fieldLabelshome_slider["English"]["link"] = "Link";
	$fieldToolTipshome_slider["English"]["link"] = "";
	$fieldLabelshome_slider["English"]["photo"] = "Photo";
	$fieldToolTipshome_slider["English"]["photo"] = "";
	$fieldLabelshome_slider["English"]["view"] = "View";
	$fieldToolTipshome_slider["English"]["view"] = "";
	$fieldLabelshome_slider["English"]["lang_id"] = "Lang Id";
	$fieldToolTipshome_slider["English"]["lang_id"] = "";
	$fieldLabelshome_slider["English"]["order"] = "Order";
	$fieldToolTipshome_slider["English"]["order"] = "";
	if (count($fieldToolTipshome_slider["English"]))
		$tdatahome_slider[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelshome_slider["Arabic"] = array();
	$fieldToolTipshome_slider["Arabic"] = array();
	$pageTitleshome_slider["Arabic"] = array();
	$fieldLabelshome_slider["Arabic"]["id"] = "Id";
	$fieldToolTipshome_slider["Arabic"]["id"] = "";
	$fieldLabelshome_slider["Arabic"]["title"] = "Title";
	$fieldToolTipshome_slider["Arabic"]["title"] = "";
	$fieldLabelshome_slider["Arabic"]["desc"] = "Desc";
	$fieldToolTipshome_slider["Arabic"]["desc"] = "";
	$fieldLabelshome_slider["Arabic"]["link"] = "Link";
	$fieldToolTipshome_slider["Arabic"]["link"] = "";
	$fieldLabelshome_slider["Arabic"]["photo"] = "Photo";
	$fieldToolTipshome_slider["Arabic"]["photo"] = "";
	$fieldLabelshome_slider["Arabic"]["view"] = "View";
	$fieldToolTipshome_slider["Arabic"]["view"] = "";
	$fieldLabelshome_slider["Arabic"]["lang_id"] = "Lang Id";
	$fieldToolTipshome_slider["Arabic"]["lang_id"] = "";
	$fieldLabelshome_slider["Arabic"]["order"] = "Order";
	$fieldToolTipshome_slider["Arabic"]["order"] = "";
	if (count($fieldToolTipshome_slider["Arabic"]))
		$tdatahome_slider[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelshome_slider[""] = array();
	$fieldToolTipshome_slider[""] = array();
	$pageTitleshome_slider[""] = array();
	if (count($fieldToolTipshome_slider[""]))
		$tdatahome_slider[".isUseToolTips"] = true;
}
	
	
	$tdatahome_slider[".NCSearch"] = true;



$tdatahome_slider[".shortTableName"] = "home_slider";
$tdatahome_slider[".nSecOptions"] = 0;
$tdatahome_slider[".recsPerRowList"] = 1;
$tdatahome_slider[".mainTableOwnerID"] = "";
$tdatahome_slider[".moveNext"] = 1;
$tdatahome_slider[".nType"] = 0;

$tdatahome_slider[".strOriginalTableName"] = "home_slider";




$tdatahome_slider[".showAddInPopup"] = false;

$tdatahome_slider[".showEditInPopup"] = false;

$tdatahome_slider[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatahome_slider[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatahome_slider[".fieldsForRegister"] = array();

$tdatahome_slider[".listAjax"] = false;

	$tdatahome_slider[".audit"] = true;

	$tdatahome_slider[".locking"] = true;

$tdatahome_slider[".edit"] = true;

$tdatahome_slider[".list"] = true;

$tdatahome_slider[".view"] = true;


$tdatahome_slider[".exportTo"] = true;

$tdatahome_slider[".printFriendly"] = true;

$tdatahome_slider[".delete"] = true;

$tdatahome_slider[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatahome_slider[".searchSaving"] = false;
//

$tdatahome_slider[".showSearchPanel"] = true;
		$tdatahome_slider[".flexibleSearch"] = true;		

if (isMobile())
	$tdatahome_slider[".isUseAjaxSuggest"] = false;
else 
	$tdatahome_slider[".isUseAjaxSuggest"] = true;

$tdatahome_slider[".rowHighlite"] = true;



$tdatahome_slider[".addPageEvents"] = false;

// use timepicker for search panel
$tdatahome_slider[".isUseTimeForSearch"] = false;





$tdatahome_slider[".allSearchFields"] = array();
$tdatahome_slider[".filterFields"] = array();
$tdatahome_slider[".requiredSearchFields"] = array();

$tdatahome_slider[".allSearchFields"][] = "id";
	$tdatahome_slider[".allSearchFields"][] = "lang_id";
	$tdatahome_slider[".allSearchFields"][] = "view";
	$tdatahome_slider[".allSearchFields"][] = "order";
	$tdatahome_slider[".allSearchFields"][] = "title";
	$tdatahome_slider[".allSearchFields"][] = "desc";
	$tdatahome_slider[".allSearchFields"][] = "link";
	$tdatahome_slider[".allSearchFields"][] = "photo";
	

$tdatahome_slider[".googleLikeFields"] = array();
$tdatahome_slider[".googleLikeFields"][] = "id";
$tdatahome_slider[".googleLikeFields"][] = "title";
$tdatahome_slider[".googleLikeFields"][] = "desc";
$tdatahome_slider[".googleLikeFields"][] = "link";
$tdatahome_slider[".googleLikeFields"][] = "photo";
$tdatahome_slider[".googleLikeFields"][] = "view";
$tdatahome_slider[".googleLikeFields"][] = "lang_id";
$tdatahome_slider[".googleLikeFields"][] = "order";


$tdatahome_slider[".advSearchFields"] = array();
$tdatahome_slider[".advSearchFields"][] = "id";
$tdatahome_slider[".advSearchFields"][] = "lang_id";
$tdatahome_slider[".advSearchFields"][] = "view";
$tdatahome_slider[".advSearchFields"][] = "order";
$tdatahome_slider[".advSearchFields"][] = "title";
$tdatahome_slider[".advSearchFields"][] = "desc";
$tdatahome_slider[".advSearchFields"][] = "link";
$tdatahome_slider[".advSearchFields"][] = "photo";

$tdatahome_slider[".tableType"] = "list";

$tdatahome_slider[".printerPageOrientation"] = 0;
$tdatahome_slider[".nPrinterPageScale"] = 100;

$tdatahome_slider[".nPrinterSplitRecords"] = 40;

$tdatahome_slider[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdatahome_slider[".pageSize"] = 20;

$tdatahome_slider[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatahome_slider[".strOrderBy"] = $tstrOrderBy;

$tdatahome_slider[".orderindexes"] = array();

$tdatahome_slider[".sqlHead"] = "SELECT id,  	title,  	`desc`,  	link,  	photo,  	`view`,  	lang_id,  	`order`";
$tdatahome_slider[".sqlFrom"] = "FROM home_slider";
$tdatahome_slider[".sqlWhereExpr"] = "";
$tdatahome_slider[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatahome_slider[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatahome_slider[".arrGroupsPerPage"] = $arrGPP;

$tdatahome_slider[".highlightSearchResults"] = true;

$tableKeyshome_slider = array();
$tableKeyshome_slider[] = "id";
$tdatahome_slider[".Keys"] = $tableKeyshome_slider;

$tdatahome_slider[".listFields"] = array();
$tdatahome_slider[".listFields"][] = "id";
$tdatahome_slider[".listFields"][] = "lang_id";
$tdatahome_slider[".listFields"][] = "view";
$tdatahome_slider[".listFields"][] = "order";
$tdatahome_slider[".listFields"][] = "title";

$tdatahome_slider[".hideMobileList"] = array();


$tdatahome_slider[".viewFields"] = array();
$tdatahome_slider[".viewFields"][] = "id";
$tdatahome_slider[".viewFields"][] = "lang_id";
$tdatahome_slider[".viewFields"][] = "view";
$tdatahome_slider[".viewFields"][] = "order";
$tdatahome_slider[".viewFields"][] = "title";
$tdatahome_slider[".viewFields"][] = "desc";
$tdatahome_slider[".viewFields"][] = "link";
$tdatahome_slider[".viewFields"][] = "photo";

$tdatahome_slider[".addFields"] = array();
$tdatahome_slider[".addFields"][] = "lang_id";
$tdatahome_slider[".addFields"][] = "view";
$tdatahome_slider[".addFields"][] = "order";
$tdatahome_slider[".addFields"][] = "title";
$tdatahome_slider[".addFields"][] = "desc";
$tdatahome_slider[".addFields"][] = "link";
$tdatahome_slider[".addFields"][] = "photo";

$tdatahome_slider[".inlineAddFields"] = array();

$tdatahome_slider[".editFields"] = array();
$tdatahome_slider[".editFields"][] = "lang_id";
$tdatahome_slider[".editFields"][] = "view";
$tdatahome_slider[".editFields"][] = "order";
$tdatahome_slider[".editFields"][] = "title";
$tdatahome_slider[".editFields"][] = "desc";
$tdatahome_slider[".editFields"][] = "link";
$tdatahome_slider[".editFields"][] = "photo";

$tdatahome_slider[".inlineEditFields"] = array();

$tdatahome_slider[".exportFields"] = array();
$tdatahome_slider[".exportFields"][] = "id";
$tdatahome_slider[".exportFields"][] = "lang_id";
$tdatahome_slider[".exportFields"][] = "view";
$tdatahome_slider[".exportFields"][] = "order";
$tdatahome_slider[".exportFields"][] = "title";
$tdatahome_slider[".exportFields"][] = "desc";
$tdatahome_slider[".exportFields"][] = "link";
$tdatahome_slider[".exportFields"][] = "photo";

$tdatahome_slider[".importFields"] = array();

$tdatahome_slider[".printFields"] = array();
$tdatahome_slider[".printFields"][] = "id";
$tdatahome_slider[".printFields"][] = "lang_id";
$tdatahome_slider[".printFields"][] = "view";
$tdatahome_slider[".printFields"][] = "order";
$tdatahome_slider[".printFields"][] = "title";
$tdatahome_slider[".printFields"][] = "desc";
$tdatahome_slider[".printFields"][] = "link";
$tdatahome_slider[".printFields"][] = "photo";

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "home_slider";
	$fdata["Label"] = GetFieldLabel("home_slider","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatahome_slider["id"] = $fdata;
//	title
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "title";
	$fdata["GoodName"] = "title";
	$fdata["ownerTable"] = "home_slider";
	$fdata["Label"] = GetFieldLabel("home_slider","title"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "title"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "title";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatahome_slider["title"] = $fdata;
//	desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "desc";
	$fdata["GoodName"] = "desc";
	$fdata["ownerTable"] = "home_slider";
	$fdata["Label"] = GetFieldLabel("home_slider","desc"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "desc"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`desc`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatahome_slider["desc"] = $fdata;
//	link
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "link";
	$fdata["GoodName"] = "link";
	$fdata["ownerTable"] = "home_slider";
	$fdata["Label"] = GetFieldLabel("home_slider","link"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "link"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "link";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatahome_slider["link"] = $fdata;
//	photo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "photo";
	$fdata["GoodName"] = "photo";
	$fdata["ownerTable"] = "home_slider";
	$fdata["Label"] = GetFieldLabel("home_slider","photo"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "photo"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "photo";
	
		$fdata["DeleteAssociatedFile"] = true;
	
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "../uploads/";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Document Download");
	
		
		
		
								$vdata["ShowIcon"] = true; 
			
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Document upload");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
							$edata["acceptFileTypes"] = "bmp";
						$edata["acceptFileTypes"] .= "|gif";
						$edata["acceptFileTypes"] .= "|jpg";
						$edata["acceptFileTypes"] .= "|png";
		$edata["acceptFileTypes"] = "(".$edata["acceptFileTypes"].")$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdatahome_slider["photo"] = $fdata;
//	view
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "view";
	$fdata["GoodName"] = "view";
	$fdata["ownerTable"] = "home_slider";
	$fdata["Label"] = GetFieldLabel("home_slider","view"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "view"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`view`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Checkbox");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Checkbox");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatahome_slider["view"] = $fdata;
//	lang_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "lang_id";
	$fdata["GoodName"] = "lang_id";
	$fdata["ownerTable"] = "home_slider";
	$fdata["Label"] = GetFieldLabel("home_slider","lang_id"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "lang_id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lang_id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Lookup wizard");
	
			
	
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "langyages";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
		
		
			
	$edata["LinkField"] = "id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "name";
	
		
	$edata["LookupOrderBy"] = "";
	
		
			
		
				
	
	
		
		$edata["SelectSize"] = 1;
		
// End Lookup Settings


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatahome_slider["lang_id"] = $fdata;
//	order
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "order";
	$fdata["GoodName"] = "order";
	$fdata["ownerTable"] = "home_slider";
	$fdata["Label"] = GetFieldLabel("home_slider","order"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "order"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`order`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdatahome_slider["order"] = $fdata;

	
$tables_data["home_slider"]=&$tdatahome_slider;
$field_labels["home_slider"] = &$fieldLabelshome_slider;
$fieldToolTips["home_slider"] = &$fieldToolTipshome_slider;
$page_titles["home_slider"] = &$pageTitleshome_slider;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["home_slider"] = array();
	
// tables which are master tables for current table (detail)
$masterTablesData["home_slider"] = array();


	
				$strOriginalDetailsTable="langyages";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="langyages";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "langyages";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispInfo"]= "1";
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["home_slider"][0] = $masterParams;	
				$masterTablesData["home_slider"][0]["masterKeys"] = array();
	$masterTablesData["home_slider"][0]["masterKeys"][]="id";
				$masterTablesData["home_slider"][0]["detailKeys"] = array();
	$masterTablesData["home_slider"][0]["detailKeys"][]="lang_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_home_slider()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  	title,  	`desc`,  	link,  	photo,  	`view`,  	lang_id,  	`order`";
$proto0["m_strFrom"] = "FROM home_slider";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "home_slider",
	"m_srcTableName" => "home_slider"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "home_slider";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "title",
	"m_strTable" => "home_slider",
	"m_srcTableName" => "home_slider"
));

$proto7["m_sql"] = "title";
$proto7["m_srcTableName"] = "home_slider";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "desc",
	"m_strTable" => "home_slider",
	"m_srcTableName" => "home_slider"
));

$proto9["m_sql"] = "`desc`";
$proto9["m_srcTableName"] = "home_slider";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "link",
	"m_strTable" => "home_slider",
	"m_srcTableName" => "home_slider"
));

$proto11["m_sql"] = "link";
$proto11["m_srcTableName"] = "home_slider";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "photo",
	"m_strTable" => "home_slider",
	"m_srcTableName" => "home_slider"
));

$proto13["m_sql"] = "photo";
$proto13["m_srcTableName"] = "home_slider";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "view",
	"m_strTable" => "home_slider",
	"m_srcTableName" => "home_slider"
));

$proto15["m_sql"] = "`view`";
$proto15["m_srcTableName"] = "home_slider";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "lang_id",
	"m_strTable" => "home_slider",
	"m_srcTableName" => "home_slider"
));

$proto17["m_sql"] = "lang_id";
$proto17["m_srcTableName"] = "home_slider";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "order",
	"m_strTable" => "home_slider",
	"m_srcTableName" => "home_slider"
));

$proto19["m_sql"] = "`order`";
$proto19["m_srcTableName"] = "home_slider";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto21=array();
$proto21["m_link"] = "SQLL_MAIN";
			$proto22=array();
$proto22["m_strName"] = "home_slider";
$proto22["m_srcTableName"] = "home_slider";
$proto22["m_columns"] = array();
$proto22["m_columns"][] = "id";
$proto22["m_columns"][] = "title";
$proto22["m_columns"][] = "desc";
$proto22["m_columns"][] = "link";
$proto22["m_columns"][] = "photo";
$proto22["m_columns"][] = "view";
$proto22["m_columns"][] = "lang_id";
$proto22["m_columns"][] = "order";
$obj = new SQLTable($proto22);

$proto21["m_table"] = $obj;
$proto21["m_sql"] = "home_slider";
$proto21["m_alias"] = "";
$proto21["m_srcTableName"] = "home_slider";
$proto23=array();
$proto23["m_sql"] = "";
$proto23["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto23["m_column"]=$obj;
$proto23["m_contained"] = array();
$proto23["m_strCase"] = "";
$proto23["m_havingmode"] = false;
$proto23["m_inBrackets"] = false;
$proto23["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto23);

$proto21["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto21);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="home_slider";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_home_slider = createSqlQuery_home_slider();


	
								
	
$tdatahome_slider[".sqlquery"] = $queryData_home_slider;

$tableEvents["home_slider"] = new eventsBase;
$tdatahome_slider[".hasEvents"] = false;

?>