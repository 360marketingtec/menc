<?php
require_once(getabspath("classes/cipherer.php"));




$tdataarticles = array();	
	$tdataarticles[".truncateText"] = true;
	$tdataarticles[".NumberOfChars"] = 80; 
	$tdataarticles[".ShortName"] = "articles";
	$tdataarticles[".OwnerID"] = "";
	$tdataarticles[".OriginalTable"] = "articles";

//	field labels
$fieldLabelsarticles = array();
$fieldToolTipsarticles = array();
$pageTitlesarticles = array();

if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsarticles["English"] = array();
	$fieldToolTipsarticles["English"] = array();
	$pageTitlesarticles["English"] = array();
	$fieldLabelsarticles["English"]["id"] = "Id";
	$fieldToolTipsarticles["English"]["id"] = "";
	$fieldLabelsarticles["English"]["parent"] = "Parent";
	$fieldToolTipsarticles["English"]["parent"] = "";
	$fieldLabelsarticles["English"]["view"] = "View";
	$fieldToolTipsarticles["English"]["view"] = "";
	$fieldLabelsarticles["English"]["order"] = "Order";
	$fieldToolTipsarticles["English"]["order"] = "";
	$fieldLabelsarticles["English"]["url"] = "Url";
	$fieldToolTipsarticles["English"]["url"] = "";
	$fieldLabelsarticles["English"]["use_full_url"] = "Use Full Url";
	$fieldToolTipsarticles["English"]["use_full_url"] = "";
	$fieldLabelsarticles["English"]["title"] = "Title";
	$fieldToolTipsarticles["English"]["title"] = "";
	$fieldLabelsarticles["English"]["data"] = "Data";
	$fieldToolTipsarticles["English"]["data"] = "";
	$fieldLabelsarticles["English"]["meta_title"] = "Meta Title";
	$fieldToolTipsarticles["English"]["meta_title"] = "";
	$fieldLabelsarticles["English"]["meta_desc"] = "Meta Desc";
	$fieldToolTipsarticles["English"]["meta_desc"] = "";
	$fieldLabelsarticles["English"]["meta_keywords"] = "Meta Keywords";
	$fieldToolTipsarticles["English"]["meta_keywords"] = "";
	$fieldLabelsarticles["English"]["lang_id"] = "Lang Id";
	$fieldToolTipsarticles["English"]["lang_id"] = "";
	$fieldLabelsarticles["English"]["show_form"] = "Show Form";
	$fieldToolTipsarticles["English"]["show_form"] = "";
	$fieldLabelsarticles["English"]["photo"] = "Photo";
	$fieldToolTipsarticles["English"]["photo"] = "";
	if (count($fieldToolTipsarticles["English"]))
		$tdataarticles[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="Arabic")
{
	$fieldLabelsarticles["Arabic"] = array();
	$fieldToolTipsarticles["Arabic"] = array();
	$pageTitlesarticles["Arabic"] = array();
	$fieldLabelsarticles["Arabic"]["id"] = "Id";
	$fieldToolTipsarticles["Arabic"]["id"] = "";
	$fieldLabelsarticles["Arabic"]["parent"] = "Parent";
	$fieldToolTipsarticles["Arabic"]["parent"] = "";
	$fieldLabelsarticles["Arabic"]["view"] = "View";
	$fieldToolTipsarticles["Arabic"]["view"] = "";
	$fieldLabelsarticles["Arabic"]["order"] = "Order";
	$fieldToolTipsarticles["Arabic"]["order"] = "";
	$fieldLabelsarticles["Arabic"]["url"] = "Url";
	$fieldToolTipsarticles["Arabic"]["url"] = "";
	$fieldLabelsarticles["Arabic"]["use_full_url"] = "Use Full Url";
	$fieldToolTipsarticles["Arabic"]["use_full_url"] = "";
	$fieldLabelsarticles["Arabic"]["title"] = "Title";
	$fieldToolTipsarticles["Arabic"]["title"] = "";
	$fieldLabelsarticles["Arabic"]["data"] = "Data";
	$fieldToolTipsarticles["Arabic"]["data"] = "";
	$fieldLabelsarticles["Arabic"]["meta_title"] = "Meta Title";
	$fieldToolTipsarticles["Arabic"]["meta_title"] = "";
	$fieldLabelsarticles["Arabic"]["meta_desc"] = "Meta Desc";
	$fieldToolTipsarticles["Arabic"]["meta_desc"] = "";
	$fieldLabelsarticles["Arabic"]["meta_keywords"] = "Meta Keywords";
	$fieldToolTipsarticles["Arabic"]["meta_keywords"] = "";
	$fieldLabelsarticles["Arabic"]["lang_id"] = "Lang Id";
	$fieldToolTipsarticles["Arabic"]["lang_id"] = "";
	$fieldLabelsarticles["Arabic"]["show_form"] = "Show Form";
	$fieldToolTipsarticles["Arabic"]["show_form"] = "";
	$fieldLabelsarticles["Arabic"]["photo"] = "Photo";
	$fieldToolTipsarticles["Arabic"]["photo"] = "";
	if (count($fieldToolTipsarticles["Arabic"]))
		$tdataarticles[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsarticles[""] = array();
	$fieldToolTipsarticles[""] = array();
	$pageTitlesarticles[""] = array();
	if (count($fieldToolTipsarticles[""]))
		$tdataarticles[".isUseToolTips"] = true;
}
	
	
	$tdataarticles[".NCSearch"] = true;



$tdataarticles[".shortTableName"] = "articles";
$tdataarticles[".nSecOptions"] = 0;
$tdataarticles[".recsPerRowList"] = 1;
$tdataarticles[".mainTableOwnerID"] = "";
$tdataarticles[".moveNext"] = 1;
$tdataarticles[".nType"] = 0;

$tdataarticles[".strOriginalTableName"] = "articles";




$tdataarticles[".showAddInPopup"] = false;

$tdataarticles[".showEditInPopup"] = false;

$tdataarticles[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdataarticles[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdataarticles[".fieldsForRegister"] = array();

$tdataarticles[".listAjax"] = false;

	$tdataarticles[".audit"] = true;

	$tdataarticles[".locking"] = true;

$tdataarticles[".edit"] = true;

$tdataarticles[".list"] = true;

$tdataarticles[".view"] = true;


$tdataarticles[".exportTo"] = true;

$tdataarticles[".printFriendly"] = true;

$tdataarticles[".delete"] = true;

$tdataarticles[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdataarticles[".searchSaving"] = false;
//

$tdataarticles[".showSearchPanel"] = true;
		$tdataarticles[".flexibleSearch"] = true;		

if (isMobile())
	$tdataarticles[".isUseAjaxSuggest"] = false;
else 
	$tdataarticles[".isUseAjaxSuggest"] = true;

$tdataarticles[".rowHighlite"] = true;



$tdataarticles[".addPageEvents"] = false;

// use timepicker for search panel
$tdataarticles[".isUseTimeForSearch"] = false;





$tdataarticles[".allSearchFields"] = array();
$tdataarticles[".filterFields"] = array();
$tdataarticles[".requiredSearchFields"] = array();

$tdataarticles[".allSearchFields"][] = "id";
	$tdataarticles[".allSearchFields"][] = "lang_id";
	$tdataarticles[".allSearchFields"][] = "parent";
	$tdataarticles[".allSearchFields"][] = "view";
	$tdataarticles[".allSearchFields"][] = "show_form";
	$tdataarticles[".allSearchFields"][] = "order";
	$tdataarticles[".allSearchFields"][] = "url";
	$tdataarticles[".allSearchFields"][] = "use_full_url";
	$tdataarticles[".allSearchFields"][] = "title";
	$tdataarticles[".allSearchFields"][] = "data";
	$tdataarticles[".allSearchFields"][] = "photo";
	$tdataarticles[".allSearchFields"][] = "meta_title";
	$tdataarticles[".allSearchFields"][] = "meta_desc";
	$tdataarticles[".allSearchFields"][] = "meta_keywords";
	

$tdataarticles[".googleLikeFields"] = array();
$tdataarticles[".googleLikeFields"][] = "id";
$tdataarticles[".googleLikeFields"][] = "parent";
$tdataarticles[".googleLikeFields"][] = "view";
$tdataarticles[".googleLikeFields"][] = "order";
$tdataarticles[".googleLikeFields"][] = "url";
$tdataarticles[".googleLikeFields"][] = "use_full_url";
$tdataarticles[".googleLikeFields"][] = "title";
$tdataarticles[".googleLikeFields"][] = "data";
$tdataarticles[".googleLikeFields"][] = "meta_title";
$tdataarticles[".googleLikeFields"][] = "meta_desc";
$tdataarticles[".googleLikeFields"][] = "meta_keywords";
$tdataarticles[".googleLikeFields"][] = "lang_id";
$tdataarticles[".googleLikeFields"][] = "show_form";
$tdataarticles[".googleLikeFields"][] = "photo";


$tdataarticles[".advSearchFields"] = array();
$tdataarticles[".advSearchFields"][] = "id";
$tdataarticles[".advSearchFields"][] = "lang_id";
$tdataarticles[".advSearchFields"][] = "parent";
$tdataarticles[".advSearchFields"][] = "view";
$tdataarticles[".advSearchFields"][] = "show_form";
$tdataarticles[".advSearchFields"][] = "order";
$tdataarticles[".advSearchFields"][] = "url";
$tdataarticles[".advSearchFields"][] = "use_full_url";
$tdataarticles[".advSearchFields"][] = "title";
$tdataarticles[".advSearchFields"][] = "data";
$tdataarticles[".advSearchFields"][] = "photo";
$tdataarticles[".advSearchFields"][] = "meta_title";
$tdataarticles[".advSearchFields"][] = "meta_desc";
$tdataarticles[".advSearchFields"][] = "meta_keywords";

$tdataarticles[".tableType"] = "list";

$tdataarticles[".printerPageOrientation"] = 0;
$tdataarticles[".nPrinterPageScale"] = 100;

$tdataarticles[".nPrinterSplitRecords"] = 40;

$tdataarticles[".nPrinterPDFSplitRecords"] = 40;





	





// view page pdf

// print page pdf


$tdataarticles[".pageSize"] = 20;

$tdataarticles[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdataarticles[".strOrderBy"] = $tstrOrderBy;

$tdataarticles[".orderindexes"] = array();

$tdataarticles[".sqlHead"] = "SELECT id,  parent,  `view`,  `order`,  url,  use_full_url,  title,  `data`,  meta_title,  meta_desc,  meta_keywords,  lang_id,  show_form,  photo";
$tdataarticles[".sqlFrom"] = "FROM articles";
$tdataarticles[".sqlWhereExpr"] = "";
$tdataarticles[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdataarticles[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdataarticles[".arrGroupsPerPage"] = $arrGPP;

$tdataarticles[".highlightSearchResults"] = true;

$tableKeysarticles = array();
$tableKeysarticles[] = "id";
$tdataarticles[".Keys"] = $tableKeysarticles;

$tdataarticles[".listFields"] = array();
$tdataarticles[".listFields"][] = "id";
$tdataarticles[".listFields"][] = "lang_id";
$tdataarticles[".listFields"][] = "parent";
$tdataarticles[".listFields"][] = "view";
$tdataarticles[".listFields"][] = "order";
$tdataarticles[".listFields"][] = "title";

$tdataarticles[".hideMobileList"] = array();


$tdataarticles[".viewFields"] = array();
$tdataarticles[".viewFields"][] = "id";
$tdataarticles[".viewFields"][] = "lang_id";
$tdataarticles[".viewFields"][] = "parent";
$tdataarticles[".viewFields"][] = "view";
$tdataarticles[".viewFields"][] = "show_form";
$tdataarticles[".viewFields"][] = "order";
$tdataarticles[".viewFields"][] = "url";
$tdataarticles[".viewFields"][] = "use_full_url";
$tdataarticles[".viewFields"][] = "title";
$tdataarticles[".viewFields"][] = "data";
$tdataarticles[".viewFields"][] = "photo";
$tdataarticles[".viewFields"][] = "meta_title";
$tdataarticles[".viewFields"][] = "meta_desc";
$tdataarticles[".viewFields"][] = "meta_keywords";

$tdataarticles[".addFields"] = array();
$tdataarticles[".addFields"][] = "lang_id";
$tdataarticles[".addFields"][] = "parent";
$tdataarticles[".addFields"][] = "view";
$tdataarticles[".addFields"][] = "show_form";
$tdataarticles[".addFields"][] = "order";
$tdataarticles[".addFields"][] = "url";
$tdataarticles[".addFields"][] = "use_full_url";
$tdataarticles[".addFields"][] = "title";
$tdataarticles[".addFields"][] = "data";
$tdataarticles[".addFields"][] = "photo";
$tdataarticles[".addFields"][] = "meta_title";
$tdataarticles[".addFields"][] = "meta_desc";
$tdataarticles[".addFields"][] = "meta_keywords";

$tdataarticles[".inlineAddFields"] = array();

$tdataarticles[".editFields"] = array();
$tdataarticles[".editFields"][] = "lang_id";
$tdataarticles[".editFields"][] = "parent";
$tdataarticles[".editFields"][] = "view";
$tdataarticles[".editFields"][] = "show_form";
$tdataarticles[".editFields"][] = "order";
$tdataarticles[".editFields"][] = "url";
$tdataarticles[".editFields"][] = "use_full_url";
$tdataarticles[".editFields"][] = "title";
$tdataarticles[".editFields"][] = "data";
$tdataarticles[".editFields"][] = "photo";
$tdataarticles[".editFields"][] = "meta_title";
$tdataarticles[".editFields"][] = "meta_desc";
$tdataarticles[".editFields"][] = "meta_keywords";

$tdataarticles[".inlineEditFields"] = array();

$tdataarticles[".exportFields"] = array();
$tdataarticles[".exportFields"][] = "id";
$tdataarticles[".exportFields"][] = "lang_id";
$tdataarticles[".exportFields"][] = "parent";
$tdataarticles[".exportFields"][] = "view";
$tdataarticles[".exportFields"][] = "show_form";
$tdataarticles[".exportFields"][] = "order";
$tdataarticles[".exportFields"][] = "url";
$tdataarticles[".exportFields"][] = "use_full_url";
$tdataarticles[".exportFields"][] = "title";
$tdataarticles[".exportFields"][] = "data";
$tdataarticles[".exportFields"][] = "photo";
$tdataarticles[".exportFields"][] = "meta_title";
$tdataarticles[".exportFields"][] = "meta_desc";
$tdataarticles[".exportFields"][] = "meta_keywords";

$tdataarticles[".importFields"] = array();

$tdataarticles[".printFields"] = array();
$tdataarticles[".printFields"][] = "id";
$tdataarticles[".printFields"][] = "lang_id";
$tdataarticles[".printFields"][] = "parent";
$tdataarticles[".printFields"][] = "view";
$tdataarticles[".printFields"][] = "show_form";
$tdataarticles[".printFields"][] = "order";
$tdataarticles[".printFields"][] = "url";
$tdataarticles[".printFields"][] = "use_full_url";
$tdataarticles[".printFields"][] = "title";
$tdataarticles[".printFields"][] = "data";
$tdataarticles[".printFields"][] = "photo";
$tdataarticles[".printFields"][] = "meta_title";
$tdataarticles[".printFields"][] = "meta_desc";
$tdataarticles[".printFields"][] = "meta_keywords";

//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","id"); 
	$fdata["FieldType"] = 3;
	
		
		$fdata["AutoInc"] = true;
	
		
				
		$fdata["bListPage"] = true; 
	
		
		
		
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataarticles["id"] = $fdata;
//	parent
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "parent";
	$fdata["GoodName"] = "parent";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","parent"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "parent"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "parent";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Lookup wizard");
	
			
	
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "articles";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
		
		
			
	$edata["LinkField"] = "id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "title";
	
		
	$edata["LookupOrderBy"] = "";
	
		
			
		
				
	
	
		
		$edata["SelectSize"] = 1;
		
// End Lookup Settings


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataarticles["parent"] = $fdata;
//	view
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "view";
	$fdata["GoodName"] = "view";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","view"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "view"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`view`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Checkbox");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Checkbox");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataarticles["view"] = $fdata;
//	order
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "order";
	$fdata["GoodName"] = "order";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","order"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "order"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`order`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "number";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataarticles["order"] = $fdata;
//	url
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "url";
	$fdata["GoodName"] = "url";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","url"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "url"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "url";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataarticles["url"] = $fdata;
//	use_full_url
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "use_full_url";
	$fdata["GoodName"] = "use_full_url";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","use_full_url"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "use_full_url"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "use_full_url";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Checkbox");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Checkbox");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataarticles["use_full_url"] = $fdata;
//	title
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "title";
	$fdata["GoodName"] = "title";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","title"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "title"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "title";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text field");
	
			
	
	


		$edata["IsRequired"] = true; 
	
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
			$edata["HTML5InuptType"] = "text";
	
		$edata["EditParams"] = "";
			
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataarticles["title"] = $fdata;
//	data
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "data";
	$fdata["GoodName"] = "data";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","data"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "data"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "`data`";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "HTML");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		$edata["UseRTE"] = true; 
	
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataarticles["data"] = $fdata;
//	meta_title
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "meta_title";
	$fdata["GoodName"] = "meta_title";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","meta_title"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_title"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_title";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataarticles["meta_title"] = $fdata;
//	meta_desc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "meta_desc";
	$fdata["GoodName"] = "meta_desc";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","meta_desc"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_desc"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_desc";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataarticles["meta_desc"] = $fdata;
//	meta_keywords
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "meta_keywords";
	$fdata["GoodName"] = "meta_keywords";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","meta_keywords"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "meta_keywords"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "meta_keywords";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Text area");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
				$edata["nRows"] = 100;
			$edata["nCols"] = 500;
	
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataarticles["meta_keywords"] = $fdata;
//	lang_id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "lang_id";
	$fdata["GoodName"] = "lang_id";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","lang_id"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		$fdata["bListPage"] = true; 
	
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "lang_id"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lang_id";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "");
	
		
		
		
		
		
		
		
		
		
		
		
		$vdata["NeedEncode"] = true;
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Lookup wizard");
	
			
	
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "langyages";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;
		
		
			
	$edata["LinkField"] = "id";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "name";
	
		
	$edata["LookupOrderBy"] = "";
	
		
			
		
				
	
	
		
		$edata["SelectSize"] = 1;
		
// End Lookup Settings


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataarticles["lang_id"] = $fdata;
//	show_form
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "show_form";
	$fdata["GoodName"] = "show_form";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","show_form"); 
	$fdata["FieldType"] = 3;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "show_form"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "show_form";
	
		
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "files";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Checkbox");
	
		
		
		
		
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Checkbox");
	
			
	
	


		
		
		
		
			$edata["acceptFileTypes"] = ".+$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
								
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between");
// the end of search options settings	

	

	
	$tdataarticles["show_form"] = $fdata;
//	photo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "photo";
	$fdata["GoodName"] = "photo";
	$fdata["ownerTable"] = "articles";
	$fdata["Label"] = GetFieldLabel("articles","photo"); 
	$fdata["FieldType"] = 201;
	
		
		
		
				
		
		$fdata["bAddPage"] = true; 
	
		
		$fdata["bEditPage"] = true; 
	
		
		$fdata["bViewPage"] = true; 
	
		$fdata["bAdvancedSearch"] = true; 
	
		$fdata["bPrinterPage"] = true; 
	
		$fdata["bExportPage"] = true; 
	
		$fdata["strField"] = "photo"; 
	
		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "photo";
	
		$fdata["DeleteAssociatedFile"] = true;
	
		
				$fdata["FieldPermissions"] = true;
	
				$fdata["UploadFolder"] = "../uploads/";
		
//  Begin View Formats
	$fdata["ViewFormats"] = array();
	
	$vdata = array("ViewFormat" => "Document Download");
	
		
		
		
								$vdata["ShowIcon"] = true; 
			
		
		
		
		
		
		
		
		
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats 	
	$fdata["EditFormats"] = array();
	
	$edata = array("EditFormat" => "Document upload");
	
			
	
	


		
		
		
		
							$edata["acceptFileTypes"] = "bmp";
						$edata["acceptFileTypes"] .= "|gif";
						$edata["acceptFileTypes"] .= "|jpg";
						$edata["acceptFileTypes"] .= "|png";
		$edata["acceptFileTypes"] = "(".$edata["acceptFileTypes"].")$";
	
		$edata["maxNumberOfFiles"] = 1;
	
		
		
		
		
		
		
		$edata["controlWidth"] = 200;
	
//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
		
		
	//	End validation
	
		
				
		
	
		
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats
	
	
	$fdata["isSeparate"] = false;
	
	
	
	
// the field's search options settings
		
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Empty");
// the end of search options settings	

	

	
	$tdataarticles["photo"] = $fdata;

	
$tables_data["articles"]=&$tdataarticles;
$field_labels["articles"] = &$fieldLabelsarticles;
$fieldToolTips["articles"] = &$fieldToolTipsarticles;
$page_titles["articles"] = &$pageTitlesarticles;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["articles"] = array();
	
// tables which are master tables for current table (detail)
$masterTablesData["articles"] = array();


	
				$strOriginalDetailsTable="langyages";
	$masterParams = array();
	$masterParams["mDataSourceTable"]="langyages";
	$masterParams["mOriginalTable"]= $strOriginalDetailsTable;
	$masterParams["mShortTable"]= "langyages";
	$masterParams["masterKeys"]= array();
	$masterParams["detailKeys"]= array();
	$masterParams["dispChildCount"]= "1";
	$masterParams["hideChild"]= "0";
	$masterParams["dispInfo"]= "1";
	$masterParams["previewOnList"]= 1;
	$masterParams["previewOnAdd"]= 0;
	$masterParams["previewOnEdit"]= 0;
	$masterParams["previewOnView"]= 0;
	$masterParams["type"] = PAGE_LIST;
					$masterTablesData["articles"][0] = $masterParams;	
				$masterTablesData["articles"][0]["masterKeys"] = array();
	$masterTablesData["articles"][0]["masterKeys"][]="id";
				$masterTablesData["articles"][0]["detailKeys"] = array();
	$masterTablesData["articles"][0]["detailKeys"][]="lang_id";
		
// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_articles()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  parent,  `view`,  `order`,  url,  use_full_url,  title,  `data`,  meta_title,  meta_desc,  meta_keywords,  lang_id,  show_form,  photo";
$proto0["m_strFrom"] = "FROM articles";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
			$proto0["cipherer"] = null;
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = false;
$proto1["m_inBrackets"] = false;
$proto1["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto5["m_sql"] = "id";
$proto5["m_srcTableName"] = "articles";
$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "parent",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto7["m_sql"] = "parent";
$proto7["m_srcTableName"] = "articles";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "view",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto9["m_sql"] = "`view`";
$proto9["m_srcTableName"] = "articles";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "order",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto11["m_sql"] = "`order`";
$proto11["m_srcTableName"] = "articles";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "url",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto13["m_sql"] = "url";
$proto13["m_srcTableName"] = "articles";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "use_full_url",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto15["m_sql"] = "use_full_url";
$proto15["m_srcTableName"] = "articles";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "title",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto17["m_sql"] = "title";
$proto17["m_srcTableName"] = "articles";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto19["m_sql"] = "`data`";
$proto19["m_srcTableName"] = "articles";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_title",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto21["m_sql"] = "meta_title";
$proto21["m_srcTableName"] = "articles";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto0["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_desc",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto23["m_sql"] = "meta_desc";
$proto23["m_srcTableName"] = "articles";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto0["m_fieldlist"][]=$obj;
						$proto25=array();
			$obj = new SQLField(array(
	"m_strName" => "meta_keywords",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto25["m_sql"] = "meta_keywords";
$proto25["m_srcTableName"] = "articles";
$proto25["m_expr"]=$obj;
$proto25["m_alias"] = "";
$obj = new SQLFieldListItem($proto25);

$proto0["m_fieldlist"][]=$obj;
						$proto27=array();
			$obj = new SQLField(array(
	"m_strName" => "lang_id",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto27["m_sql"] = "lang_id";
$proto27["m_srcTableName"] = "articles";
$proto27["m_expr"]=$obj;
$proto27["m_alias"] = "";
$obj = new SQLFieldListItem($proto27);

$proto0["m_fieldlist"][]=$obj;
						$proto29=array();
			$obj = new SQLField(array(
	"m_strName" => "show_form",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto29["m_sql"] = "show_form";
$proto29["m_srcTableName"] = "articles";
$proto29["m_expr"]=$obj;
$proto29["m_alias"] = "";
$obj = new SQLFieldListItem($proto29);

$proto0["m_fieldlist"][]=$obj;
						$proto31=array();
			$obj = new SQLField(array(
	"m_strName" => "photo",
	"m_strTable" => "articles",
	"m_srcTableName" => "articles"
));

$proto31["m_sql"] = "photo";
$proto31["m_srcTableName"] = "articles";
$proto31["m_expr"]=$obj;
$proto31["m_alias"] = "";
$obj = new SQLFieldListItem($proto31);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto33=array();
$proto33["m_link"] = "SQLL_MAIN";
			$proto34=array();
$proto34["m_strName"] = "articles";
$proto34["m_srcTableName"] = "articles";
$proto34["m_columns"] = array();
$proto34["m_columns"][] = "id";
$proto34["m_columns"][] = "parent";
$proto34["m_columns"][] = "view";
$proto34["m_columns"][] = "order";
$proto34["m_columns"][] = "url";
$proto34["m_columns"][] = "use_full_url";
$proto34["m_columns"][] = "title";
$proto34["m_columns"][] = "data";
$proto34["m_columns"][] = "meta_title";
$proto34["m_columns"][] = "meta_desc";
$proto34["m_columns"][] = "meta_keywords";
$proto34["m_columns"][] = "lang_id";
$proto34["m_columns"][] = "show_form";
$proto34["m_columns"][] = "photo";
$proto34["m_columns"][] = "home_main_article";
$obj = new SQLTable($proto34);

$proto33["m_table"] = $obj;
$proto33["m_sql"] = "articles";
$proto33["m_alias"] = "";
$proto33["m_srcTableName"] = "articles";
$proto35=array();
$proto35["m_sql"] = "";
$proto35["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto35["m_column"]=$obj;
$proto35["m_contained"] = array();
$proto35["m_strCase"] = "";
$proto35["m_havingmode"] = false;
$proto35["m_inBrackets"] = false;
$proto35["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto35);

$proto33["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto33);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="articles";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_articles = createSqlQuery_articles();


	
														
	
$tdataarticles[".sqlquery"] = $queryData_articles;

$tableEvents["articles"] = new eventsBase;
$tdataarticles[".hasEvents"] = false;

?>