﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    
    // added code for ckfinder ------>
    config.filebrowserBrowseUrl = '/admin/plugins/ckeditor/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = '/admin/plugins/ckeditor/ckfinder/ckfinder.html?type=Images';
    config.filebrowserFlashBrowseUrl = '/admin/plugins/ckeditor/ckfinder/ckfinder.html?type=Flash';
    config.filebrowserUploadUrl = '/admin/plugins/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = '/admin/plugins/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = '/admin/plugins/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
    // end: code for ckfinder ------>
    
}; 
