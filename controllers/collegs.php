<?php
//4

class collegs extends loader {
	var $metaTitle ;
	var $metaDesc ; 
	var $metaKeywords ;
//0
	protected function renderAllCollegs(){

		if( loader::$urlInfo[count(loader::$urlInfo)-1] ){
			$this->college = db_collegs::where('id', loader::$urlInfo[count(loader::$urlInfo)-1] );
		}else{
			$this->college = db_collegs::where('id','10','<');
		}
		$this->college = db_collegs::where('lang_id' ,loader::$langId);
		$this->college = db_collegs::select();
		    
		$this->metaTitle = $this->college[collegs][meta_title];
		$this->metaDesc = $this->college[collegs][meta_description];
		$this->metaKeywords = $this->college[collegs][meta_keywords];



		if( loader::$urlInfo[count(loader::$urlInfo)-1] ){

			$this->collegs = db_collegs::where('parent', loader::$urlInfo[count(loader::$urlInfo)-1] );
			$this->collegs = db_collegs::where('view', '1' );
			$this->collegs = db_collegs::where('lang_id' ,loader::$langId);
			$this->collegs = db_collegs::orderBy('order');
			$this->collegs = db_collegs::selectAll();

			if( ! count( $this->collegs ) ){
				$this->requestedView = 'renderCollege' ;
			}
		}


		view::renderLayout();

  	}

 

  
}
