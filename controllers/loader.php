<?php 

function __autoload($className) {
	if (file_exists(loader::$config[baseLocation].'/library/'.$className.'.php')){
		include_once loader::$config[baseLocation].'/library/'.$className.'.php' ;
		return new $className();
	}
	if (file_exists(loader::$config[baseLocation].'/models/'.$className.'.php')){
		include_once loader::$config[baseLocation].'/models/'.$className.'.php' ;
		return new $className();
	}
	$tableName = explode('_',$className);
	if($tableName[0] == 'db'){
		$table = "SELECT COUNT(*) AS count FROM information_schema.tables WHERE table_schema = '".loader::$config[database]."' AND table_name = '".str_replace('db_','',$className)."'";
		$table = SQLQuery::query($table,1) ;
		$table = array_values($table);
		if( $table[0][count] ){
			$model = loader::$config[baseLocation]."/models/".$className.".php";
			$fh = fopen($model, 'w') ;
			$stringData = '<?php
				class '.$className.' extends SQLQuery {
					public function __construct (){
						SQLQuery::$_table['.str_replace('db_','',$className).'] = "'.str_replace('db_','',$className).'";
					}
				}
			';
			fwrite($fh, $stringData);
			fclose($fh);
			include_once loader::$config[baseLocation].'/models/'.$className.'.php' ;
			return new $className();
		}else{
			die('Table "'.str_replace('db_','',$className). '" not found');
		}

	}
}

if(!function_exists('get_called_model')) {
        function get_called_model() {
            return class_tools::get_called_model();
        }
}

class loader {
	public static $config ;
	public static $requestedController ;
	public static $requestedAction ;
	public static $urlInfo ;
	public static $headerPlus ;
	public static $userId ;
	public static $langId = 2;
	public static $langFolder ;

	
	public function startLoader(){
		session_start();

		loader::$config = parse_ini_file("config.ini");
		loader::$urlInfo = explode( '/' , $_SERVER["REQUEST_URI"] ) ;

		loader::$config[baseLocation] = str_replace('/controllers','', dirname(__FILE__) );

		loader::$requestedController = loader::$config[enabledControllers][loader::$urlInfo[count(loader::$urlInfo)-3]] ;

		if( ! $_SESSION[referer] ){
			$_SESSION[referer] = $_SERVER['HTTP_REFERER'] ;
		}
		if( ! loader::$urlInfo[count(loader::$urlInfo)-3] ){
			// index ///
			loader::$requestedController = loader::$config[enabledControllers][1] ;
			loader::$urlInfo[count(loader::$urlInfo)-2] = 0 ;
		}
		if( ! loader::$requestedController ){
			// Not Found ///
			loader::$requestedController = loader::$config[enabledControllers][0] ;
			loader::$urlInfo[count(loader::$urlInfo)-2] = 0 ;
		}
		
	  	if( in_array( 'de' , explode( '.' , $_SERVER["HTTP_HOST"] ) ) ){
  	        loader::$langId = 3 ;
		}
	  	if( in_array( 'sp' , explode( '.' , $_SERVER["HTTP_HOST"] ) ) ){
  	        loader::$langId = 4 ;
		}
	  	if( in_array( 'it' , explode( '.' , $_SERVER["HTTP_HOST"] ) ) ){
  	        loader::$langId = 5 ;
		}

		// scape the tags 
		$_REQUEST = str_replace( array('<','>') , array('&lt;','&gt;') , $_REQUEST ) ;
		$_POST = str_replace( array('<','>') , array('&lt;','&gt;') , $_POST ) ;
		$_GET = str_replace( array('<','>') , array('&lt;','&gt;') , $_GET ) ;
	}
	
	public function __destruct(){
		SQLQuery::disconnect();
	}

	public function startApp(){

		$this->startLoader() ;

		include_once loader::$config[baseLocation].'/controllers/'.loader::$requestedController.'.php' ;

		$controller = new loader::$requestedController();
		$requestedAction = get_class_methods(loader::$requestedController) ;		
		$requestedAction = array_diff($requestedAction, get_class_methods(loader));

		loader::$requestedAction = $requestedAction ;

		$controller->$requestedAction[intval(loader::$urlInfo[count(loader::$urlInfo)-2])]();

		if( ! $requestedAction[intval(loader::$urlInfo[count(loader::$urlInfo)-2])] ){
			header("location:/Not/Found/");
		}
		translate::checkTranslationUpdates();
	}


}

