<?php

//1
class index extends loader {

	var $metaTitle ;
	var $metaDesc ;
	var $metaKeywords ;
	public $table = '';
//0
	protected function renderIndex(){

		$this->homePageMeta = db_articles::where('view','1');
		$this->homePageMeta = db_articles::where('id','10','<');
		$this->homePageMeta = db_articles::where('parent','null','is');
		$this->homePageMeta = db_articles::where('lang_id',loader::$langId);
		$this->homePageMeta = db_articles::select();

		$this->metaTitle = $this->homePageMeta[articles][meta_title];
		$this->metaDesc = $this->homePageMeta[articles][meta_desc];
		$this->metaKeywords = $this->homePageMeta[articles][meta_keywords];

		$this->offersSlider = db_home_slider::where('view',1);
		$this->offersSlider = db_home_slider::where('lang_id',loader::$langId);
		$this->offersSlider = db_home_slider::orderBy('order');
    $this->offersSlider = db_home_slider::selectAll();

		$this->centerSlider = db_home_center_slider::where('view',1);
		$this->centerSlider = db_home_center_slider::where('lang_id',loader::$langId);
		$this->centerSlider = db_home_center_slider::orderBy('order');
    $this->centerSlider = db_home_center_slider::selectAll();

		$this->partnersLogos = db_partners_logos::where('view',1);
		$this->partnersLogos = db_partners_logos::where('lang_id',loader::$langId);
		$this->partnersLogos = db_partners_logos::orderBy('order');
    $this->partnersLogos = db_partners_logos::selectAll();

		$this->companies = db_companies::where('show_on_home',1);
		$this->companies = db_companies::where('view',1);
		$this->companies = db_companies::where('lang_id',loader::$langId);
		$this->companies = db_companies::orderBy('order');
		//$this->productsCategories = db_products_categories::setLimit(3);
    $this->companies = db_companies::selectAll();

		$this->products = db_products::where('View' , 1);
		$this->products = db_products::where('top_product' , 1);
		$this->products = db_products::where('lang_id' ,loader::$langId);
		$this->products = db_products::selectAll();

#		$this->mainDialogs = db_home_main_dialogs::where('view',1);
#		$this->mainDialogs = db_home_main_dialogs::where('lang_id',loader::$langId);
#		$this->mainDialogs = db_home_main_dialogs::orderBy('order');
#		$this->mainDialogs = db_home_main_dialogs::setLimit(3);
#    	$this->mainDialogs = db_home_main_dialogs::selectAll();

#		$this->bottomDialogs = db_home_bottom_dialogs::where('view',1);
#		$this->bottomDialogs = db_home_bottom_dialogs::where('lang_id',loader::$langId);
#		$this->bottomDialogs = db_home_bottom_dialogs::orderBy('order');
#		$this->bottomDialogs = db_home_bottom_dialogs::setLimit(4);
#    	$this->bottomDialogs = db_home_bottom_dialogs::selectAll();

#		$this->homeMainArticle = db_articles::where('home_main_article','1');
#		$this->homeMainArticle = db_articles::where('lang_id',loader::$langId);
#		$this->homeMainArticle = db_articles::select();


		$this->headerPlus = '';

    	view::renderLayout();

	}

}
