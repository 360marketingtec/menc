<?php
//6

class news extends loader {
	var $metaTitle ;
	var $metaDesc ; 
	var $metaKeywords ;
//0
	protected function renderNewsPage(){

		$this->meta = db_news::where('id','10','<');
		$this->meta = db_news::where('lang_id' ,loader::$langId);
		$this->meta = db_news::select();
		    
		$this->metaTitle = $this->meta[news][meta_title];
		$this->metaDesc = $this->meta[news][meta_desc];
		$this->metaKeywords = $this->meta[news][meta_keywords];

		$this->hotNews = db_news::where('id','10','>');
		$this->hotNews = db_news::where('lang_id' ,loader::$langId);
		$this->hotNews = db_news::where('hot_news','1');
		$this->hotNews = db_news::where('view','1');
		$this->hotNews = db_news::orderBy('date');
		$this->hotNews = db_news::selectAll();

		$this->allNews = db_news::where('id','10','>');
		$this->allNews = db_news::where('lang_id' ,loader::$langId);
		$this->allNews = db_news::where('hot_news','0');
		$this->allNews = db_news::where('view','1');
		$this->allNews = db_news::orderBy('date');
		$this->allNews = db_news::setLimit(10);
		$this->allNews = db_news::selectAll();

		view::renderLayout();

  	}

//1

	protected function renderNews(){

		$this->news = db_news::where('id',loader::$urlInfo[count(loader::$urlInfo)-1]);
		$this->news = db_news::where('lang_id' ,loader::$langId);
		$this->news = db_news::select();
		    
		$this->metaTitle = $this->news[news][meta_title];
		$this->metaDesc = $this->news[news][meta_desc];
		$this->metaKeywords = $this->news[news][meta_keywords];

		$this->allNews = db_news::where('id','10','>');
		$this->allNews = db_news::where('lang_id' ,loader::$langId);
		$this->allNews = db_news::where('hot_news','0');
		$this->allNews = db_news::where('view','1');
		$this->allNews = db_news::orderBy('date');
		$this->allNews = db_news::setLimit(10);
		$this->allNews = db_news::selectAll();

		view::renderLayout();
	} 


  
}
