<?php

//4
class products extends loader {
	var $metaTitle ;
	var $metaDesc ;
	var $metaKeywords ;
//0
	protected function renderCompanies(){

		$this->company = db_companies::where(id,loader::$urlInfo[count(loader::$urlInfo)-1]);
		$this->company = db_companies::where('View' , 1);
		$this->company = db_companies::where('lang_id' ,loader::$langId);
		$this->company = db_companies::select();

		$this->metaTitle = $this->company[companies][meta_title];
		$this->metaDesc = $this->company[companies][meta_desc];
		$this->metaKeywords = $this->company[companies][meta_keywords];

		$this->subCtegories = db_companies::fields("id,title,url,photo,short_desc");
		$this->subCtegories = db_companies::where('parent',$this->company[companies][id]);
		$this->subCtegories = db_companies::where('view' , 1);
		$this->subCtegories = db_companies::where('lang_id' ,loader::$langId);
		$this->subCtegories = db_companies::orderBy('order');
		$this->subCtegories = db_companies::selectAll();


	//	if ( count($this->subCtegories[company][id]) < 1 ){

			$this->products = db_products::fields("id,title,url,photo,short_desc");
			$this->products = db_products::where('company_id',$this->company[companies][id]);
			$this->products = db_products::where('view' ,1);
			$this->products = db_products::where('lang_id' ,loader::$langId);
			$this->products = db_products::orderBy('order');
			$this->products = db_products::selectAll();

		//}

		/*$this->headerPlus = "
			<style>
				.container .grid_4 {
					max-height:477px;
					overflow:hidden;
				}
			</style>

		";*/

		view::renderLayout();


  }
//1
	protected function productDetails(){

		$this->product = db_products::where(id,loader::$urlInfo[count(loader::$urlInfo)-1]);
		$this->product = db_products::where('View' , 1);
		$this->product = db_products::where('lang_id' ,loader::$langId);
		$this->product = db_products::select();

		$this->productColors = db_product_colors::where('product_id',$this->product[products][id]);
		$this->productColors = db_product_colors::selectAll();
		//print_r($this->productColors);

			$this->colors = db_colors::where('id',$this->productColors[product_colors][color_id]);
			$this->colors = db_colors::selectAll();
		//print_r($this->colors);

		$this->metaTitle = $this->product[products][meta_title];
		$this->metaDesc = $this->product[products][meta_desc];
		$this->metaKeywords = $this->product[products][meta_keywords];

		$this->company = db_companies::fields("id,title,url,photo,short_desc");
		$this->company = db_companies::where('id',$this->product[products][company_id]);
		$this->company = db_companies::where('View' , 1);
		$this->company = db_companies::where('lang_id' ,loader::$langId);
		$this->company = db_companies::select();

		$this->relatedProducts = db_products::where('id',$this->product[products][id],'<>');
		$this->relatedProducts = db_products::where('company_id',$this->company[companies][id]);
		$this->relatedProducts = db_products::where('View' , 1);
		$this->relatedProducts = db_products::where('lang_id' ,loader::$langId);
		$this->relatedProducts = db_products::selectAll();





		view::renderLayout();

	}

}
