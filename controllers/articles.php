<?php
//2

class articles extends loader {
	var $metaTitle ;
	var $metaDesc ; 
	var $metaKeywords ;
//0
	protected function renderArticle(){

		$this->article = db_articles::where(id,loader::$urlInfo[count(loader::$urlInfo)-1]);
		$this->article = db_articles::where('view' , 1);
		$this->article = db_articles::where('lang_id' ,loader::$langId);
		$this->article = db_articles::select();
		    
		$this->metaTitle = $this->article[articles][meta_title];
		$this->metaDesc = $this->article[articles][meta_desc];
		$this->metaKeywords = $this->article[articles][meta_keywords];

		//print_r($this->article);

		view::renderLayout();

  	}

 
//1
	protected function renderAllArticles(){

		$this->article = db_articles::where(id,loader::$urlInfo[count(loader::$urlInfo)-1]);
		$this->article = db_articles::where('View' , 1);
		$this->article = db_articles::where('lang_id' ,loader::$langId);
		$this->article = db_articles::select();
		    
		$this->metaTitle = $this->article[articles][meta_title];
		$this->metaDesc = $this->article[articles][meta_desc];
		$this->metaKeywords = $this->article[articles][meta_keywords];

		view::renderLayout();
	}

  
}
