<?php
//6

class events extends loader {
	var $metaTitle ;
	var $metaDesc ; 
	var $metaKeywords ;
//0
	protected function renderEventsPage(){

		$this->meta = db_events::where('id','10','<');
		$this->meta = db_events::where('lang_id' ,loader::$langId);
		$this->meta = db_events::select();
		    
		$this->metaTitle = $this->meta[news][meta_title];
		$this->metaDesc = $this->meta[news][meta_desc];
		$this->metaKeywords = $this->meta[news][meta_keywords];

		$this->hotEvents = db_events::where('id','10','>');
		$this->hotEvents = db_events::where('lang_id' ,loader::$langId);
		$this->hotEvents = db_events::where('hot_event','1');
		$this->hotEvents = db_events::where('view','1');
		$this->hotEvents = db_events::orderBy('date');
		$this->hotEvents = db_events::selectAll();

		$this->allEvents = db_events::where('id','10','>');
		$this->allEvents = db_events::where('lang_id' ,loader::$langId);
		$this->allEvents = db_events::where('hot_event','0');
		$this->allEvents = db_events::where('view','1');
		$this->allEvents = db_events::orderBy('date');
		$this->allEvents = db_events::setLimit(10);
		$this->allEvents = db_events::selectAll();

		view::renderLayout();

  	}

//1

	protected function renderEvents(){

		$this->event = db_events::where('id',loader::$urlInfo[count(loader::$urlInfo)-1]);
		$this->event = db_events::where('lang_id' ,loader::$langId);
		$this->event = db_events::select();
		    
		$this->metaTitle = $this->event[events][meta_title];
		$this->metaDesc = $this->event[events][meta_desc];
		$this->metaKeywords = $this->event[events][meta_keywords];

		$this->allEvents = db_events::where('id','10','>');
		$this->allEvents = db_events::where('lang_id' ,loader::$langId);
		$this->allEvents = db_events::where('hot_event','0');
		$this->allEvents = db_events::where('view','1');
		$this->allEvents = db_events::orderBy('date');
		$this->allEvents = db_events::setLimit(10);
		$this->allEvents = db_events::selectAll();

		view::renderLayout();
	} 


  
}
